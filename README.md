jTimer
======

Requirements
------------

- Java 1.8


Installation
------------

Unzip archive and launch it.


Launch
------

Launch jtimer with following command:

``java -jar jtimer.jar``

or executable script:
``
    jtimer (linux)
    jtimer.exe (windows)
``

Credits
-------

Images licensed under Creative Commons Attribution 3.0 License and available at: http://www.fatcow.com/free-icons
