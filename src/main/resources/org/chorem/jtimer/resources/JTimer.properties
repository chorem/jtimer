###
# #%L
# jTimer
# %%
# Copyright (C) 2007 - 2020 CodeLutin
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
# JTimer.properties file
#
# Warning : This file is located in org.chorem.jtimer.resources
#           and is only loaded by Swing application framework.

# Standard application resources
Application.id = jtimer
Application.title = jTimer
Application.version = ${project.version}
Application.vendor = Code Lutin
Application.vendorId = codelutin
Application.homepage = http://www.codelutin.com
Application.description = jTimer
Application.lookAndFeel = system
Application.icon = jtimer-40-orange.png

# Resources for @Actions defined in jTimer
newProject.Action.text = &New Project
newProject.Action.icon = report_add.png
newProject.Action.accelerator = shift control N
newProject.Action.shortDescription = Create new project

editProject.Action.text = &Edit project
editProject.Action.accelerator = shift F2
editProject.Action.shortDescription = Edit project

deleteProject.Action.text = &Delete Project
deleteProject.Action.accelerator = shift control DELETE
deleteProject.Action.shortDescription = Delete project

closeProject.Action.text = &Open/Close Project
closeProject.Action.accelerator = shift control O
closeProject.Action.shortDescription = Open or close project

newTask.Action.text = &New Task
newTask.Action.icon = date_task.png
newTask.Action.accelerator = control N
newTask.Action.shortDescription = Create new task

editTask.Action.text = &Edit Task
editTask.Action.accelerator = F2
editTask.Action.shortDescription = Edit task

closeTask.Action.text = &Open/Close Task
closeTask.Action.accelerator = control O
closeTask.Action.shortDescription = Open or close task

deleteTask.Action.text = &Delete Task
deleteTask.Action.accelerator = DELETE
deleteTask.Action.shortDescription = Delete task

startTask.Action.text = &Start
startTask.Action.icon = clock_play.png
startTask.Action.accelerator = alt S
startTask.Action.shortDescription = Start selected task

stopTask.Action.text = St&op
stopTask.Action.icon = clock_stop.png
stopTask.Action.accelerator = alt shift S
stopTask.Action.shortDescription = Stop selected task

addAnnotation.Action.text = Add &annotation
addAnnotation.Action.icon = note_add.png
addAnnotation.Action.accelerator = control A
addAnnotation.Action.shortDescription = Add an annotation

editAlert.Action.text = Edit a&lerts
editAlert.Action.icon = bell.png
editAlert.Action.accelerator = control L
editAlert.Action.shortDescription = Edit alerts

increment1Task.Action.text = Increment 1 minute
increment1Task.Action.shortDescription = Increment task time by one minute

decrement1Task.Action.text = Decrement 1 minute
decrement1Task.Action.shortDescription = Decrement task time by one minute

increment5Task.Action.text = &Increment 5 minutes
increment5Task.Action.accelerator = control I
increment5Task.Action.shortDescription = Increment task time by five minutes

decrement5Task.Action.text = Decrement 5 minutes
decrement5Task.Action.accelerator = control D
decrement5Task.Action.shortDescription = Decrement task time by five minutes

increment30Task.Action.text = Increment 30 minutes
increment30Task.Action.accelerator = control shift I
increment30Task.Action.shortDescription = Increment task time by thirty minutes

decrement30Task.Action.text = Decrement 30 minutes
decrement30Task.Action.accelerator = control shift D
decrement30Task.Action.shortDescription = Decrement task time by thirty minutes

setToZero.Action.text = Set to &Zero
setToZero.Action.shortDecription = Reset task time to 0

mergeTasks.Action.text = &Merge
mergeTasks.Action.shortDecription = Merge tasks

makeReport.Action.text = Re&port...
makeReport.Action.shortDescription = Create report

isShowClosed.Action.text = Show &closed
isShowClosed.Action.shortDescription = Show closed task and project

isCloseToSystray.Action.text = Show task &name in systray
isCloseToSystray.Action.shortDescription = Show current running task name in systray tooltip

isShowTaskNameInSystray.Action.text = Close to &systray
isShowTaskNameInSystray.Action.shortDescription = Reduce application to system tray instead of closing

isReportFirstDayOfWeek1.Action.text = &Sunday
isReportFirstDayOfWeek2.Action.text = &Monday
isReportFirstDayOfWeek3.Action.text = &Tuesday
isReportFirstDayOfWeek4.Action.text = &Wednesday
isReportFirstDayOfWeek5.Action.text = &Thursday
isReportFirstDayOfWeek6.Action.text = &Friday
isReportFirstDayOfWeek7.Action.text = &Saturday

about.Action.text = &About...
about.Action.shortDescription = About ${Application.title}

quit.Action.text = &Quit

# Resources for named component properties

# mainframe
mainFrame.title = ${Application.title}

# menu
projectMenu.text = &Project
taskMenu.text = &Task
reportMenu.text = &Report
optionMenu.text = &Options
optionReportFirstDayMenu.text = Report - First &day of week
helpMenu.text = &Help

# Input
input.newProjectTitle=New project
input.newProjectMessage=Project name to create :
input.editProjectTitle=Edit project
input.editProjectMessage=New project name :
input.deleteProjectTitle=Confirm
input.deleteProjectMessage=Do you want to delete project "%s" ?
input.deleteProjectsMessage=Do you want to delete the %d selected projects ?
input.deleteTaskTitle=Confirm
input.deleteTaskMessage=Do you want to delete task "%s" ?
input.deleteTasksMessage=Do you want to delete the %d selected tasks ?
input.mergeTaskMessage=Do you want to merge selected %d tasks\ninto "%s" ?
input.mergeTaskTitle=Merge
input.addAnnotationTitle=Add annotation
input.addAnnotationMessage=Annotation for task "%s" :

# Error
action.invalidActionTitle=Can't do action
action.missingErrorMessage=Error message is missing (%s)

# listeners errors
vetoable.common.duplicated.project.name=A project already exists with that name at this level !
vetoable.common.duplicated.task.name=A task already exists with that name at this level !
vetoable.common.merge.invalid.types=Can't merge project and task !
vetoable.common.move.invalid.types=Can't move project !
vetoable.saver.empty.name=Empty task name !
vetoable.saver.invalid.characters=Task name contains invalid characters !

# Start fail i18n
startFail.title=Error
startFail.message=${Application.title} fail to init.\nCheck that ${Application.title} is not already launched.
