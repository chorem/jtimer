<#--
 #%L
 jTimer
 %%
 Copyright (C) 2007 - 2016 CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
Report by day
=============

<#macro displaySubtaskReport task periodTime identation=0>
  <#local ident=identation + 1/>
  <#list utils.getSubTaskOrdered(task) as subtask>
    <#local taskTotalTime=utils.getDailyTotalTaskTime(subtask, periodTime?date)/>
    <#if taskTotalTime &gt; 0>
    	<#local taskTime=utils.getDailyTaskTime(subtask, periodTime?date)/>
<#list 1..ident as i>  </#list>- ${subtask.name}<#if includeTime && taskTime &gt; 0> : ${utils.formatDuration(taskTime)}</#if><#if intermediateTotalTime && taskTotalTime &gt; 0 && !subtask.getSubTasks().isEmpty()> (total: ${utils.formatDuration(taskTotalTime)})</#if>
    	<#if annotations>
	    	<#local taskAnnTimes=utils.getDailyTaskAnnotation(subtask, periodTime?date)/>
	    	<#list taskAnnTimes.keySet() as taskAnnTime>
<#list 1..ident as i>  </#list>  * <#if annotationsTime>${taskAnnTime?time?string.short} : </#if>${taskAnnTimes.get(taskAnnTime)}
      		</#list>
      	</#if>
  	  <@displaySubtaskReport task=subtask periodTime=periodTime identation=ident/>
  	</#if>
  </#list>
</#macro>

<#assign periods=utils.getDailyDates(begin?date,end?date)/>
<#list periods as period>
<#assign periodTotalTime=0/>
<#list projects as project>
  <#assign projectPeriodTotalTime=utils.getDailyTotalTaskTime(project, period?date)/>
  <#assign periodTotalTime=periodTotalTime+projectPeriodTotalTime/>
</#list>
<#if periodTotalTime &gt; 0>

<#assign title="${period?date?string.full}"/>
${title}
${""?left_pad(title?length,"-")}

<#list projects as project>
<#assign periodProjectTotalTime=utils.getDailyTotalTaskTime(project, period?date)/>
<#if periodProjectTotalTime &gt; 0>
- ${project.name}<#if intermediateTotalTime> (total: ${utils.formatDuration(periodProjectTotalTime)})</#if>
<@displaySubtaskReport task=project periodTime=period/>
</#if>
</#list>

Total : ${utils.formatDuration(periodTotalTime)}
</#if>
</#list>
