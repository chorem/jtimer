<#--
 #%L
 jTimer
 %%
 Copyright (C) 2007 - 2016 CodeLutin
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
Report by project
=================

<#macro displaySubtaskReport task identation=0>
  <#local ident=identation + 1/>
  <#list utils.getSubTaskOrdered(task) as subtask>
    <#local taskTotalTime=0/>
    <#list periods as period>
      <#local subtaskPeriodTotalTime=utils.getDailyTotalTaskTime(subtask, period?date)/>
      <#local taskTotalTime=taskTotalTime+subtaskPeriodTotalTime/>
    </#list>
    <#if taskTotalTime &gt; 0>
        <#local taskTime=0/>
        <#list periods as period>
          <#local subtaskPeriodTime=utils.getDailyTaskTime(subtask, period?date)/>
          <#local taskTime=taskTime+subtaskPeriodTime/>
        </#list>
<#list 1..ident as i>  </#list>- ${subtask.name}<#if includeTime && taskTime &gt; 0> : ${utils.formatDuration(taskTime)}</#if><#if intermediateTotalTime && taskTotalTime &gt; 0 && !subtask.getSubTasks().isEmpty()> (total: ${utils.formatDuration(taskTotalTime)})</#if>
    	<#if annotations>
        	<#list periods as period>
	    		<#local taskAnnTimes=utils.getDailyTaskAnnotation(subtask, period?date)/>
	    		<#list taskAnnTimes.keySet() as taskAnnTime>
<#list 1..ident as i>  </#list>  * <#if annotationsTime>${taskAnnTime?datetime?string.short} : </#if>${taskAnnTimes.get(taskAnnTime)}
      			</#list>
	        </#list>
      	</#if>
  	  <@displaySubtaskReport task=subtask identation=ident/>
  	</#if>
  </#list>
</#macro>

<#assign periods=utils.getDailyDates(begin?date,end?date)/>
<#list projects as project>
  <#assign periodTotalTime=0/>
  <#list periods as period>
    <#assign projectPeriodTotalTime=utils.getDailyTotalTaskTime(project, period?date)/>
    <#assign periodTotalTime=periodTotalTime+projectPeriodTotalTime/>
  </#list>
<#if periodTotalTime &gt; 0>

<#assign title="${project.name}"/>
${title}
${""?left_pad(title?length,"-")}

<@displaySubtaskReport task=project/>

Total : ${utils.formatDuration(periodTotalTime)}
</#if>
</#list>
