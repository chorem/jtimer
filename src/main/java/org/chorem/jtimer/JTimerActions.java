/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer;

import org.chorem.jtimer.JTimerConfig.JTimerAction;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfig.Action.Step;

/**
 * JTimer actions class run from command line.
 *
 * @author chatellier
 * @version $Revision: 2825 $
 *
 * Last update : $Date: 2012-03-15 11:41:39 +0100 (jeu. 15 mars 2012) $
 * By : $Author: echatellier $
 */
public class JTimerActions {

    protected ApplicationConfig config;

    public JTimerActions(ApplicationConfig config) {
        this.config = config;
    }

    @Step(JTimerConfig.STEP_BEFORE_UI)
    public void help() {
        displayVersion();
        System.out.println();
        for (JTimerAction a : JTimerAction.values()) {
            for (int index = 0; index < a.aliases.length; index++) {
                System.out.print(a.aliases[index]);
                if (index != a.aliases.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println("\t" + a.description);
        }
        System.exit(0);
    }

    @Step(JTimerConfig.STEP_BEFORE_UI)
    public void version() {
        displayVersion();
        System.exit(0);
    }

    protected void displayVersion() {
        System.out.println("jTimer " + config.getOption("application.version"));
        System.out.println(config.getOption("application.website"));
    }

    @Step(JTimerConfig.STEP_AFTER_UI)
    public void start(String taskPath) {
        JTimer ui = config.getObject(JTimer.class);
        ui.startTask(taskPath);
    }
}
