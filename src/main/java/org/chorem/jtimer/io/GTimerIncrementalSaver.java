/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.io;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.DataEventListener;
import org.chorem.jtimer.data.DataViolationException;
import org.chorem.jtimer.entities.TimerAlert;
import org.chorem.jtimer.entities.TimerAlert.Type;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Charge et sauve les fichiers au format gTimer.
 *
 * Le format de fichier utilisé ici est 1.2. => Format: 1.2
 *
 * Changes:
 *  - contrairement à la premiere version, ce Saver sauve les
 *    projets/taches/annotations seulement si elles ont été modifiées.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class GTimerIncrementalSaver extends AbstractSaver implements Saver,
        DataEventListener {

    /** log. */
    private static Log log = LogFactory.getLog(GTimerIncrementalSaver.class);

    /**
     * Separateur pour les sous taches.
     *
     * Utilise pour assurer une compatibilite avec gTimer, qui ne permet pas de
     * stocker des sous taches.
     */
    protected static final String GTIMER_SUBTASK_SEPARATOR = "/";

    /** Version du format de fichier. */
    protected static final String GTIMER_FILE_VERSION = "1.2";

    /** Extension project. */
    protected static final String GTIMER_PROJECT_EXTENSION = "project";

    /** Extension task. */
    protected static final String GTIMER_TASK_EXTENSION = "task";

    /** Extension annotation. */
    protected static final String GTIMER_ANNOTATION_EXTENSION = "ann";

    /** Extension alert. */
    protected static final String GTIMER_ALERT_EXTENSION = "alert";

    /** Empty gtimer project name. */
    protected static final String GTIMER_EMPTY_PROJECT_NAME = "No project";

    /** Lock filename. */
    protected static final String LOCK_FILE_NAME = ".lock";

    /**
     * Save directory. Init with $user.home/.jtimer/data
     */
    protected File dataSaveDirectory;

    /** Resource used by lock. */
    protected FileLock lock;

    /** Resource used by lock. */
    protected RandomAccessFile raf;

    /** Resource used by lock. */
    protected FileChannel channel;

    /** Timer used for autosave. */
    protected Timer autoSaveTimer;

    /**
     * Auto save delay. (default value is 5min)
     */
    protected long autoSaveDelay = 1000 * 60 * 5; // 5 min

    /** Running task list. */
    protected final Collection<TimerTask> runningTasks;

    /**
     * Constructor.
     */
    public GTimerIncrementalSaver() {

        dataSaveDirectory = JTimer.config.getDataDirectory();

        // make implementation synchronized
        runningTasks = Collections.synchronizedCollection(new ArrayList<>());

        autoSaveTimer = new Timer();
    }

    @Override
    public void setAutoSaveDelay(long autoSaveDelay) {

        // autoSaveDelay is in ms
        if (autoSaveDelay > 0) {
            this.autoSaveDelay = autoSaveDelay;
        }
    }

    /**
     * Check that save directory exists. Create it if not.
     */
    protected void checkSaveDirectory() {
        // creer le dossier s'il n'existe pas (pour le lock)
        if (!dataSaveDirectory.exists()) {
            dataSaveDirectory.mkdirs();
        }
    }

    @Override
    public void lock() throws DataLockingException {

        try {
            // be sure that directory exists
            checkSaveDirectory();

            // try to get lock
            raf = new RandomAccessFile(dataSaveDirectory + File.separator
                    + LOCK_FILE_NAME, "rw");

            channel = raf.getChannel();

            // lock request
            // use tryLock, lock() is blocking
            lock = channel.tryLock();

            // can't get lock
            if (lock == null) {
                throw new DataLockingException("Cannot get lock");
            }

            // clear task list
            runningTasks.clear();

            // start timer
            // FIXME doesn't work if lock() is called twice
            autoSaveTimer.schedule(this, autoSaveDelay, autoSaveDelay);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Cannot get lock", e);
            }
            throw new DataLockingException("Cannot get lock", e);
        }
    }

    @Override
    public void unlock() throws DataLockingException {

        if (lock != null) {
            try {
                // release lock
                lock.release();

                // cancel timer
                cancel();
                autoSaveTimer.purge();
                autoSaveTimer.cancel();

                // save running tasks
                saveRunningTasks();

                // clear task list
                runningTasks.clear();

                // release resources
                channel.close();
                raf.close();
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Cannot release lock", e);
                }
                throw new DataLockingException("Cannot release lock", e);
            }
        }
    }

    @Override
    public Collection<TimerProject> load() {

        // be sure that directory exists
        // already done by lock()
        // checkSaveDirectory();

        // load all in gtimer format
        Collection<TimerProject> result = gTimerLoad();

        return result;
    }

    /**
     * Load from gtimer file format.
     *
     * @return projects loaded
     */
    protected Collection<TimerProject> gTimerLoad() {

        // map number => project
        Map<String, TimerProject> mapNumberProject = new HashMap<>();

        // probleme : il faut d'abord lire les "project" avant les task
        // pour pouvoir fair l'association
        // donc deux boucles

        // loop 1 , reading projects
        // project file "0.project"
        Pattern pPatronNommage = Pattern.compile("^(\\d+)\\."
                + GTIMER_PROJECT_EXTENSION + "$");
        for (File f : dataSaveDirectory.listFiles()) {
            if (f.isFile() && f.canRead()) {
                Matcher m = pPatronNommage.matcher(f.getName());
                if (m.find()) {
                    String number = m.group(1); // 1 = le chiffre

                    try {
                        int projectNumber = Integer.parseInt(number);

                        TimerProject p = getProjectFromFile(f);
                        if (p != null) {
                            p.setNumber(projectNumber);
                            mapNumberProject.put(number, p);
                        }
                    } catch (NumberFormatException e) {
                        if (log.isWarnEnabled()) {
                            log.warn("Can't parse " + f.getName() + " as gtimer file", e);
                        }
                    } catch (IOException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can't read file", e);
                        }
                    }
                }
            }
        }

        // loop 2 , reading tasks
        // task file "0.task"
        pPatronNommage = Pattern.compile("^(\\d+)\\." + GTIMER_TASK_EXTENSION + "$");

        // map to remember tasks to manage after load
        // sorted on / number in their name
        SortedMap<TimerTask, TimerProject> taskToPostManaged = new TreeMap<>((t1, t2) -> {

            int numberOfSeparatorInT1 = t1.getName().split(
                    GTIMER_SUBTASK_SEPARATOR).length;
            int numberOfSeparatorInT2 = t2.getName().split(
                    GTIMER_SUBTASK_SEPARATOR).length;

            int compare = numberOfSeparatorInT1
                    - numberOfSeparatorInT2;

            // seems to delete when return 0 ???
            if (compare == 0) {
                compare = -1;
            }
            return compare;
        });

        // now process each file
        for (File f : dataSaveDirectory.listFiles()) {
            if (f.isFile() && f.canRead()) {
                Matcher m = pPatronNommage.matcher(f.getName());
                if (m.find()) {

                    // oui, annotations sur un numero de tache
                    String number = m.group(1);

                    try {
                        int taskNumber = Integer.parseInt(number);

                        TimerTask task = parseTaskFromFile(mapNumberProject, f,
                                taskToPostManaged);

                        if (task != null) {
                            task.setNumber(taskNumber);

                            parseAnnotations(task);
                            parseAlerts(task);
                        }
                    } catch (NumberFormatException e) {
                        if (log.isWarnEnabled()) {
                            log.warn("Can't parse " + f.getName()
                                    + " as gtimer file", e);
                        }
                    } catch (IOException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can't parse task file", e);
                        }
                    }
                }
            }
        }

        // post process tasks
        parseTaskFromSavedMap(taskToPostManaged);

        // manage backup
        if (backupGTimerFiles()) {
            cleanBackupFiles();
        }

        // collection to return
        Collection<TimerProject> projects = mapNumberProject.values();

        return projects;
    }

    /**
     * Load a project in his file.
     *
     * @param projectFile project file
     * @return a project
     * @throws IOException if ioexception occurs
     */
    protected TimerProject getProjectFromFile(File projectFile)
            throws IOException {

        TimerProject p = null;

        try (InputStream is = new BufferedInputStream(new FileInputStream(projectFile))) {
            Properties prop = new Properties();
            prop.load(is);

            // log
            if (log.isDebugEnabled()) {
                log.debug("Load project (" + projectFile.getName() + ") : "
                        + prop.getProperty("Name"));
            }


            if (GTIMER_FILE_VERSION.equals(prop.get("Format"))) {
                p = new TimerProject();
                p.setName(prop.getProperty("Name"));

                // manage creation timestamp (conversion to long)
                try {
                    String creationTimeStamp = prop.getProperty("Created");
                    long timestampinms = Long.parseLong(creationTimeStamp) * 1000;
                    p.setCreationDate(new Date(timestampinms));
                } catch (NumberFormatException e) {
                    if (log.isWarnEnabled()) {
                        log.warn("Invalid 'Created' timestamp", e);
                    }
                    p.setCreationDate(new Date(0));
                }

                p.setClosed(prop.getProperty("Options").equals("1"));
            } else {
                if (log.isWarnEnabled()) {
                    log.warn("Invalid file format. Excepted " + GTIMER_FILE_VERSION
                            + ", found " + prop.get("Format"));
                }
            }
        }

        return p;
    }

    /**
     * Parse task file.
     *
     * @param mapNumberProject map entre les projets reels et leur numero gtimer
     * @param taskFile gtimer task file
     * @param taskToManage sorted map to remember task to post process
     * @return parsed task
     * @throws IOException if ioexception occurs
     */
    protected TimerTask parseTaskFromFile(
            Map<String, TimerProject> mapNumberProject, File taskFile,
            SortedMap<TimerTask, TimerProject> taskToManage) throws IOException {

        TimerTask t = null;

        Pattern dataPattern = Pattern.compile("(\\d{4})(\\d{2})(\\d{2})");

        try (InputStream is = new BufferedInputStream(new FileInputStream(taskFile))) {
            Properties prop = new Properties();
            prop.load(is);

            if (GTIMER_FILE_VERSION.equals(prop.get("Format"))) {
                t = new TimerTask();

                // manage creation timestamp (convertion to long)
                try {
                    String creationTimeStamp = prop.getProperty("Created");
                    long timestampinms = Long.parseLong(creationTimeStamp) * 1000;
                    t.setCreationDate(new Date(timestampinms));
                } catch (NumberFormatException e) {
                    if (log.isWarnEnabled()) {
                        log.warn("Invalid 'Created' timestamp for " + taskFile, e);
                    }
                    t.setCreationDate(new Date(0));
                }

                t.setClosed(prop.getProperty("Options").equals("1"));

                // name = task
                // name = task/subtask1
                // name = task/subtask1/subsubtask1
                String gtimerTaskName = prop.getProperty("Name");
                t.setName(gtimerTaskName);
                // yes put all names
                // will be corrected later

                // log
                if (log.isDebugEnabled()) {
                    log.debug("Load task (" + taskFile.getName() + ") : " + gtimerTaskName);
                }

                // analyse des donnees (temps)
                for (Object key : prop.keySet()) {
                    String sKey = (String) key;

                    // test if key format match
                    Matcher m = dataPattern.matcher(sKey);
                    if (m.find()) {
                        try {
                            Date keyDate = GTimerTimeUtil.yyyyMMdd2Date(sKey);
                            String timeString = (String) prop.get(sKey);
                            Long time = Long.valueOf(timeString) * 1000;
                            if (time > 0) {
                                t.setTime(keyDate, time);
                            }
                        } catch (NumberFormatException e) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't convert " + prop.get(sKey) + " into long");
                            }
                        }
                    }
                    // else not data entry
                }

                // find associated project instance with number
                String taskProjectNumber = (String) prop.get("Project");
                TimerProject associatedProject = mapNumberProject
                        .get(taskProjectNumber);

                // fix bug case task has no associated project
                // can do that in gtimer
                if (associatedProject == null && taskProjectNumber.equals("-1")) {
                    associatedProject = new TimerProject();
                    associatedProject.setName(GTIMER_EMPTY_PROJECT_NAME);
                    mapNumberProject.put(taskProjectNumber, associatedProject);
                }

                if (associatedProject != null) {
                    // used to correct bug #1636 : [jTimer] Bug du rechargement des
                    // sous taches
                    taskToManage.put(t, associatedProject);

                    if (log.isDebugEnabled()) {
                        log.debug("Put " + t.getName() + ", " + associatedProject.getName());
                    }
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("File " + taskFile + ": task " + t.getName()
                                + " is associated with a wrong project number " + prop.get("Project"));
                    }
                }

            } else {
                if (log.isWarnEnabled()) {
                    log.warn("File " + taskFile + ": Invalid format. Excepted " + GTIMER_FILE_VERSION
                            + ", found " + prop.get("Format"));
                }
            }
        }

        return t;
    }

    /**
     * Try to find and load annotations from task.
     *
     * @param task task to load annotations
     * @throws IOException if can't read ann file
     */
    protected void parseAnnotations(TimerTask task) throws IOException {
        int taskNumber = task.getNumber();

        File annotationsTaskFile = new File(dataSaveDirectory + File.separator
                + taskNumber + "." + GTIMER_ANNOTATION_EXTENSION);

        if (annotationsTaskFile.exists()) {
            if (log.isDebugEnabled()) {
                log.debug("Annotations found for task " + task.getName());
            }

            try (InputStream is = new BufferedInputStream(new FileInputStream(annotationsTaskFile))) {
                Properties prop = new Properties();
                prop.load(is);

                // analyse des donnees (temps)
                for (Object key : prop.keySet()) {
                    String sKey = (String) key;

                    // test if key format match
                    try {

                        // key of annotation is in seconds
                        long timestamp = Long.parseLong(sKey);
                        Date dateTS = new Date(timestamp * 1000);

                        String annoText = (String) prop.get(sKey);

                        task.addAnnotation(dateTS, annoText);
                    } catch (NumberFormatException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Can't convert " + sKey + " into long");
                        }
                    }
                }
            }
        }
    }

    /**
     * Find task alert and load it.
     *
     * This file can't be loaded by {@code Properties#load(java.io.InputStream)}.
     * Constains duplicated keys.
     *
     * @param task task to load alert
     * @throws IOException if can't read ann file
     */
    protected void parseAlerts(TimerTask task) throws IOException {
        int taskNumber = task.getNumber();

        File alertTaskFile = new File(dataSaveDirectory + File.separator
                + taskNumber + "." + GTIMER_ALERT_EXTENSION);

        if (alertTaskFile.exists()) {
            if (log.isDebugEnabled()) {
                log.debug("Alert found for task " + task.getName());
            }

            try (BufferedReader alertIn = new BufferedReader(new FileReader(alertTaskFile))) {

                // skip first line : "format: 1.2"
                String line = alertIn.readLine();
                while ((line = alertIn.readLine()) != null) {

                    line = line.trim();
                    if (!line.isEmpty()) {
                        String alertType = line.substring(0, line.indexOf(' '));
                        String alertDuration = line.substring(
                                line.indexOf(' ') + 1, line.length());

                        try {
                            if ("reachtotaltime".equals(alertType)) {
                                Long duration = Long.parseLong(alertDuration) * 1000;
                                TimerAlert alert = new TimerAlert(
                                        TimerAlert.Type.REACH_TOTAL_TIME, duration);
                                task.addAlert(alert);
                            } else if ("reachdailytime".equals(alertType)) {
                                Long duration = Long.parseLong(alertDuration) * 1000;
                                TimerAlert alert = new TimerAlert(
                                        TimerAlert.Type.REACH_DAILY_TIME, duration);
                                task.addAlert(alert);
                            } else {
                                if (log.isWarnEnabled()) {
                                    log.warn("Unknow alert type " + alertType);
                                }
                            }
                        } catch (NumberFormatException e) {
                            if (log.isErrorEnabled()) {
                                log.error("Can't convert " + alertDuration
                                        + " into long");
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Post process task to manage subtask loaded before parent task.
     *
     * @param taskToManage sorted map of those task
     */
    protected void parseTaskFromSavedMap(
            SortedMap<TimerTask, TimerProject> taskToManage) {

        // process each task
        for (Map.Entry<TimerTask, TimerProject> entry : taskToManage.entrySet()) {
            TimerTask currentTask = entry.getKey();
            TimerProject associatedProject = entry.getValue();

            if (log.isDebugEnabled()) {
                log.debug("Post process task " + currentTask.getName());
            }

            // process task name
            String gTimerTaskname = currentTask.getName();
            String[] tabTaskNames = gTimerTaskname
                    .split(GTIMER_SUBTASK_SEPARATOR);

            // associated to project
            if (tabTaskNames.length == 1) {
                associatedProject.addTask(currentTask);

            } else {
                // tabTaskNames.length != 1
                // sub task : add task to corresponding task

                // correct task name
                String realTaskName = tabTaskNames[tabTaskNames.length - 1];
                currentTask.setName(realTaskName);

                if (log.isDebugEnabled()) {
                    log.debug("Converting task " + gTimerTaskname
                            + " in sub tasks");
                }

                TimerTask task = findTask(associatedProject, Arrays
                        .copyOfRange(tabTaskNames, 0, tabTaskNames.length - 1));
                if (task != null) {
                    task.addTask(currentTask);
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("task " + Arrays.toString(tabTaskNames)
                                + " cannot be found, add task to project");
                    }

                    // add task to project, to not loose it
                    currentTask.setName(gTimerTaskname);
                    associatedProject.addTask(currentTask);
                }
            }
        }
    }

    /**
     * Find a task in task's subtask
     *
     * @param parentTask task
     * @param taskNames names
     * @return a task or <tt>null</tt>
     */
    protected TimerTask findTask(TimerTask parentTask, String[] taskNames) {

        TimerTask result = null;

        if (taskNames.length > 0) {
            for (TimerTask task : parentTask.getSubTasks()) {
                if (task.getName().equals(taskNames[0])) {
                    result = findTask(task, Arrays.copyOfRange(taskNames, 1,
                            taskNames.length));
                }
            }
        } else {
            result = parentTask;
        }

        return result;
    }

    /**
     * For development.
     *
     * Save gtimer files (because project collection is sometimes erased)
     *
     * @return success flag fro creating backup
     */
    protected boolean backupGTimerFiles() {

        boolean result = false;

        // build date string format = YYYYMMDDHHMMSS
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String nowString = dateFormat.format(new Date());

        // backup directory
        String backupDir = JTimer.config.getBackupDirectory().getAbsolutePath();

        // build file name
        String zipFileName = backupDir + File.separator + "backup-" + nowString
                + ".zip";

        // log
        if (log.isDebugEnabled()) {
            log.debug("Creating backup archive : " + zipFileName);
        }

        // create dir if not exists
        File backupDirFile = new File(backupDir);
        if (!backupDirFile.exists()) {
            backupDirFile.mkdirs();
        }

        //
        try (ZipOutputStream outZip = new ZipOutputStream(new FileOutputStream(zipFileName))) {

            // Create a buffer for reading the files
            byte[] buffer = new byte[1024];

            // add in this archive only gtimer files
            File[] filesInIt = dataSaveDirectory.listFiles();

            for (File fileInIt : filesInIt) {
                String filename = fileInIt.getName();
                if (isGTimerFile(filename)) {

                    try (FileInputStream inFileStream = new FileInputStream(fileInIt)) {
                        outZip.putNextEntry(new ZipEntry(filename));

                        // Transfer bytes from the file to the ZIP file
                        int len;
                        while ((len = inFileStream.read(buffer)) > 0) {
                            outZip.write(buffer, 0, len);
                        }

                        // Complete the entry
                        outZip.closeEntry();
                    }
                }
            }

            result = true;

        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't create archive", e);
            }
        }

        return result;
    }

    /**
     * Clean backup files older than one week.
     */
    protected void cleanBackupFiles() {

        // compute date from one week ago
        LocalDateTime oneWeekAgo = LocalDateTime.now().minusWeeks(2);
        String oneWeekString = oneWeekAgo.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        Pattern pattern = Pattern.compile("backup-(\\d{14})\\.zip");

        // collect each file in backup dir
        Path backupDir = JTimer.config.getBackupDirectory().toPath();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(backupDir, "backup-*.zip")) {
            for (Path file : stream) {
                String name = file.getFileName().toString();
                Matcher m = pattern.matcher(name);
                m.matches();
                String backupDate = m.group(1);

                // delete all files older than two weeks
                if (backupDate.compareTo(oneWeekString) < 0) {
                    Files.delete(file);
                }
            }
        } catch (DirectoryIteratorException | IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't create backup", ex);
            }
        }
    }

    /**
     * Check if a file denoted by his name is a gtimer file.
     *
     * Check that the name end with .project, .task or .ann
     *
     * @param filename filename
     * @return answer
     */
    protected boolean isGTimerFile(String filename) {
        boolean result = false;

        if (filename.endsWith("." + GTIMER_PROJECT_EXTENSION)
                || filename.endsWith("." + GTIMER_TASK_EXTENSION)
                || filename.endsWith("." + GTIMER_ANNOTATION_EXTENSION)
                || filename.endsWith("." + GTIMER_ALERT_EXTENSION)) {
            result = true;
        }

        return result;
    }

    /**
     * Save a unique project.
     *
     * @param project project to save
     */
    protected void saveProject(TimerProject project) {

        if (log.isDebugEnabled()) {
            log.debug("Save project : " + project.getName());
        }

        // build full file name
        String filename = dataSaveDirectory + File.separator + project.getNumber()
                + "." + GTIMER_PROJECT_EXTENSION;

        if (log.isDebugEnabled()) {
            log.debug("Save project in " + filename);
        }

        File backupfile = null;
        File projectfile = new File(filename);

        // encode it to iso-8859-1, because props.load() use this encoding
        try (Writer out = new OutputStreamWriter(new FileOutputStream(projectfile), "ISO-8859-1")) {

            // first try to make backup
            backupfile = BackupUtils.makeBackupFile(projectfile);

            // get creation date
            long mscreatedtime = project.getCreationDate().getTime();
            String createdTime = String.valueOf(mscreatedtime / 1000);

            // file content
            out.write("Format: " + GTIMER_FILE_VERSION + "\n");
            out.write("Name: " + project.getName() + "\n");
            out.write("Created: " + createdTime + "\n");
            out.write("Options: " + (project.isClosed() ? "1" : "0") + "\n");

            BackupUtils.deleteBackupFile(backupfile);
        } catch (IOException e) {
            if (log.isDebugEnabled()) {
                log.error("Can't save project information, restore backup file", e);
            }

            // can be null if backup throw the exception
            if (backupfile != null) {
                BackupUtils.restoreBackupFile(backupfile);
            }
        }
    }

    /**
     * Save task, found correct prefix, and project.
     *
     * Then call {@link #saveTask(TimerTask, int, String)}
     *
     * @param task task to save
     */
    protected void saveTask(TimerTask task) {

        // Try to find project
        // And prefix
        String taskPrefixName = "";

        TimerTask currentTask = task;
        while (currentTask.getParent() != null) {
            currentTask = currentTask.getParent();
            if (currentTask.getParent() != null) {
                // currentTask is styll a task
                taskPrefixName = currentTask.getName()
                        + GTIMER_SUBTASK_SEPARATOR + taskPrefixName;
            }
        }
        int associatedToProject = currentTask.getNumber();

        // then save task
        saveTask(task, associatedToProject, taskPrefixName);
    }

    /**
     * Save a task.
     *
     * taskPrefixName represents the task prefix is case, it is represented in
     * jTimer as a subtask. It is saved in gTimer format in a new task, which
     * name is composed of parents task name.
     *
     * @param task task to save
     * @param associatedToProject associated project number
     * @param taskPrefixName task prefix name
     */
    protected void saveTask(TimerTask task, int associatedToProject, String taskPrefixName) {

        // check project number
        if (associatedToProject < 0) {
            throw new IllegalArgumentException(
                    "Task project number is invalid : " + associatedToProject);
        }

        // check prefix
        if (taskPrefixName == null) {
            throw new IllegalArgumentException("Task prefix is null");
        }

        if (log.isDebugEnabled()) {
            log.debug("Save task : " + task.getName());
        }

        // build full file name
        String filename = dataSaveDirectory + File.separator + task.getNumber()
                + "." + GTIMER_TASK_EXTENSION;

        if (log.isDebugEnabled()) {
            log.debug("Save task in " + filename);
        }

        File backupfile = null;
        File taskfile = new File(filename);

        // encode it to iso-8859-1, because props.load() use this encoding
        try (Writer out = new OutputStreamWriter(new FileOutputStream(taskfile), "ISO-8859-1")) {

            // first make backup
            backupfile = BackupUtils.makeBackupFile(taskfile);

            // Format: 1.2
            // Name: Test Tache 1.2
            // Created: 1180944724
            // Options: 0
            // Project: 0
            // Data:
            // 20070603 750
            // 20070604 366

            // get creation date
            long mscreatedtime = task.getCreationDate().getTime();
            String createdTime = String.valueOf(mscreatedtime / 1000);

            // file content
            out.write("Format: " + GTIMER_FILE_VERSION + "\n");
            out.write("Name: " + taskPrefixName + task.getName() + "\n");
            out.write("Created: " + createdTime + "\n");
            out.write("Options: " + (task.isClosed() ? "1" : "0") + "\n");
            out.write("Project: " + associatedToProject + "\n");
            out.write("Data:\n");

            // save time of each day
            for (Entry<Date, Long> entry : task.getAllDaysAndTimes().entrySet()) {

                Date date = entry.getKey();
                long value = entry.getValue() / 1000;
                if (value > 0) {
                    String gtimerDate = GTimerTimeUtil.date2yyyyMMdd(date);
                    out.write(gtimerDate + " " + value + "\n");
                }
            }

            BackupUtils.deleteBackupFile(backupfile);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't save task", e);
            }

            // can be null if backup throw the exception
            if (backupfile != null) {
                BackupUtils.restoreBackupFile(backupfile);
            }
        }
    }

    /**
     * Save annotation for a task.
     *
     * @param task task to save annotation
     */
    protected void saveTaskAnnotation(TimerTask task) {

        int taskNumber = task.getNumber();

        File annotationTaskFile = new File(dataSaveDirectory + File.separator
                + taskNumber + "." + GTIMER_ANNOTATION_EXTENSION);

        if (task.getAllDaysAnnotations() != null
                && !task.getAllDaysAnnotations().isEmpty()) {

            File backupfile = null;
            try (Writer out = new OutputStreamWriter(new FileOutputStream(annotationTaskFile), "ISO-8859-1")) {

                // first make backup
                backupfile = BackupUtils.makeBackupFile(annotationTaskFile);

                // save time of each day
                for (Entry<Date, String> entry : task.getAllDaysAnnotations()
                        .entrySet()) {

                    Date date = entry.getKey();
                    String gtimerTS = String.valueOf(date.getTime() / 1000);

                    out.write(gtimerTS + " " + entry.getValue() + "\n");
                }

                BackupUtils.deleteBackupFile(backupfile);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.debug("Can't save task", e);
                }

                // can be null if backup throw the exception
                if (backupfile != null) {
                    BackupUtils.restoreBackupFile(backupfile);
                }
            }
        } else {
            annotationTaskFile.delete();
        }
    }

    /**
     * Save task alerts.
     *
     * @param task task to save alert
     */
    protected void saveAlerts(TimerTask task) {

        int taskNumber = task.getNumber();

        File alertTaskFile = new File(dataSaveDirectory + File.separator
                + taskNumber + "." + GTIMER_ALERT_EXTENSION);

        if (task.getAlerts() != null && !task.getAlerts().isEmpty()) {

            File backupfile = null;
            try (Writer out = new OutputStreamWriter(new FileOutputStream(alertTaskFile), "ISO-8859-1")) {

                // first make backup
                backupfile = BackupUtils.makeBackupFile(alertTaskFile);

                out.write("Format: " + GTIMER_FILE_VERSION + "\n");

                // save each alert
                for (TimerAlert alert : task.getAlerts()) {

                    Type type = alert.getType();
                    long duration = alert.getDuration() / 1000;

                    switch (type) {
                        case REACH_DAILY_TIME:
                            out.write("reachdailytime " + duration + "\n");
                            break;
                        case REACH_TOTAL_TIME:
                            out.write("reachtotaltime " + duration + "\n");
                            break;
                    }
                }

                BackupUtils.deleteBackupFile(backupfile);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.debug("Can't save task", e);
                }

                // can be null if backup throw the exception
                if (backupfile != null) {
                    BackupUtils.restoreBackupFile(backupfile);
                }
            }
        } else {
            alertTaskFile.delete();
        }

    }

    @Override
    public void addProject(TimerProject project) {

        // searching for good project number
        int projectNumber = getUnusedNumber("." + GTIMER_PROJECT_EXTENSION);
        project.setNumber(projectNumber);

        if (log.isDebugEnabled()) {
            log.debug("Setting project number for " + project.getName()
                    + " to " + projectNumber);
        }

        saveProject(project);

        // loop on subtasks to set correct task number (only on add)
        project.getSubTasks().forEach(this::addTask);
    }

    @Override
    public void addTask(TimerTask task) {

        // searching for good task number
        int taskNumber = getUnusedNumber("." + GTIMER_TASK_EXTENSION);
        task.setNumber(taskNumber);

        if (log.isDebugEnabled()) {
            log.debug("Setting task number for " + task.getName() + " to "
                    + taskNumber);
        }

        saveTask(task);

        // loop on subtasks to set correct task number (only on add)
        task.getSubTasks().forEach(this::addTask);
    }

    /**
     * Explore directory and find a non used number for project.
     *
     * @param extension to check (ie ".project" or ".task")
     *
     * @return a non used project number
     */
    protected int getUnusedNumber(String extension) {

        // init -1, ++ start with 0
        int foundProjectNumber = -1;

        File aGtimerFile;

        // en esperant que sera pas infini :)
        do {
            ++foundProjectNumber;
            aGtimerFile = new File(dataSaveDirectory + File.separator
                    + foundProjectNumber + extension);
        } while (aGtimerFile.exists());

        return foundProjectNumber;
    }

    @Override
    public void deleteProject(TimerProject project) {
        deleteTaskOrProject(project, GTIMER_PROJECT_EXTENSION);
    }

    @Override
    public void deleteTask(TimerTask task) {
        deleteTaskOrProject(task, GTIMER_TASK_EXTENSION);
    }

    /**
     * Delete associated file or project files on disk.
     *
     * @param taskOrProject task or project to delete file
     * @param extension extension without .
     */
    protected void deleteTaskOrProject(TimerTask taskOrProject, String extension) {

        // and then manage current file
        int fileNumber = taskOrProject.getNumber();

        // first, delete task annotation file
        if (extension.equals(GTIMER_TASK_EXTENSION)) {
            File annfileToDelete = new File(dataSaveDirectory + File.separator
                    + fileNumber + "." + GTIMER_ANNOTATION_EXTENSION);
            if (annfileToDelete.exists()) {
                annfileToDelete.delete();
                if (log.isDebugEnabled()) {
                    log.debug("Annotation file deleted for "
                            + taskOrProject.getName() + "("
                            + annfileToDelete.getPath() + ")");
                }
            }

            File alertFileToDelete = new File(dataSaveDirectory + File.separator
                    + fileNumber + "." + GTIMER_ALERT_EXTENSION);
            if (alertFileToDelete.exists()) {
                alertFileToDelete.delete();
                if (log.isDebugEnabled()) {
                    log.debug("Alert file deleted for "
                            + taskOrProject.getName() + "("
                            + alertFileToDelete.getPath() + ")");
                }
            }
        }

        // second, go recursively on subtasks
        for (TimerTask subtask : taskOrProject.getSubTasks()) {
            deleteTaskOrProject(subtask, GTIMER_TASK_EXTENSION);
        }

        // then manage current task file
        File fileToDelete = new File(dataSaveDirectory + File.separator
                + fileNumber + "." + extension);
        if (fileToDelete.exists()) {
            fileToDelete.delete();
            if (log.isDebugEnabled()) {
                log.debug("File deleted for " + taskOrProject.getName() + "("
                        + fileToDelete.getPath() + ")");
            }
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Try to delete non existing file "
                        + fileToDelete.getAbsolutePath());
            }
        }
    }

    @Override
    public void modifyProject(TimerProject project) {
        saveProject(project);
    }

    @Override
    public void modifyTask(TimerTask task) {

        // ne sauve la tache que si elle n'est pas
        // en cours d'execution
        if (!runningTasks.contains(task)) {
            saveTask(task);
        }

        // on a besoin de sauver les annotations
        // seulement dans le cas d'un merge
        // si le merge a merger les annotations par
        // exemple.
        // Potentiellement aussi lors d'un move.
        saveTaskAnnotation(task);
        saveAlerts(task);

        // fix a bug with the gtimer subtask
        // save format du to composed task name
        // subtasks have to be resaved
        task.getSubTasks().forEach(this::modifyTask);
    }

    @Override
    public void setAnnotation(TimerTask task, Date date, String annotation) {
        saveTaskAnnotation(task);
    }

    @Override
    public void setTaskTime(TimerTask task, Date date, Long time) {
        // ne sauve la tache que si elle n'est pas
        // en cours d'execution
        if (!runningTasks.contains(task)) {
            saveTask(task);
        }
    }

    @Override
    public void changeClosedState(TimerTask task) {
        if (task instanceof TimerProject) {
            saveProject((TimerProject) task);
        } else {
            // ne sauve la tache que si elle n'est pas
            // en cours d'execution
            if (!runningTasks.contains(task)) {
                saveTask(task);
            }
        }
    }

    @Override
    public void moveTask(TimerTask task) {

        if (log.isDebugEnabled()) {
            log.debug("postMoveTask event received");
        }

        modifyTask(task);
    }

    @Override
    public void postMergeTasks(TimerTask destinationTask, List<TimerTask> otherTasks) {
        // destination task need to be updated
        modifyTask(destinationTask);

        // delete all others
        // for (TimerTask otherTask : otherTasks) {
        // deleteTask(otherTask);
        // }
        // better if deleted here
        // but deleted by another event
        // some otherTasks subtasks must be deleted too
    }

    @Override
    public void startTask(TimerTask task) {

        if (log.isDebugEnabled()) {
            log.debug("startTask event received");
        }

        runningTasks.add(task);

    }

    @Override
    public void stopTask(TimerTask task) {

        if (log.isDebugEnabled()) {
            log.debug("stopTask event received");
        }

        // remove task from running task and force save
        runningTasks.remove(task);
        saveTask(task);
    }

    @Override
    public void run() {

        if (log.isDebugEnabled()) {
            log.debug("Saver wake up");
        }

        saveRunningTasks();
    }

    /**
     * Save runing tasks and their synchronization info
     */
    protected void saveRunningTasks() {
        synchronized (runningTasks) {
            runningTasks.forEach(this::saveTask);
        }
    }

    @Override
    public void checkAddProject(TimerProject project) {
        checkName(project);
    }

    @Override
    public void checkAddTask(TimerTask parent, TimerTask task) {
        checkName(task);
    }

    /**
     * Check task name.
     *
     * @param task task to check
     */
    protected void checkName(TimerTask task) {
        String name = task.getName();

        if (name.trim().length() <= 0) {
            throw new DataViolationException("Can't add task", "vetoable.saver.empty.name");
        }

        if (name.contains(GTIMER_SUBTASK_SEPARATOR)) {
            throw new DataViolationException("Can't add task", "vetoable.saver.invalid.characters");
        }
    }
}
