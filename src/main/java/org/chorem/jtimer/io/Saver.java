/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.io;

import org.chorem.jtimer.data.DataEventListener;
import org.chorem.jtimer.data.VetoableDataEventListener;
import org.chorem.jtimer.entities.TimerProject;

import java.util.Collection;

/**
 * Common interface for jtimer data savers.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public interface Saver extends DataEventListener, VetoableDataEventListener {

    /**
     * Set auto save delay (in milliseconds).
     *
     * @param autoSaveDelay delay in milliseconds
     */
    void setAutoSaveDelay(long autoSaveDelay);

    /**
     * Try to lock current saver directory.
     *
     * @throws DataLockingException if can't obtain lock
     */
    void lock() throws DataLockingException;

    /**
     * Try to unlock current saver directory.
     *
     * @throws DataLockingException if there is no lock
     */
    void unlock() throws DataLockingException;

    /**
     * Load a project list.
     *
     * @return a collection of projects
     */
    Collection<TimerProject> load();

}
