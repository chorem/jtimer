/*-
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.jtimer.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * Saver with backup support.
 */
public class BackupUtils {

    /** log. */
    private static Log log = LogFactory.getLog(BackupUtils.class);

    /** Backup file extension. */
    public static final String BACKUP_EXTENSION = ".tmp";

    /**
     * Make to backup of file if exists.
     * <p>
     * Copy file to filename + ".tmp"
     *
     * @param file file to backup
     * @return backup file or {@code null} if input file doesn't exist
     * @throws IOException
     */
    public static File makeBackupFile(File file) throws IOException {

        // if not exist, don't do anything
        if (!file.exists()) {
            return null;
        }

        File backupFile = new File(file.getAbsoluteFile() + BACKUP_EXTENSION);

        if (log.isDebugEnabled()) {
            log.debug("Backuping file " + file.getName() + " to " + backupFile.getName());
        }

        backupFile.delete();
        FileUtils.copyFile(file, backupFile);
        return backupFile;
    }

    /**
     * Rename backup file to original file name;
     *
     * @param backupFile backup file
     * @return if backup file has been restored
     */
    public static boolean restoreBackupFile(File backupFile) {

        String fileName = backupFile.getAbsolutePath();
        if (!fileName.endsWith(BACKUP_EXTENSION)) {
            throw new IllegalArgumentException("Not a valid backup file" + backupFile);
        }

        fileName = fileName.substring(0, fileName.length() - BACKUP_EXTENSION.length());
        File file = new File(fileName);

        if (log.isDebugEnabled()) {
            log.debug("Renaming " + backupFile.getName() + " to " + file.getName());
        }

        boolean done;
        try {
            Files.move(backupFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            done = true;
        } catch (IOException ex) {
            done = false;
        }
        return done;
    }

    /**
     * Delete backup file.
     * <p>
     * This function NEVER throw IOException.
     *
     * @param backupFile backup file (can be {@code null})
     */
    public static void deleteBackupFile(File backupFile) {
        if (backupFile != null) {
            backupFile.delete();
        }
    }
}
