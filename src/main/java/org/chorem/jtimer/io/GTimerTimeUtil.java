/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.io;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Util class to manipulate gTimer date format.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public class GTimerTimeUtil {

    /** Class log */
    private static Log log = LogFactory.getLog(GTimerTimeUtil.class);

    /** gtimer day string date format */
    protected static final DateFormat GTIMERDATEFORMAT = new SimpleDateFormat("yyyyMMdd");

    /**
     * Protected constructor.
     */
    protected GTimerTimeUtil() {

    }

    /**
     * Convert a yyyyMMdd date string to Calendar.
     *
     * @param dateAsString yyyyMMdd sting to convert
     * @return {@link Date}
     * @throws IllegalArgumentException
     */
    public static Date yyyyMMdd2Date(String dateAsString) {

        int year, mouth, day;
        Date date;

        try {
            String syear = dateAsString.substring(0, 4);
            String smonth = dateAsString.substring(4, 6);
            String sday = dateAsString.substring(6, 8);

            year = Integer.parseInt(syear);
            mouth = Integer.parseInt(smonth);
            day = Integer.parseInt(sday);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, mouth - 1);
            calendar.set(Calendar.DAY_OF_MONTH, day);

            date = calendar.getTime();
        } catch (NumberFormatException e) {
            if (log.isWarnEnabled()) {
                log.warn("Can't parse string " + dateAsString + " in yyyyMMdd format", e);
            }
            throw new IllegalArgumentException("Can't parse string " + dateAsString + " in yyyyMMdd format", e);
        }

        return date;
    }

    /**
     * Convert a calendar to yyyyMMdd date string.
     *
     * @param date date to convert
     * @return a string yyyyMMdd date
     * @throws IllegalArgumentException
     */
    public static String date2yyyyMMdd(Date date) {

        String dateAsString = GTIMERDATEFORMAT.format(date);

        return dateAsString;
    }
}
