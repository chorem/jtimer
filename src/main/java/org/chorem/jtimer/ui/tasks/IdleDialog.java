/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tasks;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.ui.NewTaskView;
import org.chorem.jtimer.ui.tree.ProjectsAndTasksTree;
import org.chorem.jtimer.ui.tree.TaskTreeModel;
import org.chorem.jtimer.ui.treetable.ProjectsAndTasksCellRenderer;
import org.jdesktop.application.Action;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceManager;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.Timer;

/**
 * Modal idle dialog showed to user when idle has been detected.
 *
 * Composed of an unique blocking show method.
 *
 * Also composed of three resume option :
 * <ul>
 *  <li>Stop task</li>
 *  <li>Continue (with idle time summed)</li>
 *  <li>Resume (without idle time summed)</li>
 *  <li>Set time to another task</li>
 * </ul>
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class IdleDialog extends JDialog {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7669429291708466753L;

    /** Dialog response type. */
    public enum IdleOption {
        REVERT, CONTINUE, RESUME, ASSIGN
    }

    /** log */
    private static Log log = LogFactory.getLog(IdleDialog.class);

    /** Mutex object (multiples running tasks are waiting on it) */
    protected static final Object mutex = new Object();

    /** Singleton dialog instance. */
    protected static IdleDialog idleDialog;

    /** Resume option. */
    protected static IdleOption lastResumeOption;

    /** Selected task (only valid for assign option). */
    protected static TimerTask assignSelectedTask;

    /** Parent application. */
    protected JTimer application;

    /** Timer core. */
    protected TimerCore core;

    /** I18N resource map. */
    protected ResourceMap resourceMap;

    /** Timer (for idle duration scheduling). */
    protected Timer timer;

    /** Current Idle duration refresh task. */
    protected UpdateIdleTime updateIdleTime;

    /** Timestamp when idle starts. */
    protected long idleStartTimestamp;

    /** Current running task. */
    protected TimerTask currentRunningTask;

    /** Task label. */
    protected JLabel taskNameLabel;

    /** Duration label. */
    protected JLabel idleDurationLabel;

    /** Assign toogle button. */
    protected JToggleButton assignButton;

    /** Assign panel (non visible by default). */
    protected JPanel assignPanel;

    /** Assign project tree. */
    protected ProjectsAndTasksTree projectsTree;

    /** Task selected in tree. */
    protected boolean selectedTask;

    /** Task selected in tree (other than current running task). */
    protected boolean selectedOtherTask;

    /**
     * IdleDialog constructor.
     *
     * Protected to force use of show static method.
     *
     * @param application parent application
     * @param core timer core
     */
    protected IdleDialog(JTimer application, TimerCore core) {
        // don't make reference on other parent
        // windows, cause idle to unlock some task
        // if parent window is hidden by systray
        this.core = core;

        // init resources map
        this.application = application;
        ApplicationContext ctxt = application.getContext();
        ResourceManager mgr = ctxt.getResourceManager();
        resourceMap = mgr.getResourceMap(IdleDialog.class);

        // remove dialog name to this dialog, due to bug
        // http://chorem.org/issues/484 and
        // http://kenai.com/jira/browse/BSAF-107
        // cause infinite loop under openjdk
        setName(null);

        setTitle(resourceMap.getString("idleTitle"));
        try {
            setIconImage(ImageIO.read(getClass().getResource("/org/chorem/jtimer/resources/jtimer-40-orange.png")));
        } catch (IOException e) {
            // ignored
        }
        setResizable(false);
        setModal(true);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        getRootPane().setLayout(new BorderLayout(1, 1));
        getRootPane().add(getMainComponent(), BorderLayout.CENTER);

        // timer
        timer = new Timer();
    }

    /**
     * Build main component UI.
     *
     * @return component ui.
     */
    private JComponent getMainComponent() {
        JPanel mainComponent = new JPanel(new GridBagLayout());

        // label
        JLabel idleIcon = new JLabel(resourceMap.getIcon("idleIcon"));
        mainComponent.add(idleIcon, new GridBagConstraints(0, 0, 1, 3, 0, 1,
                GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(10, 5, 10, 10), 0, 0));

        // label
        JLabel idleLabel = new JLabel(resourceMap.getString("idleMessage", JTimer.config.getIdleTime() / 60));
        mainComponent.add(idleLabel, new GridBagConstraints(1, 0, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 3), 0, 0));

        taskNameLabel = new JLabel(" ");
        mainComponent.add(taskNameLabel, new GridBagConstraints(1, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 3, 5), 0, 0));

        idleDurationLabel = new JLabel(" ");
        mainComponent.add(idleDurationLabel, new GridBagConstraints(1, 2, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 3, 5), 0, 0));

        // separator
        mainComponent.add(new JSeparator(), new GridBagConstraints(0, 3, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));

        // label
        JLabel idleRestart = new JLabel(resourceMap.getString("idleRestart"));
        mainComponent.add(idleRestart, new GridBagConstraints(0, 4, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 5, 0, 5), 0, 0));

        // buttons
        JPanel optionPanel = getOptionPanel();
        mainComponent.add(optionPanel, new GridBagConstraints(0, 5, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(3, 5, 5, 5), 0, 0));

        // tasks tree
        assignPanel = getAssignPanel();
        mainComponent.add(assignPanel, new GridBagConstraints(0, 6, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(3, 5, 5, 0), 0, 0));

        return mainComponent;
    }

    /**
     * Build option panel with a GridLayout for all button to get same size.
     *
     * @return option panel
     */
    protected JPanel getOptionPanel() {
        JPanel optionPanel = new JPanel(new GridLayout(0, 4, 2, 0));

        JButton revertButton = new JButton();
        revertButton.setAction(application.getContext().getActionMap(this).get("chooseRevertOption"));
        revertButton.setHorizontalTextPosition(SwingConstants.CENTER);
        revertButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        optionPanel.add(revertButton);

        JButton continueButton = new JButton();
        continueButton.setAction(application.getContext().getActionMap(this).get("chooseContinueOption"));
        continueButton.setHorizontalTextPosition(SwingConstants.CENTER);
        continueButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        optionPanel.add(continueButton);

        JButton resumeButton = new JButton();
        resumeButton.setAction(application.getContext().getActionMap(this).get("chooseResumeOption"));
        resumeButton.setHorizontalTextPosition(SwingConstants.CENTER);
        resumeButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        optionPanel.add(resumeButton);

        assignButton = new JToggleButton();
        assignButton.setAction(application.getContext().getActionMap(this).get("chooseAssignOption"));
        assignButton.setHorizontalTextPosition(SwingConstants.CENTER);
        assignButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        optionPanel.add(assignButton);

        return optionPanel;
    }

    /**
     * Build assign panel.
     *
     * @return assign panel
     */
    protected JPanel getAssignPanel() {
        JPanel panel = new JPanel(new GridBagLayout());

        // separator
        panel.add(new JSeparator(), new GridBagConstraints(0, 0, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));

        // label
        JLabel labelIdle = new JLabel();
        labelIdle.setText(resourceMap.getString("assignIdleTimeTo"));
        panel.add(labelIdle, new GridBagConstraints(0, 1, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 0, 0, 5), 0, 0));

        // Tree
        projectsTree = new ProjectsAndTasksTree();
        projectsTree.setRootVisible(false);
        projectsTree.setShowsRootHandles(true);
        projectsTree.setModel(new TaskTreeModel(core, null));
        projectsTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        projectsTree.setCellRenderer(new ProjectsAndTasksCellRenderer());
        projectsTree.setScrollsOnExpand(true);
        projectsTree.addTreeSelectionListener(e -> {
            TreePath path = e.getPath();
            if (path.getPathCount() > 2) {
                assignSelectedTask = (TimerTask) path.getLastPathComponent();
            } else {
                assignSelectedTask = null;
            }
            // must select a task and a different one from one running
            setSelectedTask(assignSelectedTask != null);
            // must select a task and a different one from one running
            setSelectedOtherTask(assignSelectedTask != null && !assignSelectedTask.equals(currentRunningTask));
        });
        panel.add(new JScrollPane(projectsTree), new GridBagConstraints(0, 2, 2, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 3, 5), 0, 0));

        // new task
        JButton newTaskButton = new JButton();
        newTaskButton.setAction(application.getContext().getActionMap(this).get("newTaskOption"));
        panel.add(newTaskButton, new GridBagConstraints(0, 3, 1, 1, 0, 0,
                GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

        // ok
        JButton assignValidButton = new JButton();
        assignValidButton.setAction(application.getContext().getActionMap(this).get("validAssignOption"));
        panel.add(assignValidButton, new GridBagConstraints(1, 3, 1, 1, 0, 0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));

        return panel;
    }

    public boolean isSelectedTask() {
        return selectedTask;
    }

    public void setSelectedTask(boolean selectedTask) {
        boolean oldValue = this.selectedTask;
        this.selectedTask = selectedTask;
        firePropertyChange("selectedTask", oldValue, selectedTask);
    }

    public boolean isSelectedOtherTask() {
        return selectedOtherTask;
    }

    public void setSelectedOtherTask(boolean selectedOtherTask) {
        boolean oldValue = this.selectedOtherTask;
        this.selectedOtherTask = selectedOtherTask;
        firePropertyChange("selectedOtherTask", oldValue, selectedOtherTask);
    }

    /**
     * Init dialog idleDialog instance.
     *
     * @param parent parent reference
     * @param core core reference
     */
    public static void init(JTimer parent, TimerCore core) {
        idleDialog = new IdleDialog(parent, core);
    }

    /**
     * Revert button action.
     */
    @Action
    public void chooseRevertOption() {
        lastResumeOption = IdleOption.REVERT;
        idleEnded();
    }

    /**
     * Continue button action.
     */
    @Action
    public void chooseContinueOption() {
        lastResumeOption = IdleOption.CONTINUE;
        idleEnded();
    }

    /**
     * Resume button action.
     */
    @Action
    public void chooseResumeOption() {
        lastResumeOption = IdleOption.RESUME;
        idleEnded();
    }

    /**
     * Assign button action.
     */
    @Action
    public void chooseAssignOption() {
        assignPanel.setVisible(assignButton.isSelected());
        if (assignButton.isSelected()) {
            //projectsTree.expandRow(0); // all hidden without this
            projectsTree.expandPath(new TreePath(projectsTree.getModel().getRoot()));
        }
        this.pack();
    }

    /**
     * Action when creating a new sub task.
     */
    @Action(enabledProperty = "selectedTask")
    public void newTaskOption() {
        // select task to add new task
        TreePath selectionPath = projectsTree.getSelectionPath();
        TimerTask selectedTask = (TimerTask) selectionPath.getLastPathComponent();

        NewTaskView newTaskPanel = new NewTaskView(this, application, core, selectedTask);
        application.show(newTaskPanel);

        TimerTask newTask = newTaskPanel.getTask();
        if (newTask != null) {
            TaskTreeModel model = (TaskTreeModel)projectsTree.getModel();
            int newIndex = model.getIndexOfChild(selectedTask, newTask);
            model.getModelSupport().fireChildAdded(selectionPath, newIndex, newTask);
            projectsTree.expandPath(selectionPath);
            projectsTree.setSelectedTask(newTask);
        }
    }

    /**
     * Action when validating assign option.
     */
    @Action(enabledProperty = "selectedOtherTask")
    public void validAssignOption() {
        lastResumeOption = IdleOption.ASSIGN;
        idleEnded();
    }

    /**
     * Reset some part of the UI.
     */
    protected void reset() {
        assignButton.setSelected(false);
        assignPanel.setVisible(assignButton.isSelected());
        this.pack();
    }

    /**
     * Unblock all waiting threads on {@link #mutex}.
     */
    protected void idleEnded() {
        synchronized (mutex) {
            mutex.notifyAll();
        }
        setVisible(false);
    }

    @Override
    public void setVisible(boolean b) {

        if (b) {
            // get running task
            ApplicationContext ctxt = application.getContext();
            TaskMonitor tm = ctxt.getTaskMonitor();
            Task<?, ?> t = tm.getTasks().get(0);
            TimerTask currentTask = ((RunTaskJob) t).getTask();

            // update selected task related
            this.currentRunningTask = currentTask;
            taskNameLabel.setText(resourceMap.getString("currentTask", currentTask.getName()));
            projectsTree.setSelectedTask(currentTask);

            // schedule timer
            updateIdleTime = new UpdateIdleTime();
            timer.schedule(updateIdleTime, 0, 1000 * 60 /* every minutes */);

            reset();
        } else {
            updateIdleTime.cancel();
            timer.purge();
        }

        super.setVisible(b);
    }

    /**
     * Called function on idle detect.
     *
     * This function is thread safe and show only one dialog even if
     * function is called multiples times.
     *
     * @param idleStartTimestamp time stamp when idle start
     * @return idle option
     */
    public static IdleOption showIdleDialog(long idleStartTimestamp) {

        // only the first call must display dialog
        // (can happen if multiples task are running)
        boolean mustShow;
        synchronized (idleDialog) {
            mustShow = !idleDialog.isVisible();
        }

        // the first frame will be blocked by show modal
        // blocking status...
        if (mustShow) {
            idleDialog.idleStartTimestamp = idleStartTimestamp;
            idleDialog.application.show(idleDialog);
        } else {
            // ...the others by wait();
            synchronized (mutex) {
                try {
                    mutex.wait();
                } catch (InterruptedException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Thread interrupted", e);
                    }
                }
            }
        }

        return lastResumeOption;
    }

    /**
     * Task to update idle time duration while dialog is showed.
     */
    protected class UpdateIdleTime extends java.util.TimerTask {

        @Override
        public void run() {
            if (log.isDebugEnabled()) {
                log.debug("Update idle duration");
            }

            // update message
            String duration = DurationFormatUtils.formatDuration(
                    System.currentTimeMillis() - idleStartTimestamp, "HH:mm");
            idleDurationLabel.setText(resourceMap.getString("idleDuration", duration));
        }
    }
}
