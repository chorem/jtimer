/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tasks;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;

import java.util.Date;
import java.util.List;

/**
 * Task used to refresh tree at midnight.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class RefreshTreeTask extends java.util.TimerTask {

    /** log */
    private static Log log = LogFactory.getLog(RefreshTreeTask.class);

    /** Data manager */
    protected TimerDataManager dataManager;

    /**
     * Constructor with core.
     *
     * @param core core
     */
    public RefreshTreeTask(TimerCore core) {
        dataManager = core.getData();
    }

    /*
     * @see java.util.TimerTask#run()
     */
    @Override
    public void run() {

        log.info("Refresh tree");

        List<TimerProject> projects = dataManager.getProjectsList();

        // don't call changeTaskTime on projects
        for (TimerProject project : projects) {
            refreshTasks(project.getSubTasks());
        }
    }

    /**
     * Refresh task list.
     *
     * @param tasks tasks list
     */
    protected void refreshTasks(List<TimerTask> tasks) {

        for (TimerTask task : tasks) {

            // first refresh sub tasks
            refreshTasks(task.getSubTasks());

            // fire event
            Date now = new Date();
            // TODO find a better way to do that !
            // Only useful for UI listeners
            dataManager.changeTaskTime(task, now, task.getTime(now));
        }
    }

}
