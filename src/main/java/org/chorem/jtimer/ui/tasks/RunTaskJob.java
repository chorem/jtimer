/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tasks;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerAlert;
import org.chorem.jtimer.entities.TimerAlert.Type;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.chorem.jtimer.system.SystemInfo;
import org.chorem.jtimer.system.SystemInfoFactory;
import org.chorem.jtimer.ui.tasks.IdleDialog.IdleOption;
import org.jdesktop.application.Task;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * RunTaskJob.
 *
 * Notify every second the core controler.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class RunTaskJob extends Task<Void, Void> {

    /** Class logger. */
    private static Log log = LogFactory.getLog(RunTaskJob.class);

    /** Parent app reference. */
    protected JTimer parentApp;

    /** The task to manage. */
    protected TimerTask managedTask;

    /** Data manager. */
    protected TimerDataManager dataManager;

    /** System information (idle...). */
    protected SystemInfo systemInfo;

    /** Already thrown alert. */
    protected Collection<TimerAlert> alreadyThrownAlerts;

    /** Last publish time to detect hibernate. */
    protected AtomicLong lastUserActivity;

    /** Want to stop flag. */
    protected final AtomicBoolean bWantToStop;

    /**
     * Constructor.
     *
     * Take the task to manage
     *
     * @param parentApp parent application
     * @param managedTask task
     * @param dataManager data
     */
    public RunTaskJob(JTimer parentApp, TimerTask managedTask,
                      TimerDataManager dataManager) {
        super(parentApp);
        this.parentApp = parentApp;

        // init with False
        bWantToStop = new AtomicBoolean(false);

        // save vars
        this.managedTask = managedTask;
        this.dataManager = dataManager;

        // must be init here, checkAlreadyThrownAlerts is called multiples times
        alreadyThrownAlerts = new HashSet<>();
        checkAlreadyThrownAlerts(managedTask);

        // init system info to get idleTime
        systemInfo = SystemInfoFactory.getSystemInfo().orElse(null);
    }

    /**
     * Check for already thrown alerts.
     *
     * Used to not fired again already fired alerts during previous
     * timing.
     *
     * @param task current task
     */
    protected void checkAlreadyThrownAlerts(TimerTask task) {

        Date now = new Date();
        // check alert to be fired
        for (TimerAlert alert : task.getAlerts()) {
            if (alert.getType().equals(Type.REACH_DAILY_TIME)
                    && TimerTaskHelper.getTotalTime(task, now) > alert
                    .getDuration()) {
                alreadyThrownAlerts.add(alert.clone());
            } else if (alert.getType().equals(Type.REACH_TOTAL_TIME)
                    && TimerTaskHelper.getAllTotalTime(task) > alert
                    .getDuration()) {
                alreadyThrownAlerts.add(alert.clone());
            }
        }

        // se souvient aussi de alerte deja lancées
        // pour les taches parentes
        if (task.getParent() != null) {
            checkAlreadyThrownAlerts(task.getParent());
        }

    }

    /**
     * Task getter.
     *
     * @return managed task
     */
    public TimerTask getTask() {
        return managedTask;
    }

    @Override
    protected Void doInBackground() throws Exception {

        // task effective start
        // notify ui
        parentApp.startedTask(managedTask);

        // init
        lastUserActivity = new AtomicLong(System.currentTimeMillis());

        // get idle time
        long configIdleTime = JTimer.config.getIdleTime() * 1000;

        boolean dontWantToStop = true;
        while (dontWantToStop) {

            // this algorithm is based on this atomic operation
            // that ensure hibernate won't break time tracking
            long oldUserActivity = lastUserActivity.getAndSet(System.currentTimeMillis());

            // check for idleness with last user activity (real idle and hibernate)
            long delta = lastUserActivity.get() - oldUserActivity;

            // time have to be published, even if idle is detected
            // because time adjustment will remove more time than published
            // one if done after
            addTaskDelta(managedTask, lastUserActivity.get(), delta);

            // check user idle time
            long idleTime = 0;
            if (systemInfo != null) { // idle time available
                idleTime = systemInfo.getIdleTime();

                if (log.isDebugEnabled()) {
                    log.debug("User is idle since " + (idleTime / 1000) + " s");
                }
            }

            if (delta >= configIdleTime || idleTime >= configIdleTime) {

                long userIdleTime = Math.max(delta, idleTime);

                // remove delta from now
                addTaskDelta(managedTask, lastUserActivity.get() + delta, -userIdleTime);

                // display idle detected (blocking call)
                JTimer parentApplication = (JTimer) getApplication();
                parentApplication.preIdleDetect();
                IdleOption option = IdleDialog.showIdleDialog(lastUserActivity.get() - userIdleTime);
                parentApplication.postIdleDetect();

                // restart timing from current time after idle
                // mandatory call, otherwise, next iteration will display idle
                // dialog again
                oldUserActivity = lastUserActivity.getAndSet(System.currentTimeMillis());
                delta = lastUserActivity.get() - oldUserActivity;

                switch (option) {
                    case REVERT:
                        // just stop the task
                        ((JTimer) getApplication()).stopTask(managedTask);
                        break;

                    case CONTINUE:
                        // readd delta since oldUserActivity
                        addTaskDelta(managedTask, oldUserActivity - userIdleTime, delta + userIdleTime);
                        break;

                    case ASSIGN:
                        // readd delta since oldUserActivity to another task
                        TimerTask otherTask = IdleDialog.assignSelectedTask;
                        addTaskDelta(otherTask, oldUserActivity - userIdleTime, delta + userIdleTime);
                        break;
                }
            }

            Thread.sleep(1000); // 1s

            // test if task want to stop
            synchronized (bWantToStop) {
                dontWantToStop = !bWantToStop.get();
            }
        }

        return null;
    }

    /**
     * Add task delta.
     *
     * @param task task to apply delta
     * @param from time when delta was notified
     * @param delta delta to add
     */
    protected void addTaskDelta(TimerTask task, long from, long delta) {

        long localDelta = delta;

        if (log.isDebugEnabled()) {
            log.debug("Adding delta to task : " + delta);
        }

        // remove time
        if (localDelta < 0) {
            Date currentDate = new Date(from);
            while (localDelta < 0) {
                Date todayMidnight = DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH);
                long msToMidnight = currentDate.getTime() - todayMidnight.getTime();
                long toRemove = Math.min(-localDelta, msToMidnight);
                // FIXME there is a little bug in the algorithm. Can set a time to -1 in some case
                long newTaskTime = Math.max(0, task.getTime(currentDate) - toRemove);
                dataManager.changeTaskTime(task, currentDate, newTaskTime);
                if (log.isDebugEnabled()) {
                    log.debug(" remove delta to task on " + currentDate + " : " + toRemove);
                }
                localDelta += toRemove;
                currentDate = DateUtils.addMilliseconds(todayMidnight, -1);
            }
        } else {
            // add time
            Date currentDate = new Date(from);
            while (localDelta > 0) {
                Date today235959 = DateUtils.truncate(currentDate, Calendar.DAY_OF_MONTH);
                today235959 = DateUtils.addDays(today235959, 1);
                today235959 = DateUtils.addMilliseconds(today235959, -1);
                long msToMidnight = today235959.getTime() - currentDate.getTime();
                long toAdd = Math.min(localDelta, msToMidnight);
                dataManager.changeTaskTime(task, currentDate, task.getTime(currentDate) + toAdd);
                if (log.isDebugEnabled()) {
                    log.debug(" adding delta to task on " + currentDate + " : " + toAdd);
                }
                localDelta -= toAdd;
                currentDate = DateUtils.addMilliseconds(today235959, 1);
            }
        }

        checkTaskAlerts(task);
    }

    /**
     * Check for alert to be fired.
     *
     * @param task
     */
    protected void checkTaskAlerts(TimerTask task) {
        Date now = new Date();

        // check alert to be fired
        task.getAlerts().stream().filter(alert -> !alreadyThrownAlerts.contains(alert)).forEach(alert -> {
            if (alert.getType().equals(Type.REACH_DAILY_TIME)
                    && TimerTaskHelper.getTotalTime(task, now) >= alert
                    .getDuration()) {
                displayAlert(task, Type.REACH_DAILY_TIME,
                        alert.getDuration());
                alreadyThrownAlerts.add(alert.clone());
            } else if (alert.getType().equals(Type.REACH_TOTAL_TIME)
                    && TimerTaskHelper.getAllTotalTime(task) >= alert
                    .getDuration()) {
                displayAlert(task, Type.REACH_TOTAL_TIME,
                        alert.getDuration());
                alreadyThrownAlerts.add(alert.clone());
            }
        });

        // lance aussi les alertes sur les taches parentes
        // par exemple, si une tache passe en temps journaliers a 1h
        // sont parent y passe aussi, donc les alertes doivent être levée.
        if (task.getParent() != null) {
            checkTaskAlerts(task.getParent());
        }
    }

    /**
     * Display alert message without breaking UI.
     *
     * @param task task
     * @param alertType alert type
     * @param alertDuration alert duration
     */
    protected void displayAlert(final TimerTask task, final Type alertType,
                                final long alertDuration) {
        SwingUtilities.invokeLater(() -> {
            String alertMessage = null;
            String formattedTime = DurationFormatUtils.formatDuration(
                    alertDuration, "HH:mm:ss");
            if (Type.REACH_DAILY_TIME.equals(alertType)) {
                alertMessage = getResourceMap().getString(
                        "alert.dailyAlertMessage", task.getName(),
                        formattedTime);
            } else if (Type.REACH_TOTAL_TIME.equals(alertType)) {
                alertMessage = getResourceMap().getString(
                        "alert.totalAlertMessage", task.getName(),
                        formattedTime);
            }

            // do not make reference to main frame (can be hiden)
            JOptionPane.showMessageDialog(null, alertMessage,
                    getResourceMap().getString("alert.title"),
                    JOptionPane.INFORMATION_MESSAGE, getResourceMap()
                            .getIcon("alert.alertIcon"));
        });
    }

    /**
     * Notify that task want to stop
     */
    public void wantToStop() {
        synchronized (bWantToStop) {
            bWantToStop.set(true);
        }
    }

    /**
     * Tell if task is trying to stop.
     *
     * @return stopping flag
     */
    public boolean isStopping() {
        boolean stopping;
        synchronized (bWantToStop) {
            stopping = bWantToStop.get();
        }
        return stopping;
    }
}
