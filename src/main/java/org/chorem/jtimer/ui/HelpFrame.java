/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Help window.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public class HelpFrame extends FrameView implements HyperlinkListener {

    /** log */
    private static Log log = LogFactory.getLog(HelpFrame.class);

    /**
     * Constructor.
     *
     * Build UI.
     *
     * @param application parent application
     */
    public HelpFrame(Application application) {
        super(application);

        // rename frame to get proper position
        getFrame().setName("helpFrame");

        // set title
        getFrame().setTitle(getResourceMap().getString("aboutTitle"));

        // build UI
        setComponent(getMainComponent());
    }

    /**
     * Get UI main component.
     *
     * @return view's main component
     */
    protected JComponent getMainComponent() {

        JPanel mainComponent = new JPanel(new GridBagLayout());

        // image
        Icon logoIcon = getResourceMap().getIcon("aboutIcon");
        JLabel labelIcon = new JLabel(logoIcon);
        mainComponent.add(labelIcon, new GridBagConstraints(0, 0, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(1, 1, 1, 1), 0, 0));

        JTabbedPane tabPanel = new JTabbedPane();
        // first panel
        Component firstTab = getAboutTab();
        tabPanel.add(getResourceMap().getString("aboutHtmlTitle"), firstTab);

        // second panel
        Component secondTab = getLicenseTab();
        tabPanel.add(getResourceMap().getString("aboutLicenseTitle"), secondTab);

        mainComponent.add(tabPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
                0, 5, 5, 5), 0, 0));

        // ok button
        JButton closeButton = new JButton();
        closeButton.setAction(getContext().getActionMap(this).get("closeView"));
        mainComponent.add(closeButton, new GridBagConstraints(0, 2, 1, 1, 1, 0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0,
                5, 5, 5), 20, 0));

        // fix resizing
        getFrame().setResizable(false);

        // fix background
        mainComponent.setBackground(Color.WHITE);

        return mainComponent;
    }

    /**
     * Build license tab.
     *
     * @return license tab component
     */
    protected Component getLicenseTab() {

        JTextArea licenseArea = new JTextArea();
        licenseArea.setLineWrap(true);
        licenseArea.setWrapStyleWord(true);
        licenseArea.setText(getResourceMap().getString("aboutLicense"));
        licenseArea.setEditable(false);
        return licenseArea;
    }

    /**
     * Build about tab.
     *
     * @return about tab component
     */
    protected Component getAboutTab() {

        JEditorPane htmlAbout = new JEditorPane("text/html", getResourceMap()
                .getString("aboutHtml"));
        htmlAbout.addHyperlinkListener(this);
        htmlAbout.setEditable(false);
        return htmlAbout;
    }

    /**
     * Close action.
     */
    @Action
    public void closeView() {
        getApplication().hide(this);
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent he) {
        if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED
                && Desktop.isDesktopSupported()) {

            try {
                URL u = he.getURL();
                if (u.getProtocol().equalsIgnoreCase("mailto")
                        || u.getProtocol().equalsIgnoreCase("http")
                        || u.getProtocol().equalsIgnoreCase("ftp")) {
                    Desktop.getDesktop().browse(u.toURI());
                }
            } catch (IOException | URISyntaxException e) {
                if (log.isErrorEnabled()) {
                    log.error("Error while opening link", e);
                }
            } catch (UnsupportedOperationException e) {
                if (log.isWarnEnabled()) {
                    log.warn("Cannot open link (maybe default browser in not configured ?)");
                }
                if (log.isDebugEnabled()) {
                    log.debug("Error while opening link", e);
                }
            }
        }
    }

}
