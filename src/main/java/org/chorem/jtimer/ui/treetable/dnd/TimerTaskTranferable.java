/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.treetable.dnd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.entities.TimerTask;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

/**
 * Represent transfered data.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public class TimerTaskTranferable implements Transferable {

    /** log */
    private static Log log = LogFactory.getLog(TimerTaskTranferable.class);

    /** Data transfer mime type */
    protected static final String MIMETYPE = DataFlavor.javaJVMLocalObjectMimeType
            + ";class=" + TimerTask.class.getName();

    /** Data instance */
    protected static DataFlavor myData;

    /** Tasks to transfer. Chosen implementation must be serializable. */
    protected List<TimerTask> tasksToTransfer;

    /**
     * Constructor.
     *
     * @param tasks task to transfer
     */
    public TimerTaskTranferable(List<TimerTask> tasks) {

        // save task
        this.tasksToTransfer = tasks;

        // build new DataFlavor
        try {
            myData = new DataFlavor(MIMETYPE);
        } catch (ClassNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Class not found", e);
            }
        }
    }

    @Override
    public Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException, IOException {

        List<TimerTask> tasks;

        if (flavor == null) {
            throw new IOException("flavor is null");
        }

        if (flavor.equals(myData)) {
            tasks = tasksToTransfer;
        } else {
            throw new UnsupportedFlavorException(flavor);
        }

        return tasks;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{myData};
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor.equals(myData);
    }
}
