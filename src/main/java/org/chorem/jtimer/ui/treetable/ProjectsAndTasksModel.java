/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.treetable;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.DataEventListener;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Cette classe gere l'affichage d'une liste de project dans l'arbre/table
 *
 * Cela permet au programme principal de manipuler la liste et d'oublier la
 * gestion de l'arbre...
 *
 * Le modele de cet arbre utilise directement des TimerTask au lieu des TreeTableNode
 * qui ne servent a rien et qui complique tout. Par contre, les tâches étant
 * les mêmes instances que celle manipulées par le programme, il doit mettre
 * en cache certains infos pour un rafraichissement correct:
 * <ul>
 *  <li>Nom : cas de renommage avec changement d'ordre
 *  <li>Sous tache : pour supprimer une tache qui disparaitrait directement du modèle
 * </ul>
 *
 * Voir cet article pour la non utilisation des TreeNode :
 * http://www.javalobby.org/forums/thread.jspa?threadID=16052&tstart=0
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date: 2008-06-13 11:05:57 +0200 (ven., 13 juin 2008)
 * $ By : $Author$
 */
public class ProjectsAndTasksModel extends AbstractTreeTableModel implements
        DataEventListener {

    /** log */
    private static Log log = LogFactory.getLog(ProjectsAndTasksModel.class);

    /** Add node operation. */
    public static final int OPERATION_ADD = 0;
    /** Modify node operation. */
    public static final int OPERATION_MODIFY = 1;
    /** Delete Node operation. */
    public static final int OPERATION_DELETE = 2;

    /** Tree managed by this model. */
    protected ProjectsAndTasksTable projectsAndTaskTable;

    /** Data. */
    protected TimerDataManager dataManager;

    /** Tree column identifiers. */
    protected List<String> columnIdentifiers;

    /** Show closed task property. Default to false. */
    protected boolean showClosedTask;

    protected Map<TimerTask, String> taskNameCache;
    protected Map<Object, List<TimerTask>> subTasksCache;

    /**
     * Constructor.
     *
     * @param projectsAndTaskTable table
     * @param core timer core
     * @param columnIdentifiers column identifiers
     */
    public ProjectsAndTasksModel(ProjectsAndTasksTable projectsAndTaskTable, TimerCore core,
                                 List<String> columnIdentifiers) {
        super(new TimerProject("root"));

        // remember
        this.projectsAndTaskTable = projectsAndTaskTable;
        this.dataManager = core.getData();
        this.columnIdentifiers = columnIdentifiers;

        taskNameCache = new HashMap<>();
        subTasksCache = new HashMap<>();
    }

    @Override
    public int getColumnCount() {
        return columnIdentifiers.size();
    }

    @Override
    public String getColumnName(int column) {
        return columnIdentifiers.get(column);
    }

    @Override
    public Object getValueAt(Object object, int column) {

        // default value, but normally never visible
        String value = "n/a";

        if (object instanceof TimerTask) {
            TimerTask task = (TimerTask) object;

            switch (column) {
                case 0:
                    value = task.getName();
                    break;
                case 1:
                    long duration = TimerTaskHelper.getTotalTime(task, new Date());
                    value = DurationFormatUtils.formatDuration(duration, "HH:mm:ss");
                    break;
                case 2:
                    long totalDuration = TimerTaskHelper.getAllTotalTime(task);
                    value = DurationFormatUtils.formatDuration(totalDuration, "HH:mm:ss");
                    break;
            }
        } else {
            // means that it's tree root
            // or error
            if (log.isTraceEnabled()) {
                log.trace("bad user object " + object);
            }
        }

        return value;
    }

    @Override
    public boolean isCellEditable(Object object, int column) {
        // non editable
        return false;
    }

    @Override
    public Object getChild(Object parent, int index) {

        TimerTask t = getFiteredSubListFor(parent).get(index);
        return t;
    }

    @Override
    public int getChildCount(Object parent) {

        int childCount = getFiteredSubListFor(parent).size();
        return childCount;
    }

    /**
     * Recupere la sous liste: data.getProjectsList() si parent = root
     * getSubTasks() sinon (cache results).
     *
     * @param parent parent to task sublist
     * @return filtered list
     */
    protected List<TimerTask> getFiteredSubListFor(Object parent) {
        return getFiteredSubListFor(parent, false);
    }

    /**
     * Recupere la sous liste: data.getProjectsList() si parent = root
     * getSubTasks() sinon.
     *
     * @param parent parent to task sublist
     * @param noCache disable use of cached result and result caching
     * @return filtered list
     */
    protected List<TimerTask> getFiteredSubListFor(Object parent, boolean noCache) {

        List<TimerTask> result = subTasksCache.get(parent);
        if (result == null || noCache) {
            result = new ArrayList<>();

            // get correct list
            if (parent == root) { // case root node
                List<TimerProject> projects = dataManager.getProjectsList();
                result.addAll(projects);
            } else { // not root node
                TimerTask task = (TimerTask) parent;
                result.addAll(task.getSubTasks());
            }

            // filter list, if only show closed
            if (!showClosedTask) {
                result = result.stream()
                        .filter(task -> !task.isClosed())
                        .collect(Collectors.toList());
            }

            // Since sort is not supported by the table, do a manual sorting.
            result = TimerTaskHelper.sortTask(result);

            if (!noCache) {
                // cache tasks name
                for (TimerTask task : result) {
                    taskNameCache.put(task, task.getName());
                }

                subTasksCache.put(parent, result);
            }
        }

        return result;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {

        int childIndex = getFiteredSubListFor(parent).indexOf(child);
        return childIndex;
    }

    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    /**
     * Notification de changement de donnees.
     *
     * @param task task to update
     * @param operation operation
     */
    protected void notifyTaskChanged(TimerTask task, int operation) {
        // this.modelSupport.
        TreePath path = new TreePath(root);
        SwingUtilities.invokeLater(() -> updateChildren(task, path, operation));
    }

    /**
     * Recursively try to update node.
     *
     * @param task the task to update
     * @param path current path
     * @param operation operation (add, modify, delete)
     * @return <tt>true</tt> if children have been found and updated in current recursion
     */
    protected synchronized boolean updateChildren(TimerTask task, TreePath path, int operation) {
        TimerTask pathLastComponent = (TimerTask) path.getLastPathComponent();

        boolean updated = false;
        // get childreen without cache in case of add operation
        // delete operation MUST use cached result
        List<TimerTask> subTask = getFiteredSubListFor(pathLastComponent, operation == OPERATION_ADD);
        int childCount = subTask.size();
        for (int childIndex = 0; !updated && childIndex < childCount; ++childIndex) {
            TimerTask taskUO = subTask.get(childIndex);

            if (task.equals(taskUO)) {

                // this update only node, not all path...
                switch (operation) {

                    case OPERATION_ADD: {
                        subTasksCache.remove(pathLastComponent); // force child recache
                        int newIndex = getIndexOfChild(pathLastComponent, taskUO);
                        modelSupport.fireChildAdded(path, newIndex, taskUO);

                        // prevent side effect where tree branch is totally not visible
                        if (projectsAndTaskTable.isExpanded(path.getParentPath())) {
                            // expand path
                            projectsAndTaskTable.expandPath(path);
                        }
                        break;
                    }
                    case OPERATION_DELETE: {
                        // before cache invalidate (use index of himself)
                        modelSupport.fireChildRemoved(path, childIndex, taskUO);
                        subTasksCache.get(pathLastComponent).remove(taskUO); // not in cache anymore
                        taskNameCache.remove(taskUO); // not in cache anymore
                        break;
                    }
                    case OPERATION_MODIFY: {
                        String cacheName = taskNameCache.get(taskUO);
                        if (cacheName.equals(task.getName())) {
                            // not name modification (time...)
                            modelSupport.fireChildChanged(path, childIndex, taskUO);
                        } else {
                            subTasksCache.remove(pathLastComponent); // force child recache
                            int newIndex = getIndexOfChild(pathLastComponent, taskUO);
                            if (newIndex != childIndex) {
                                // name modification (order DO change)
                                modelSupport.fireChildRemoved(path, childIndex, taskUO);
                                modelSupport.fireChildAdded(path, newIndex, taskUO);
                            } else {
                                // name modification (order DO NOT change)
                                modelSupport.fireChildChanged(path, childIndex, taskUO);
                            }
                        }
                        break;
                    }
                    default:
                        if (log.isErrorEnabled()) {
                            log.error("Unknow operation : " + operation);
                        }
                }

                updated = true;
            } else {
                TreePath childTreePath = path.pathByAddingChild(taskUO);
                updated = updateChildren(task, childTreePath, operation);

                if (updated) {
                    // ...and by recursion update all path
                    modelSupport.firePathChanged(path);
                }
            }
        }

        return updated;
    }

    @Override
    public void addProject(TimerProject project) {
        notifyTaskChanged(project, OPERATION_ADD);
    }

    @Override
    public void startTask(TimerTask task) {
        // to improve UI response when task is started
        notifyTaskChanged(task, OPERATION_MODIFY);
    }

    @Override
    public void addTask(TimerTask task) {
        notifyTaskChanged(task, OPERATION_ADD);
    }

    @Override
    public void dataLoaded(Collection<TimerProject> projects) {

        // save size
        final Collection<TableColumn> tcs = new ArrayList<>(projectsAndTaskTable.getColumns());

        taskNameCache.clear();
        subTasksCache.clear();

        // works but it's not beautiful
        // and header are resized
        modelSupport.fireNewRoot();

        // hack, restore size after
        SwingUtilities.invokeLater(() -> {
            for (TableColumn tc : tcs) {
                projectsAndTaskTable.getColumnExt(tc.getIdentifier())
                        .setPreferredWidth(tc.getPreferredWidth());
            }
        });
    }

    @Override
    public void deleteProject(TimerProject project) {
        notifyTaskChanged(project, OPERATION_DELETE);
    }

    @Override
    public void deleteTask(TimerTask task) {
        notifyTaskChanged(task, OPERATION_DELETE);
    }

    @Override
    public void modifyProject(TimerProject project) {
        notifyTaskChanged(project, OPERATION_MODIFY);

    }

    @Override
    public void modifyTask(TimerTask task) {
        notifyTaskChanged(task, OPERATION_MODIFY);

    }

    @Override
    public void setTaskTime(TimerTask task, Date date, Long time) {
        notifyTaskChanged(task, OPERATION_MODIFY);
    }

    @Override
    public void changeClosedState(TimerTask task) {

        if (showClosedTask) {
            if (log.isDebugEnabled()) {
                log.debug("Fire modify on state change because hidden task are shown");
            }
            notifyTaskChanged(task, OPERATION_MODIFY);
        } else {
            if (task.isClosed()) {
                if (log.isDebugEnabled()) {
                    log.debug("Fire delete on state change because task disapear from view");
                }
                notifyTaskChanged(task, OPERATION_DELETE);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Fire add on state change because task reappear in view");
                }
                notifyTaskChanged(task, OPERATION_ADD);
            }
        }
    }

    /**
     * Change closed task property.
     *
     * @param showClosedTask closed task property
     */
    public void setShowClosed(boolean showClosedTask) {
        this.showClosedTask = showClosedTask;
        // TODO echatellier 20120309 add better diff code
        dataLoaded(null);
    }

    @Override
    public void moveTask(TimerTask task) {
        notifyTaskChanged(task, OPERATION_ADD);
    }

    @Override
    public void preMoveTask(TimerTask task) {
        notifyTaskChanged(task, OPERATION_DELETE);
    }

    @Override
    public void postMergeTasks(TimerTask destinationTask, List<TimerTask> otherTasks) {

        notifyTaskChanged(destinationTask, OPERATION_MODIFY);
        for (TimerTask otherTask : otherTasks) {
            notifyTaskChanged(otherTask, OPERATION_DELETE);
        }
    }
}
