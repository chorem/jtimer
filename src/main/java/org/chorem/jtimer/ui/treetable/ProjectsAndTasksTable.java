/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2018 CodeLutin, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.treetable;

import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.chorem.jtimer.ui.treetable.dnd.TimerTaskTransferHandler;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceManager;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.tree.TreePath;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * ProjectAndTaskTable.
 *
 * Just redefine JXTreeTable setting up a specific model
 *
 * @see org.jdesktop.swingx.JXTreeTable
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ProjectsAndTasksTable extends JXTreeTable {

    /** serialVersionUID. */
    private static final long serialVersionUID = -6909972377431117193L;

    /** Parent application. */
    protected JTimer parent;

    /** Table model. */
    protected ProjectsAndTasksModel treeTableModel;

    /** Table renderer. */
    protected ProjectsAndTasksRunningCellRenderer treeCellRenderer;

    /**
     * Constructor.
     *
     * @param parent parent
     * @param core timer core
     */
    public ProjectsAndTasksTable(JTimer parent, TimerCore core) {
        this.parent = parent;

        // start with init i18n of table column name
        ApplicationContext ctxt = parent.getContext();
        ResourceManager mgr = ctxt.getResourceManager();
        ResourceMap resourceMap = mgr.getResourceMap(ProjectsAndTasksTable.class);
        // init list
        List<String> columnIdentifiers = new ArrayList<>();
        columnIdentifiers.add(resourceMap.getString("projectsAndTaskColumnName"));
        columnIdentifiers.add(resourceMap.getString("todayTimeColumnName"));
        columnIdentifiers.add(resourceMap.getString("totalTimeColumnName"));

        // set model
        treeTableModel = new ProjectsAndTasksModel(this, core, columnIdentifiers);

        // set renderer
        treeCellRenderer = new ProjectsAndTasksRunningCellRenderer(this, core);

        setTreeCellRenderer(treeCellRenderer);
        setTreeTableModel(treeTableModel);

        // hack (work for white based theme)
        // swingx bug : https://swingx.dev.java.net/issues/show_bug.cgi?id=964
        setBackground(Color.WHITE);

        //treeTableModel.setSortColumn("Projects & Tasks");
        core.getData().addDataEventListener(treeTableModel);

        // enable drag n drop
        setDragEnabled(true);
        setTransferHandler(new TimerTaskTransferHandler(parent, core.getData()));

        registerKeys();
    }

    /**
     * Add some key action to tree.
     */
    protected void registerKeys() {
        // add action to collapse (left arrow) selected node
        Action actionLeft = new AbstractAction("collapseSelectedNode") {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = getSelectedRow();
                if (selectedRow != -1) {
                    if (isExpanded(selectedRow)) {
                        collapseRow(selectedRow);
                    } else {
                        // select parent node
                        TimerTask timerTask = getSelectedTasks().get(0);
                        setSelectedTask(timerTask.getParent());
                    }
                }
            }
        };

        getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
                actionLeft.getValue(Action.NAME));
        getActionMap().put(actionLeft.getValue(Action.NAME), actionLeft);


        // add action to expand (right arrow) selected node
        Action actionRight = new AbstractAction("expandSelectedNode") {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = getSelectedRow();
                if (selectedRow != -1) {
                    expandRow(selectedRow);
                }
            }
        };

        getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
                actionRight.getValue(Action.NAME));
        getActionMap().put(actionRight.getValue(Action.NAME), actionRight);


        // add action to collapse (left arrow) selected node
        Action actionEnter = new AbstractAction("startSelectedTask") {
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                parent.startOrStopCurrentSelectedTask();
            }
        };

        getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
                actionEnter.getValue(Action.NAME));
        getActionMap().put(actionEnter.getValue(Action.NAME), actionEnter);
    }

    /**
     * Change show closed task property.
     *
     * @param closed new closed state
     *
     * @see ProjectsAndTasksModel#setShowClosed(boolean)
     */
    public void setShowClosed(boolean closed) {
        treeTableModel.setShowClosed(closed);
    }

    /**
     * Get tasks corresponding to selected rows
     *
     * @return tasks list (never <tt>null</tt>)
     */
    public List<TimerTask> getSelectedTasks() {

        List<TimerTask> results = new ArrayList<>();

        // oula, c long de retouver la tache selectionnee
        TreePath[] paths = getTreeSelectionModel().getSelectionPaths();

        if (paths != null) {
            for (TreePath path : paths) {

                // s'il y a une selection...
                if (path != null) {
                    Object[] pathWay = path.getPath();

                    if (pathWay.length >= 3) { // 1 = root, 2=project , 3+=task
                        // le last en la derniere feuille selmectionnee, donc la
                        // tache
                        TimerTask task = (TimerTask) pathWay[pathWay.length - 1];

                        results.add(task);
                    }
                }
            }
        }
        return results;
    }

    /**
     * Get tasks or projects corresponding to selected rows
     *
     * @return project or task list (never <tt>null</tt>)
     */
    public List<TimerTask> getSelectedElements() {

        List<TimerTask> results = new ArrayList<>();

        // oula, c long de retouver la tache selectionnee
        TreePath[] paths = getTreeSelectionModel().getSelectionPaths();

        if (paths != null) {
            for (TreePath path : paths) {

                // s'il y a une selection...
                if (path != null) {
                    Object[] pathWay = path.getPath();

                    if (pathWay.length >= 2) { // 1 = root, 2=project , 3+=task
                        // le last est la derniere feuille selectionnee, donc la
                        // tache
                        TimerTask task = (TimerTask) pathWay[pathWay.length - 1];

                        results.add(task);
                    }
                }
            }
        }
        return results;
    }

    /**
     * Get projects corresponding to selected rows.
     *
     * @return projects list (never <tt>null</tt>)
     */
    public List<TimerProject> getSelectedProjects() {

        List<TimerProject> results = new ArrayList<>();

        // oula, c long de retouver la tache selectionnee
        TreePath[] paths = getTreeSelectionModel().getSelectionPaths();

        if (paths != null) {
            for (TreePath path : paths) {

                // s'il y a une selection...
                if (path != null) {
                    Object[] pathWay = path.getPath();

                    if (pathWay.length == 2) { // 1 = root, 2=project , 3+=task
                        // le last est la derniere feuille selectionnee, donc la
                        // tache
                        TimerProject project = (TimerProject) pathWay[pathWay.length - 1];

                        results.add(project);
                    }
                }
            }
        }

        return results;
    }

    /**
     * Set selected task in tree.
     *
     * @param task task
     */
    public void setSelectedTask(TimerTask task) {
        // compute task tree path
        List<TimerTask> components = TimerTaskHelper.getPathFromParent(task);
        components.add(0, (TimerTask) treeTableModel.getRoot());

        // select tree path in reverse order (from root to leaf)
        TreePath path = new TreePath(components.toArray());
        int row = getRowForPath(path);
        getSelectionModel().setSelectionInterval(row, row);
    }
}
