/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.treetable.dnd;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.DataViolationException;
import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.chorem.jtimer.ui.treetable.ProjectsAndTasksTable;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceManager;
import org.jdesktop.application.ResourceMap;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.TransferHandler;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Transfer handler used to transfer tasks in table.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public class TimerTaskTransferHandler extends TransferHandler {

    /** serialVersionUID. */
    private static final long serialVersionUID = 5433321973795969278L;

    /** log. */
    private static Log log = LogFactory.getLog(TimerTaskTransferHandler.class);

    /** I18n resources map */
    protected ResourceMap resourceMap;

    /** Data manager. */
    protected TimerDataManager dataManager;

    /**
     * Constructor.
     *
     * @param application
     * @param dataManager data manager
     */
    public TimerTaskTransferHandler(Application application,
                                    TimerDataManager dataManager) {
        this.dataManager = dataManager;

        // init resources map
        ApplicationContext ctxt = application.getContext();
        ResourceManager mgr = ctxt.getResourceManager();
        resourceMap = mgr.getResourceMap(TimerTaskTransferHandler.class);
    }

    @Override
    public boolean canImport(JComponent cp, DataFlavor[] df) {

        for (DataFlavor dataFlavor : df) {
            if (dataFlavor.equals(TimerTaskTranferable.myData)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean importData(JComponent component, Transferable transferable) {

        boolean confirmImport = false;

        if (transferable.isDataFlavorSupported(TimerTaskTranferable.myData)) {
            try {

                ProjectsAndTasksTable treeTable = (ProjectsAndTasksTable) component;
                // elements here, task can be move to task or projects
                List<TimerTask> selectedTasks = treeTable.getSelectedElements();

                if (selectedTasks != null && !selectedTasks.isEmpty()) {
                    TimerTask destinationTask = selectedTasks.get(0);
                    Object myObject = transferable
                            .getTransferData(TimerTaskTranferable.myData);
                    List<TimerTask> movedTasks = (List<TimerTask>) myObject;

                    // can't move task to itself
                    boolean validMove = !TimerTaskHelper
                            .collectionContainsTask(movedTasks, destinationTask);
                    if (validMove) {
                        String title = resourceMap.getString("move.task.title");
                        String message;
                        if (movedTasks.size() == 1) {
                            message = resourceMap.getString("move.task.message",
                                    movedTasks.get(0).getName(), destinationTask.getName());
                        } else {
                            message = resourceMap.getString("move.tasks.message",
                                    movedTasks.size(), destinationTask.getName());
                        }
                        int answer = JOptionPane.showConfirmDialog(component,
                                message, title, JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE);

                        if (answer == JOptionPane.YES_OPTION) {
                            confirmImport = true;
                        }
                    }
                }
            } catch (IOException | UnsupportedFlavorException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transferring task", e);
                }
            }
        }
        return confirmImport;

    }

    /**
     * Get selected task in tree, and build Transferable object for it.
     *
     * @param cp component
     * @return transferable instance for selected task
     */
    @Override
    protected Transferable createTransferable(JComponent cp) {

        Transferable transferable = null;

        ProjectsAndTasksTable treeTable = (ProjectsAndTasksTable) cp;
        // only task can be moved !
        List<TimerTask> selectedTasks = treeTable.getSelectedTasks();
        if (selectedTasks != null && !selectedTasks.isEmpty()) {
            List<TimerTask> tasks = new LinkedList<>(selectedTasks);
            transferable = new TimerTaskTranferable(tasks);
        }

        return transferable;
    }

    @Override
    protected void exportDone(JComponent cp, Transferable transferable, int type) {
        if (log.isDebugEnabled()) {
            log.debug("Transfert done");
        }

        if (type == TransferHandler.MOVE) {

            try {
                ProjectsAndTasksTable treeTable = (ProjectsAndTasksTable) cp;
                // elements here, task can be move to task or projects
                List<TimerTask> selectedTasks = treeTable.getSelectedElements();

                if (selectedTasks != null && !selectedTasks.isEmpty()) {
                    TimerTask destinationTask = selectedTasks.get(0);
                    Object myObject = transferable
                            .getTransferData(TimerTaskTranferable.myData);
                    List<TimerTask> movedTasks = (List<TimerTask>) myObject;

                    try {
                        dataManager.moveTask(destinationTask, movedTasks);
                    } catch (DataViolationException e) {
                        String title = resourceMap
                                .getString("action.invalidActionTitle");
                        String message = resourceMap.getString(e
                                .getExceptionKey());
                        if (StringUtils.isEmpty(message)) {
                            message = resourceMap.getString(
                                    "action.missingErrorMessage", e.getExceptionKey());
                        }
                        JOptionPane.showMessageDialog(cp, message, title,
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            } catch (IOException | UnsupportedFlavorException e) {
                if (log.isErrorEnabled()) {
                    log.error("Exception while transferring task", e);
                }
            }
        }
    }

    @Override
    public int getSourceActions(JComponent component) {
        return MOVE;
    }
}
