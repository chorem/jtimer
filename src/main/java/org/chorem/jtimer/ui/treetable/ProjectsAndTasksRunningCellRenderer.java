/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.treetable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.DataEventListener;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerTask;
import org.jdesktop.swingx.JXTreeTable;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import java.awt.Component;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Gere l'affichage des noeuds de l'arbre.
 *
 * Comme une icone quand la taches est lancee...
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ProjectsAndTasksRunningCellRenderer extends ProjectsAndTasksCellRenderer implements DataEventListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = 1383276150996517529L;

    /** Logger. */
    private static Log log = LogFactory.getLog(ProjectsAndTasksRunningCellRenderer.class);

    /** Running task icon. */
    protected ImageIcon runningIcon;

    /** Running tasks. */
    protected Collection<TimerTask> runningTasks;

    /** Tree table reference for image observer. */
    protected JXTreeTable treeTable;

    /** Node image nodeObserver for running task animated icon. */
    protected NodeImageObserver nodeObserver;

    /**
     * Constructor.
     *
     * @param treeTable Tree table reference for image observer
     * @param core TimerCore
     */
    public ProjectsAndTasksRunningCellRenderer(JXTreeTable treeTable, TimerCore core) {
        this.treeTable = treeTable;

        // init
        runningTasks = new HashSet<>();

        URL runnigIconUrl = ProjectsAndTasksRunningCellRenderer.class
                .getResource("/org/chorem/jtimer/resources/running.gif");
        runningIcon = new ImageIcon(runnigIconUrl);
        nodeObserver = new NodeImageObserver();
        runningIcon.setImageObserver(nodeObserver);

        // be notified on events
        core.getData().addDataEventListener(this);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {

        // le fait en 2 temps car sinon, on voit de temps en temps
        // le toString() suivit du setName()
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        // if this is a task
        if (value instanceof TimerTask) {
            TimerTask task = (TimerTask) value;

            // add icon if task is running  
            if (runningTasks.contains(task)) {
                setIcon(runningIcon);
                nodeObserver.addRow(row);
            } else {
                // force no default icon
                setIcon(null);
                nodeObserver.removeRow(row);
            }
        }

        return this;
    }

    /**
     * Enclosed class to manage gif image refresh.
     */
    class NodeImageObserver implements ImageObserver {

        protected Set<Integer> rows = Collections.synchronizedSet(new HashSet<>());

        public void addRow(Integer row) {
            rows.add(row);
        }

        public void removeRow(Integer row) {
            rows.remove(row);
        }

        @Override
        public boolean imageUpdate(Image img, int flags, int x, int y, int w, int h) {
            if ((flags & (FRAMEBITS | ALLBITS)) != 0) {
                for (Integer row : rows) {
                    Rectangle rowBounds = treeTable.getCellRect(row, 0, true);
                    treeTable.repaint(rowBounds);
                }
            }
            return (flags & (ALLBITS | ABORT)) == 0;
        }
    }

    @Override
    public void startTask(TimerTask task) {

        if (log.isDebugEnabled()) {
            log.debug("startTask on " + task.getName());
        }

        // remember running task
        runningTasks.add(task);
    }

    @Override
    public void stopTask(TimerTask task) {

        if (log.isDebugEnabled()) {
            log.debug("stopTask on " + task.getName());
        }

        // remember don't running task
        runningTasks.remove(task);
    }
}
