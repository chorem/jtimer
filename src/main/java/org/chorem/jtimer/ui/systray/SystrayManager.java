/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.systray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.DataEventListener;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.ui.widget.JPopupTrayIcon;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceManager;
import org.jdesktop.application.ResourceMap;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.AWTException;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Factory to get correct working systray implementation.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class SystrayManager extends WindowAdapter implements ActionListener, DataEventListener,
        MouseListener, Runnable {

    /** Log. */
    private static Log log = LogFactory.getLog(SystrayManager.class);

    /** Parent reference. */
    protected JTimer parent;

    /** I18n resources map. */
    protected ResourceMap resourceMap;

    /** Idle image. */
    protected Image idleImage;

    /** Running image. */
    protected Image runningImage;

    /** Idle detect image. */
    protected Image idleDetectImage;

    /** Tray icon. (null values when tray is non available) */
    protected JPopupTrayIcon trayIcon;

    /** Reference how many tasks are running. */
    protected int nbTasksRunning = 0;

    /** Non running popup menu instance. */
    protected JPopupMenu popup;

    /** Menu show. (used to change text) */
    protected JMenuItem showItem;

    /** Menu stop. (used to enable/disable) */
    protected JMenuItem stopItem;

    /**
     * Default constructor.
     *
     * @param parent parent
     */
    public SystrayManager(JTimer parent) {
        this.parent = parent;

        // init resources map
        ApplicationContext ctxt = parent.getContext();
        ResourceManager mgr = ctxt.getResourceManager();
        resourceMap = mgr.getResourceMap(SystrayManager.class);

        // load an image
        // use FQN to not conflict with annotation
        idleImage = resourceMap.getImageIcon("idleImage").getImage();
        runningImage = resourceMap.getImageIcon("runningImage").getImage();
        idleDetectImage = resourceMap.getImageIcon("idleDetectImage").getImage();

        // make popup menu instance
        buildPopupMenu();
    }

    /**
     * Build popup menu.
     */
    private void buildPopupMenu() {

        // create a action listener to listen for default action executed on
        // the tray icon
        // create a popup menu
        popup = new JPopupMenu();

        // show
        showItem = new JMenuItem(resourceMap.getString("hideMenuText"));
        showItem.addActionListener(this);
        showItem.setActionCommand("showHide");
        popup.add(showItem);
        popup.addSeparator();

        // stop task
        stopItem = new JMenuItem(resourceMap.getString("stopMenuText"));
        stopItem.addActionListener(this);
        stopItem.setActionCommand("stop");
        stopItem.setEnabled(false);
        popup.add(stopItem);
        popup.addSeparator();

        // quit
        JMenuItem quitItem = new JMenuItem(resourceMap.getString("quitMenuText"));
        quitItem.addActionListener(this);
        quitItem.setActionCommand("quit");
        popup.add(quitItem);
    }

    /**
     * Install try icon into systray.
     */
    public void install() {

        // desactive la croix
        for (WindowListener l : parent.getMainFrame().getWindowListeners()) {
            parent.getMainFrame().removeWindowListener(l);
        }
        // et la reactive de facon personnalisee
        parent.getMainFrame().addWindowListener(this);

        EventQueue.invokeLater(this);
    }

    /**
     * Tray icon installation is make into a thread, put un AWT event
     * queue.
     *
     * @see EventQueue#invokeLater(Runnable)
     */
    @Override
    public void run() {
        if (SystemTray.isSupported()) {
            // get the SystemTray instance
            SystemTray tray = SystemTray.getSystemTray();

            // construct a TrayIcon
            trayIcon = new JPopupTrayIcon(idleImage, resourceMap.getString("tooltipIdleText"), popup);
            trayIcon.setImageAutoSize(true);
            trayIcon.addMouseListener(this);

            // add the tray image
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                // exception is throw if systray is currently missing
                if (log.isWarnEnabled()) {
                    log.warn("Error while setting system tray", e);
                }
            }
        }
    }

    @Override
    public void startTask(TimerTask task) {
        startStopTask(task, true);
    }

    @Override
    public void stopTask(TimerTask task) {
        startStopTask(task, false);
    }

    /**
     * Common code for start or stop task.
     *
     * @param task task
     * @param start start(true) or stop(false) task
     */
    protected void startStopTask(TimerTask task, boolean start) {

        // display message
        if (trayIcon != null) {

            // increment or decrement task
            if (start) {
                ++nbTasksRunning;
            } else {
                --nbTasksRunning;
            }

            String message;
            if (nbTasksRunning == 0) {
                message = resourceMap.getString("tooltipIdleText");

                trayIcon.setImage(idleImage);
            } else {
                trayIcon.setImage(runningImage);
                if (nbTasksRunning == 1) {
                    if (JTimer.config.isShowTaskNameInSystray()) {
                        message = resourceMap.getString("tooltipRunningTaskNameText", task.getName());
                    } else {
                        message = resourceMap.getString("tooltipRunningTaskText", nbTasksRunning);
                    }
                } else {
                    message = resourceMap.getString("tooltipRunningTasksText", nbTasksRunning);
                }
            }

            trayIcon.setToolTip(message);

            // enable some menus
            stopItem.setEnabled(nbTasksRunning > 0);
        }
    }

    /**
     * Called by main application UI on idle detect.
     */
    public void preIdleDetect() {

        if (log.isDebugEnabled()) {
            log.debug("Pre idle detect");
        }

        // tray available only if trayIcon != null
        if (trayIcon != null) {
            trayIcon.setImage(idleDetectImage);
        }
    }

    /**
     * Called by main application UI after idle detect.
     */
    public void postIdleDetect() {

        if (log.isDebugEnabled()) {
            log.debug("Post idle detect");
        }

        // tray available only if trayIcon != null
        if (trayIcon != null) {
            if (nbTasksRunning == 0) {
                trayIcon.setImage(idleImage);
            } else {
                trayIcon.setImage(runningImage);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // This is only popop menu action here

        String actionCommand = e.getActionCommand();

        if ("showHide".equals(actionCommand)) {
            if (parent.getMainFrame().isVisible()) {
                // on la cache
                parent.hide();
                showItem.setText(resourceMap.getString("showMenuText"));
            } else {
                // sinon on la montre
                parent.show();
                showItem.setText(resourceMap.getString("hideMenuText"));
            }
        } else if ("stop".equals(actionCommand)) {
            parent.stopAllTasks();
        } else if ("quit".equals(actionCommand)) {
            parent.quit(e);
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {

        // ne fait pas de double affichage si plus de clics
        // et ne change pas l'affichage si clic droit
        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON1) {

            // si la fenetre est affichee
            if (parent.getMainFrame().isVisible()) {
                showItem.setText(resourceMap.getString("showMenuText"));

                // on la cache
                parent.hide();

            } else {

                showItem.setText(resourceMap.getString("hideMenuText"));

                // sinon on la montre
                // correct iconified bug
                // http://www.java-forums.org/awt-swing/7000-problem-setvisible-linux.html
                //parent.getMainFrame().setExtendedState(JFrame.NORMAL);
                parent.show();

            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

        // tray available only if trayIcon != null
        if (trayIcon != null && JTimer.config.isCloseToSystray()) {
            // hide window (without exiting)
            parent.hide();
            showItem.setText(resourceMap.getString("showMenuText"));
        } else {
            // just exit here
            parent.exit(e);
        }
    }
}
