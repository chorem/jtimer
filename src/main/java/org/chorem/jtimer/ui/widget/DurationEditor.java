/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.widget;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.FlowLayout;

/**
 * Duration editor.
 *
 * Graphical editor to set hh/mm/ss duration.
 *
 * @author chatellier
 * @version $Revision: 1.0 $
 *
 * Last update : $Date: 2 juil. 2009 $
 * By : $Author: chatellier $
 */
public class DurationEditor extends JPanel implements ChangeListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = -5395307807542911835L;

    /** Hour spinner. */
    protected JSpinner hourSpinner;

    /** Minute spinner. */
    protected JSpinner minuteSpinner;

    /** Second spinner. */
    protected JSpinner secondSpinner;

    /** Hour spinner model. */
    protected SpinnerNumberModel hourSpinnerModel;

    /** Minute spinner model. */
    protected SpinnerNumberModel minuteSpinnerModel;

    /** Second spinner model. */
    protected SpinnerNumberModel secondSpinnerModel;

    /** Duration (in ms). */
    protected long duration;

    /**
     * Constructor.
     *
     * Default hours set to 24.
     */
    public DurationEditor() {
        this(24);
    }

    /**
     * Constructor.
     *
     * @param maxHourValue max hour value
     */
    public DurationEditor(int maxHourValue) {
        super(new FlowLayout(FlowLayout.CENTER, 0, 0));

        hourSpinnerModel = new SpinnerNumberModel(0, 0, maxHourValue, 1);
        hourSpinner = new JSpinner(hourSpinnerModel);
        add(hourSpinner);

        add(new JLabel(":"));

        minuteSpinnerModel = new SpinnerNumberModel(0, -1, 60, 1);
        minuteSpinner = new JSpinner(minuteSpinnerModel);
        add(minuteSpinner);

        add(new JLabel(":"));

        secondSpinnerModel = new SpinnerNumberModel(0, -1, 60, 1);
        secondSpinner = new JSpinner(secondSpinnerModel);
        add(secondSpinner);

        hourSpinnerModel.addChangeListener(this);
        minuteSpinnerModel.addChangeListener(this);
        secondSpinnerModel.addChangeListener(this);
    }

    /*
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     */
    @Override
    public void stateChanged(ChangeEvent e) {

        // click on minute spinner
        if (e.getSource() == minuteSpinnerModel) {
            int value = minuteSpinnerModel.getNumber().intValue();
            // up
            if (value >= 60) {
                if (hourSpinnerModel.getNextValue() != null) {
                    minuteSpinnerModel.setValue(value - 60);
                    hourSpinnerModel.setValue(hourSpinnerModel.getNextValue());
                } else {
                    minuteSpinnerModel.setValue(value - 1);
                }
                // down
            } else if (value < 0) {
                if (hourSpinnerModel.getPreviousValue() != null) {
                    minuteSpinnerModel.setValue(value + 60);
                    hourSpinnerModel.setValue(hourSpinnerModel
                            .getPreviousValue());
                } else {
                    minuteSpinnerModel.setValue(value + 1);
                }
            }
        }

        // click on seconds spinner
        else if (e.getSource() == secondSpinnerModel) {
            int value = secondSpinnerModel.getNumber().intValue();
            // up
            if (value >= 60) {
                if (minuteSpinnerModel.getNextValue() != null) {
                    secondSpinnerModel.setValue(value - 60);
                    minuteSpinnerModel.setValue(minuteSpinnerModel
                            .getNextValue());
                } else {
                    secondSpinnerModel.setValue(value - 1);
                }
                // down
            } else if (value < 0) {
                if (hourSpinnerModel.getNumber().intValue() > 0
                        || minuteSpinnerModel.getNumber().intValue() > 0) {
                    secondSpinnerModel.setValue(value + 60);
                    minuteSpinnerModel.setValue(minuteSpinnerModel
                            .getPreviousValue());
                } else {
                    secondSpinnerModel.setValue(value + 1);
                }
            }
        }

        long oldDuration = duration;
        duration = getDuration();
        firePropertyChange("duration", oldDuration, duration);
    }

    /**
     * Get duration in seconds
     *
     * @return duration in seconds
     */
    public long getDuration() {
        long modelDuration = 0;
        // add hour
        modelDuration += hourSpinnerModel.getNumber().longValue() * 3600;
        // add minutes
        modelDuration += minuteSpinnerModel.getNumber().longValue() * 60;
        // add seconds
        modelDuration += secondSpinnerModel.getNumber().longValue();

        return modelDuration;
    }

    /**
     * Set duration.
     *
     * @param duration
     */
    public void setDuration(long duration) {
        this.duration = duration;
        long localDuration = duration;

        hourSpinnerModel.setValue((int) (localDuration / 3600));

        // reste de la division par 3600
        localDuration %= 3600;

        minuteSpinnerModel.setValue((int) (localDuration / 60l));

        // reste de la division par 60
        localDuration %= 60;

        secondSpinnerModel.setValue((int) localDuration);
    }
}
