/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.widget;

import javax.swing.JDialog;
import javax.swing.JPopupMenu;
import javax.swing.JWindow;
import javax.swing.RootPaneContainer;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.TrayIcon;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * JPopupMenu compatible TrayIcon based on Alexander Potochkin's JXTrayIcon
 * (http://weblogs.java.net/blog/alexfromsun/archive/2008/02/jtrayicon_updat.html)
 * but uses a JWindow instead of a JDialog to workaround some bugs on linux.
 *
 * @author Michael Bien
 * @author Chatellier Eric
 */
public class JPopupTrayIcon extends TrayIcon implements MouseListener,
        PopupMenuListener {

    private JPopupMenu menu;

    private Window window;

    private static final boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().contains("windows");

    public JPopupTrayIcon(Image image, String tooltip, JPopupMenu popup) {
        super(image, tooltip);
        addMouseListener(this);
        setJPopupMenu(popup);
    }

    protected void showJPopupMenu(MouseEvent e) {
        if (e.isPopupTrigger() && menu != null) {
            if (window == null) {

                if (IS_WINDOWS) {
                    window = new JDialog((Frame) null);
                    ((JDialog) window).setUndecorated(true);
                } else {
                    window = new JWindow((Frame) null);
                }
                window.setAlwaysOnTop(true);
                Dimension size = menu.getPreferredSize();

                Point centerPoint = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
                if (e.getY() > centerPoint.getY()) {
                    window.setLocation(e.getX(), e.getY() - size.height);
                } else {
                    window.setLocation(e.getX(), e.getY());
                }

                window.setVisible(true);

                menu.show(((RootPaneContainer) window).getContentPane(), 0, 0);

                // popup works only for focused windows
                window.toFront();
            }
        }
    }

    public void setJPopupMenu(JPopupMenu menu) {
        if (this.menu != null) {
            this.menu.removePopupMenuListener(this);
        }
        this.menu = menu;
        menu.addPopupMenuListener(this);
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        if (window != null) {
            window.dispose();
            window = null;
        }
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
        if (window != null) {
            window.dispose();
            window = null;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        showJPopupMenu(e);

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        showJPopupMenu(e);
    }
}
