/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2012 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.widget;

import org.jdesktop.application.session.WindowProperty;

import java.awt.Component;
import java.awt.Frame;

/**
 * Code from http://kenai.com/jira/browse/BSAF-74.
 *
 * Force jtimer to always start in not iconified mode.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class WindowProperty2 extends WindowProperty {

    @Override
    public void setSessionState(final Component comp, final Object state) {
        super.setSessionState(comp, state);
        if (comp instanceof Frame && !((Frame) comp).isLocationByPlatform()) {
            ((Frame) comp).setExtendedState(((Frame) comp).getExtendedState() & ~Frame.ICONIFIED);
        }
    }
}
