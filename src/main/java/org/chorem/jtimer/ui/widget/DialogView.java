/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2016 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.jtimer.ui.widget;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.View;

import javax.swing.JDialog;
import javax.swing.JRootPane;
import java.awt.Image;
import java.awt.Window;

import static org.jdesktop.application.Application.KEY_APPLICATION_ICON;
import static org.jdesktop.application.Application.KEY_APPLICATION_TITLE;

/**
 * Another implementation of FrameView, but using a dialog to use modal state and non visible
 * window in window task bar.
 */
public class DialogView extends View {
    public static final String MAIN_DIALOG_NAME = "mainDialog";

    protected Window owner;
    protected JDialog dialog;

    public DialogView(Window owner, Application application) {
        super(application);
        this.owner = owner;
    }

    public JDialog getDialog() {
        if (dialog == null) {
            ResourceMap resourceMap = getContext().getResourceMap();
            String title = resourceMap.getString(KEY_APPLICATION_TITLE);

            dialog = new JDialog(owner, title);
            dialog.setName(MAIN_DIALOG_NAME);
            dialog.setResizable(false);
            dialog.setModal(true);
            if (resourceMap.containsKey(KEY_APPLICATION_ICON)) {
                Image icon = resourceMap.getImageIcon(KEY_APPLICATION_ICON).getImage();
                dialog.setIconImage(icon);
            }
        }
        return dialog;
    }

    @Override
    public JRootPane getRootPane() {
        return getDialog().getRootPane();
    }
}
