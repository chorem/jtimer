/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin, Chorlet Stéphane, Chemit Tony
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.DataViolationException;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.chorem.jtimer.ui.widget.DialogView;
import org.jdesktop.application.Action;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.calendar.DateSelectionModel;

import javax.swing.ActionMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

/**
 * JTimer task editor
 *
 * @author sch
 */
public class TimerTaskEditor extends DialogView {

    /** log */
    private static Log log = LogFactory.getLog(TimerTaskEditor.class);

    protected JTimer parent;
    protected ActionMap actionMap;

    /** task title */
    protected JTextField titleText;
    /** month calendar */
    protected JXMonthView monthView;
    /** hour spinner */
    protected JSpinner spinnerH;
    /** minute spinner */
    protected JSpinner spinnerM;
    /** second spinner */
    protected JSpinner spinnerS;
    /** annotation textarea */
    protected JTextArea annotationText;

    /** change listener on spinners */
    protected ChangeListener spinnerListener;

    /** change listener on annotation textarea */
    protected DocumentListener titleChangeListener;
    /** change listener on annotation textarea */
    protected DocumentListener annotationChangeListener;

    /** task to edit */
    protected TimerTask task;
    /** task to edit */
    protected TimerTask cloneTask;
    /** timer core */
    protected TimerCore core;
    /** task manager */
    protected TimerDataManager dataManager;

    protected boolean dataChanged;
    /** did the modified the title ? */
    protected boolean isTitleChanged;
    /** did the modified annotation ? */
    protected boolean isAnnotationChanged;

    /**
     * days modified: You can modify any number of days before you apply changes, 
     * this set keep track of days modified (for time, annotations or title).
     */
    protected Set<Date> dateChanged = new HashSet<>();

    /**
     * days where annotation were modified: You can modify any number of days before you apply changes, 
     * this set keep track of days where annotations were modified.
     */
    protected Set<Date> annotationChanged = new HashSet<>();

    /**
     * selected monthview day: should never be null
     */
    protected Calendar selectedDay;

    /**
     * TimerTaskEditor
     *
     * @param parent
     * @param task
     * @param core
     */
    public TimerTaskEditor(JTimer parent, TimerTask task, TimerCore core) {
        super(parent.getMainFrame(), parent);
        this.parent = parent;

        // remove dialog name to this dialog, due to bug
        // http://chorem.org/issues/484 and
        // http://kenai.com/jira/browse/BSAF-107
        // cause infinite loop under openjdk
        getDialog().setName("editTask"); // force nameExplicitlySet to true
        getDialog().setTitle(getResourceMap().getString("editor.title"));

        // set color on jxmonthview unselectable days 
        //        UIManager.put("JXMonthView.unselectableDayForeground", Color.GRAY);

        this.task = task;
        cloneTask = task.clone();
        this.core = core;
        dataManager = core.getData();

        // init resources map
        //ApplicationContext ctxt = parent.getContext();
        //ResourceManager mgr = ctxt.getResourceManager();
        //resourceMap = mgr.getResourceMap(TimerTaskEditor.class);
        actionMap = parent.getContext().getActionMap(this);

        // build UI
        buildUI();

        // update monthview flagged days according to tasktime
        updateFlaggedDates();

        // display last selectable day tasktime and annotation
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        setSelectedDay(yesterday.getTime());
    }

    /**
     * Build ui.
     */
    protected void buildUI() {
        JPanel panel = new JPanel(new BorderLayout(1, 0));
        panel.add(createContentPanel(), BorderLayout.CENTER);
        panel.add(createCommandPanel(), BorderLayout.SOUTH);
        setComponent(panel);
    }

    /**
     * Create command panel.
     *
     * @return command panel
     */
    protected JPanel createCommandPanel() {
        // apply button
        JButton applyButton = new JButton();
        applyButton.setAction(actionMap.get("apply"));

        // cancel button
        JButton cancelButton = new JButton();
        cancelButton.setAction(actionMap.get("cancel"));
        // default escape
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "ESCAPE");
        getRootPane().getActionMap().put("ESCAPE", cancelButton.getAction());

        // command panel
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 0));
        panel.add(cancelButton);
        panel.add(applyButton);
        return panel;
    }

    /**
     * Create main content.
     *
     * @return content panel
     */
    protected JPanel createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        // text change listener on annotationTextArea and titleTextField
        titleChangeListener = new TextChangeListener();
        annotationChangeListener = new TextChangeListener();

        // task title
        titleText = new JTextField("", 20); // prevent too long task names
        titleText.getDocument().addDocumentListener(titleChangeListener);

        panel.add(titleText, BorderLayout.NORTH);
        panel.add(createJXMonthView(), BorderLayout.CENTER);
        panel.add(createEditionPanel(), BorderLayout.SOUTH);

        return panel;
    }

    /**
     * Create edition panel.
     *
     * @return edition panel
     */
    protected JPanel createEditionPanel() {
        // timetask label
        JLabel spinnerLabel = new JLabel(getResourceMap().getString("label.time.text"));

        // timetask panel 
        JPanel spinnerPanel = createSpinnersPanel();

        // annotation label
        JLabel annotationLabel = new JLabel(getResourceMap().getString("label.annotation.text"));

        // timetask textarea
        annotationText = new JTextArea();
        annotationText.getDocument().addDocumentListener(annotationChangeListener);
        annotationText.setRows(4);
        annotationText.setLineWrap(true);
        annotationText.setWrapStyleWord(true);
        JScrollPane annotationPane = new JScrollPane(annotationText,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        // edition panel
        SpringLayout layout = new SpringLayout();
        JPanel panel = new JPanel(layout);
        panel.add(spinnerLabel);
        panel.add(annotationLabel);
        panel.add(spinnerPanel);
        panel.add(annotationPane);

        JLabel largerLabel = spinnerLabel.getText().length() < annotationLabel
                .getText().length() ? annotationLabel : spinnerLabel;

        // layout constraints
        layout.putConstraint(SpringLayout.NORTH, spinnerLabel, 2,
                SpringLayout.NORTH, panel);
        layout.putConstraint(SpringLayout.WEST, spinnerLabel, 2,
                SpringLayout.WEST, panel);

        layout.putConstraint(SpringLayout.NORTH, spinnerPanel, 2,
                SpringLayout.NORTH, panel);
        layout.putConstraint(SpringLayout.WEST, spinnerPanel, 2,
                SpringLayout.EAST, largerLabel);

        layout.putConstraint(SpringLayout.NORTH, annotationLabel, 1,
                SpringLayout.SOUTH, spinnerPanel);
        layout.putConstraint(SpringLayout.WEST, annotationLabel, 2,
                SpringLayout.WEST, panel);

        layout.putConstraint(SpringLayout.NORTH, annotationPane, 1,
                SpringLayout.SOUTH, spinnerPanel);
        layout.putConstraint(SpringLayout.WEST, annotationPane, 2,
                SpringLayout.EAST, largerLabel);

        layout.putConstraint(SpringLayout.SOUTH, panel, 2, SpringLayout.SOUTH,
                annotationPane);
        layout.putConstraint(SpringLayout.EAST, panel, 2, SpringLayout.EAST,
                annotationPane);

        return panel;
    }

    /**
     * Create JXMonthView.
     *
     * @return month view component
     */
    protected JXMonthView createJXMonthView() {
        monthView = new JXMonthView();
        monthView.setTraversable(true);
        monthView.setFirstDayOfWeek(Calendar.MONDAY);
        monthView.setFlaggedDayForeground(Color.BLUE);

        // single selection
        monthView.setSelectionMode(DateSelectionModel.SelectionMode.SINGLE_SELECTION);

        // upper selectable day
        Calendar yesterdayCalendar = Calendar.getInstance();
        yesterdayCalendar.add(Calendar.DAY_OF_YEAR, -1);
        monthView.setUpperBound(yesterdayCalendar.getTime());

        // selected day
        monthView.setSelectionDate(yesterdayCalendar.getTime());

        // set sunday red
        monthView.setDayForeground(Calendar.SUNDAY, Color.RED);

        // listener on monthview selection day 
        monthView.addActionListener(e -> {
            if (JXMonthView.COMMIT_KEY.equals(e.getActionCommand())) {
                setSelectedDay(monthView.getFirstSelectionDate());
            }
        });

        return monthView;
    }

    /**
     * Create three spinners panel.
     *
     * @return spinner panel
     */
    protected JPanel createSpinnersPanel() {
        // hours spinner
        spinnerH = new JSpinner(new SpinnerNumberModel(0, 0, 24, 1));
        spinnerH.setEditor(new JSpinner.NumberEditor(spinnerH, "00"));

        // minutes spinner
        spinnerM = new JSpinner(new SpinnerNumberModel(0, -1, 60, 1));
        spinnerM.setEditor(new JSpinner.NumberEditor(spinnerM, "00"));
        spinnerM.setPreferredSize(spinnerH.getPreferredSize());

        // seconds spinner
        spinnerS = new JSpinner(new SpinnerNumberModel(0, -1, 60, 1));
        spinnerS.setEditor(new JSpinner.NumberEditor(spinnerS, "00"));
        spinnerS.setPreferredSize(spinnerH.getPreferredSize());

        // add listener on spinners
        spinnerListener = new SpinnerListener();
        spinnerH.addChangeListener(spinnerListener);
        spinnerM.addChangeListener(spinnerListener);
        spinnerS.addChangeListener(spinnerListener);

        // create panel
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        panel.add(spinnerH);
        panel.add(new JLabel(":"));
        panel.add(spinnerM);
        panel.add(new JLabel(":"));
        panel.add(spinnerS);

        return panel;
    }

    /**
     * Listener on spinnerH, spinnerM and spinnerS.
     */
    protected class SpinnerListener implements ChangeListener {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (e.getSource() == spinnerM) {
                int value = ((SpinnerNumberModel) spinnerM.getModel()).getNumber().intValue();

                if (value >= 60) {
                    if (spinnerH.getNextValue() != null) {
                        spinnerM.setValue(value - 60);
                        spinnerH.setValue(spinnerH.getNextValue());
                    } else {
                        spinnerM.setValue(value - 1);
                    }

                } else if (value < 0) {
                    if (spinnerH.getPreviousValue() != null) {
                        spinnerM.setValue(value + 60);
                        spinnerH.setValue(spinnerH.getPreviousValue());
                    } else {
                        spinnerM.setValue(0);
                    }
                }

            } else if (e.getSource() == spinnerS) {
                int value = ((SpinnerNumberModel) spinnerS.getModel()).getNumber().intValue();

                if (value >= 60) {
                    if (spinnerM.getNextValue() != null) {
                        spinnerS.setValue(value - 60);
                        spinnerM.setValue(spinnerM.getNextValue());
                    } else {
                        spinnerS.setValue(value - 1);
                    }

                } else if (value < 0) {
                    int hour = ((SpinnerNumberModel) spinnerH.getModel()).getNumber().intValue();

                    int minute = ((SpinnerNumberModel) spinnerM.getModel()).getNumber().intValue();

                    if (hour != 0 || minute != 0) {
                        spinnerS.setValue(value + 60);
                        spinnerM.setValue(spinnerM.getPreviousValue());
                    } else {
                        spinnerS.setValue(0);
                    }
                }
            }

            updateTask();
        }
    }

    protected class TextChangeListener implements DocumentListener {
        @Override
        public void changedUpdate(DocumentEvent e) {
            updateTask(this);
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            updateTask(this);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateTask(this);
        }
    }

    /**
     * Monthview selected day.
     *
     * @return monthview selected day
     */
    public Calendar getSelectedDay() {
        return selectedDay;
    }

    /**
     * set monthview selected day 
     * @param date day to select
     */
    public void setSelectedDay(Date date) {
        if (date != null) {
            displayTask(date);
        } else {
            // display last selectable day tasktime and annotation
            Calendar yesterDay = Calendar.getInstance();
            yesterDay.add(Calendar.DAY_OF_YEAR, -1);
            displayTask(yesterDay.getTime());
        }
    }

    /**
     * update spinners and annotation textarea according to selected day.
     *
     * @param date task date
     */
    protected void displayTask(Date date) {

        selectedDay = Calendar.getInstance();
        selectedDay.setTime(date);

        if (log.isDebugEnabled()) {
            log.debug("Displaying information on: " + date);
        }

        // spinners
        spinnerH.removeChangeListener(spinnerListener);
        spinnerM.removeChangeListener(spinnerListener);

        long time = cloneTask.getTime(date);
        Calendar cal = Calendar.getInstance();
        // FIXME me, not use a calendar here !
        cal.setTimeInMillis(time - cal.get(Calendar.ZONE_OFFSET));

        spinnerH.setValue(cal.get(Calendar.HOUR_OF_DAY));
        spinnerM.setValue(cal.get(Calendar.MINUTE));
        spinnerS.setValue(cal.get(Calendar.SECOND));

        spinnerH.addChangeListener(spinnerListener);
        spinnerM.addChangeListener(spinnerListener);

        // annotation
        annotationText.getDocument().removeDocumentListener(
                annotationChangeListener);

        annotationText.setText("");
        List<String> notes = TimerTaskHelper.getAnnotation(cloneTask, date);
        String carriage = "";
        for (String note : notes) {
            annotationText.setText(carriage + annotationText.getText() + note);
            carriage = "\n";
        }
        annotationText.setCaretPosition(0);
        annotationText.getDocument().addDocumentListener(
                annotationChangeListener);

        // title
        titleText.getDocument().removeDocumentListener(titleChangeListener);
        titleText.setText(cloneTask.getName());
        titleText.setCaretPosition(0);
        titleText.getDocument().addDocumentListener(titleChangeListener);
    }

    /**
     * update tasktime according to selected date on jxmonthview
     *
     * @param issuer of change
     */
    protected void updateTask(TextChangeListener issuer) {
        if (issuer == titleChangeListener) {
            isTitleChanged = true;
        } else if (issuer == annotationChangeListener) {
            isAnnotationChanged = true;
        }
        updateTask();
    }

    /**
     * update tasktime according to selected date on jxmonthview
     */
    protected void updateTask() {

        // time
        long seconds = ((Integer) spinnerH.getValue() * 60 * 60)
                + ((Integer) spinnerM.getValue() * 60)
                + ((Integer) spinnerS.getValue());
        cloneTask.setTime(getSelectedDay().getTime(), seconds * 1000);

        // annotation
        if (isAnnotationChanged) {
            TimerTaskHelper.removeAnnotation(cloneTask, getSelectedDay()
                    .getTime());
            String note = annotationText.getText();

            if (note != null) {
                Calendar cal = getSelectedDay();
                StringTokenizer st = new StringTokenizer(note, "\n");
                while (st.hasMoreTokens()) {
                    cal.add(Calendar.SECOND, 1);
                    cloneTask.addAnnotation(cal.getTime(), st.nextToken());
                }
                annotationChanged.add(getSelectedDay().getTime());
            }
            isAnnotationChanged = false;
        }

        // title
        if (isTitleChanged) {
            cloneTask.setName(titleText.getText());
        }

        dateChanged.add(getSelectedDay().getTime());
        setDataChanged(true);

        updateFlaggedDates();
    }

    /**
     * flag calendar days according to tasktime
     */
    public void updateFlaggedDates() {
        List<Date> dates = cloneTask.getAllDaysAndTimes().keySet().stream().filter(date -> cloneTask.getTime(date) > 0).collect(Collectors.toList());
        dates.addAll(cloneTask.getAllDaysAnnotations().keySet());
        monthView.setFlaggedDates(dates.toArray(new Date[dates.size()]));
    }

    @Action(enabledProperty = "dataChanged")
    public void apply() {

        getApplication().hide(this);

        try {
            // title
            if (isTitleChanged) {
                dataManager.editTask(task, cloneTask.getName());
            }
            for (Date date : dateChanged) {
                if (log.isDebugEnabled()) {
                    log.debug("Applying changes on: " + date);
                }

                // time 
                dataManager.changeTaskTime(task, date, cloneTask.getTime(date));

                // annotation 
                if (annotationChanged.contains(date)) {
                    TimerTaskHelper.removeAnnotation(task, date);

                    Map<Date, String> annotations = TimerTaskHelper.getAnnotationMap(cloneTask, date);
                    for (Date date2 : annotations.keySet()) {
                        dataManager.addAnnotation(task, date2, annotations.get(date2));
                    }
                }
            }
    
            /*dateChanged.clear();
            annotationChanged.clear();
            setDataChanged(false);
            isTitleChanged = false;
            isAnnotationChanged = false;*/
        } catch (DataViolationException ex) {
            parent.displayErrorMessage(ex.getExceptionKey());
        }
    }

    @Action
    public void cancel() {
        getApplication().hide(this);
    }

    public boolean isDataChanged() {
        return dataChanged;
    }

    public void setDataChanged(boolean dataChanged) {
        boolean oldValue = this.dataChanged;
        this.dataChanged = dataChanged;
        firePropertyChange("dataChanged", oldValue, dataChanged);
    }
}
