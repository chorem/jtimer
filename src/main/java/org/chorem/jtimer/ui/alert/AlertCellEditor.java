/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.alert;

import org.chorem.jtimer.entities.TimerAlert.Type;
import org.chorem.jtimer.ui.widget.DurationEditor;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Tree cell editor for alerts table.
 *
 * @author chatellier
 * @version $Revision: 1.0 $
 *
 * Last update : $Date: 6 juil. 2009 $
 * By : $Author: chatellier $
 */
public class AlertCellEditor extends AbstractCellEditor implements TableCellEditor, ItemListener, PropertyChangeListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = -363052829182024180L;

    /** Current editor. */
    protected Component editor;

    @Override
    public Object getCellEditorValue() {

        Object value = null;
        if (editor instanceof JComboBox) {
            value = ((JComboBox<?>) editor).getSelectedItem();
        } else if (editor instanceof DurationEditor) {
            value = ((DurationEditor) editor).getDuration();
        }
        return value;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        switch (column) {
            case 0:
                JComboBox<Type> combo = new JComboBox<>();
                combo.addItem(Type.REACH_DAILY_TIME);
                combo.addItem(Type.REACH_TOTAL_TIME);
                combo.setSelectedItem(value);
                combo.addItemListener(this);
                editor = combo;
                break;

            case 1:
                DurationEditor durationEditor = new DurationEditor(9999);
                long duration = (Long) value;
                durationEditor.setDuration(duration);
                durationEditor.addPropertyChangeListener("duration", this);
                durationEditor.setSize(100, 30);
                editor = durationEditor;
                break;

            default:
                break;
        }

        // restore super component properties
        if (isSelected) {
            editor.setBackground(table.getSelectionBackground());
        }

        return editor;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        stopCellEditing();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        stopCellEditing();
    }
}
