/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.alert;

import org.chorem.jtimer.entities.TimerAlert;
import org.chorem.jtimer.entities.TimerAlert.Type;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Table model for alerts.
 *
 * @author chatellier
 * @version $Revision: 1.0 $
 *
 * Last update : $Date: 6 juil. 2009 $
 * By : $Author: chatellier $
 */
public class AlertTableModel extends AbstractTableModel {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4103529427954019924L;

    /** Table column identifiers. */
    protected List<String> columnIdentifiers;

    /** Edition alerts list. */
    protected List<TimerAlert> alerts;

    /**
     * Contructor.
     *
     * @param alerts alerts list
     * @param columnIdentifiers columns identifiers
     */
    public AlertTableModel(List<TimerAlert> alerts, List<String> columnIdentifiers) {
        this.alerts = alerts;
        this.columnIdentifiers = columnIdentifiers;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {

        String columnName = columnIdentifiers.get(column);
        return columnName;
    }

    @Override
    public int getRowCount() {
        return alerts.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        TimerAlert alert = alerts.get(rowIndex);

        Object result = null;

        switch (columnIndex) {
            case 0:
                result = alert.getType();
                break;

            case 1:
                result = alert.getDuration() / 1000;
                break;

            default:
                break;
        }

        return result;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        TimerAlert alert = alerts.get(rowIndex);

        switch (columnIndex) {
            case 0:
                Type type = (Type) value;
                alert.setType(type);
                break;

            case 1:
                long duration = (Long) value;
                alert.setDuration(duration * 1000);
                break;

            default:
                break;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
}
