/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.alert;

import org.chorem.jtimer.entities.TimerAlert.Type;
import org.chorem.jtimer.ui.widget.DurationEditor;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;

/**
 * Tree cell editor for alerts table.
 *
 * @author chatellier
 * @version $Revision: 1.0 $
 *
 * Last update : $Date: 6 juil. 2009 $
 * By : $Author: chatellier $
 */
public class AlertCellRenderer extends DefaultTableCellRenderer {

    /** serialVersionUID. */
    private static final long serialVersionUID = -363052829182024180L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                                                   int row, int column) {

        Component superComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        Component c = null;
        switch (column) {
            case 0:
                JComboBox<Type> combo = new JComboBox<>();
                combo.addItem(Type.REACH_DAILY_TIME);
                combo.addItem(Type.REACH_TOTAL_TIME);
                combo.setSelectedItem(value);
                c = combo;
                break;

            case 1:
                DurationEditor durationEditor = new DurationEditor(9999);
                long duration = (Long) value;
                durationEditor.setDuration(duration);
                c = durationEditor;
                break;

            default:
                break;
        }

        // restore super properties
        if (c != null) {
            c.setBackground(superComponent.getBackground());
        }

        return c;
    }
}
