/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.alert;

import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerAlert;
import org.chorem.jtimer.entities.TimerAlert.Type;
import org.chorem.jtimer.entities.TimerTask;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Alert editor dialog.
 *
 * @author chatellier
 * @version $Revision: 1.0 $
 *
 * Last update : $Date: 6 juil. 2009 $
 * By : $Author: chatellier $
 */
public class AlertEditor extends FrameView implements ListSelectionListener {

    /** Task to manage alert on. */
    protected TimerTask task;

    /** List of cloned task alert list. */
    protected List<TimerAlert> alerts;

    /** Manager to commit alert modification. */
    protected TimerDataManager timerDataManager;

    /** Table to display alerts .*/
    protected JTable alertTable;

    /** Alert model for table. */
    protected AlertTableModel alertModel;

    /** Selected alert property. */
    protected boolean selectedAlert;

    /**
     * Constructor.
     *
     * @param application parent
     * @param task task
     * @param timerDataManager
     */
    public AlertEditor(Application application, TimerDataManager timerDataManager, TimerTask task) {
        super(application);
        this.task = task;
        this.timerDataManager = timerDataManager;

        // build a new editing list for alerts
        alerts = task.getAlerts().stream().map(TimerAlert::clone).collect(Collectors.toList());

        // rename frame to get proper position
        getFrame().setName("alertFrame");
        getFrame().setTitle(getResourceMap().getString("alert.title"));

        setComponent(getMainComponent());
    }

    /**
     * Build main component.
     *
     * @return main component
     */
    protected JComponent getMainComponent() {

        JPanel panel = new JPanel(new GridBagLayout());

        JLabel label = new JLabel(getResourceMap().getString("alert.alertlist"));
        panel.add(label, new GridBagConstraints(0, 0, 2, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        // get column labels
        List<String> columnIdentifiers = new ArrayList<>();
        columnIdentifiers.add(getResourceMap().getString("alert.type"));
        columnIdentifiers.add(getResourceMap().getString("alert.duration"));

        alertModel = new AlertTableModel(alerts, columnIdentifiers);
        alertTable = new JTable(alertModel);
        alertTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        alertTable.getColumnModel().getColumn(0).setCellEditor(new AlertCellEditor());
        alertTable.getColumnModel().getColumn(1).setCellEditor(new AlertCellEditor());
        alertTable.getColumnModel().getColumn(0).setCellRenderer(new AlertCellRenderer());
        alertTable.getColumnModel().getColumn(1).setCellRenderer(new AlertCellRenderer());
        alertTable.setRowHeight(30);
        alertTable.getSelectionModel().addListSelectionListener(this);

        JScrollPane sp = new JScrollPane(alertTable);
        panel.add(sp, new GridBagConstraints(0, 1, 2, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));

        JButton addButton = new JButton();
        addButton.setAction(getContext().getActionMap(this).get("addAlert"));
        panel.add(addButton, new GridBagConstraints(0, 2, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        JButton removeButton = new JButton();
        removeButton.setAction(getContext().getActionMap(this).get(
                "removeAlert"));
        panel.add(removeButton, new GridBagConstraints(1, 2, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        JButton cancelButton = new JButton();
        cancelButton.setAction(getContext().getActionMap(this).get("cancel"));
        panel.add(cancelButton, new GridBagConstraints(0, 3, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        JButton saveButton = new JButton();
        saveButton.setAction(getContext().getActionMap(this).get("save"));
        panel.add(saveButton, new GridBagConstraints(1, 3, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        return panel;
    }

    /**
     * Is selected alert.
     *
     * @return selected alert property
     */
    public boolean isSelectedAlert() {
        return selectedAlert;
    }

    /**
     * Change selected alert property.
     *
     * @param selectedAlert selected alert property
     */
    public void setSelectedAlert(boolean selectedAlert) {
        boolean oldValue = this.selectedAlert;
        this.selectedAlert = selectedAlert;
        firePropertyChange("selectedAlert", oldValue, selectedAlert);
    }

    @Action
    public void addAlert() {
        TimerAlert alert = new TimerAlert();
        // set default value, too hard to manage with null values :(
        alert.setType(Type.REACH_DAILY_TIME);
        alerts.add(alert);
        alertModel.fireTableDataChanged();
    }

    @Action(enabledProperty = "selectedAlert")
    public void removeAlert() {
        int selectedRow = alertTable.getSelectedRow();
        TimerAlert alert = alerts.get(selectedRow);
        alerts.remove(alert);
        alertModel.fireTableDataChanged();
    }

    @Action
    public void save() {

        // filter null type alert (forbidden)
        task.setAlert(alerts);

        timerDataManager.modifyAlert(task);
        getApplication().hide(this);
    }

    @Action
    public void cancel() {
        getApplication().hide(this);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        // used to update remove button property
        setSelectedAlert(e.getFirstIndex() >= 0);
    }
}
