/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tree;

import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerTask;

import javax.swing.JCheckBox;
import javax.swing.JTree;
import java.awt.Color;
import java.awt.Component;
import java.util.Set;

/**
 * JCheckBox tree node cell component.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public abstract class CheckBoxTreeCellComponent extends JCheckBox {

    /** serialVersionUID */
    private static final long serialVersionUID = 2497464481840318274L;

    /** Timer core. */
    protected TimerCore core;

    /** Parent tree. */
    protected JTree parentTree;

    /** Unchecked task set. */
    protected Set<TimerTask> uncheckedTaskSet;

    /**
     * Constructor.
     *
     * @param core core reference
     * @param parentTree tree reference
     * @param uncheckedTaskList common unselected tasks list
     */
    protected CheckBoxTreeCellComponent(TimerCore core, JTree parentTree, Set<TimerTask> uncheckedTaskList) {
        this.core = core;
        this.parentTree = parentTree;
        this.uncheckedTaskSet = uncheckedTaskList;
    }

    /**
     * Generic check box component.
     *
     * @param tree tree
     * @param value value
     * @param selected selected
     * @param expanded expanded
     * @param leaf leaf
     * @param row row
     * @return check box component
     */
    protected Component getCheckBoxComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf,
                                             int row) {

        // if this is a task
        if (value instanceof TimerTask) {
            TimerTask task = (TimerTask) value;

            // task name should not be "null"
            this.setText(task.getName());

            // add color if task is closed
            if (task.isClosed()) {
                this.setForeground(Color.GRAY);
            } else {
                this.setForeground(Color.BLACK);
            }

            // set checked ?
            if (uncheckedTaskSet.contains(task)) {
                this.setSelected(false);
            } else {
                this.setSelected(true);
            }
        }

        return this;
    }
}
