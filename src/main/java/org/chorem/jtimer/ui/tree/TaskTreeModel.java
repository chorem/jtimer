/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.jdesktop.swingx.tree.TreeModelSupport;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Timer task tree model.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TaskTreeModel implements TreeModel {

    /** Class logger. */
    private static Log log = LogFactory.getLog(TaskTreeModel.class);

    protected TreeModelSupport modelSupport;

    /** Timer core. */
    protected TimerCore core;

    /** Tree model root. */
    protected TimerTask root;

    /** Show closed tasks. */
    protected boolean showClosed;

    /**
     * Constructor.
     *
     * @param core core
     * @param rootName root node name
     */
    public TaskTreeModel(TimerCore core, String rootName) {
        this.core = core;
        root = new TimerTask(rootName);
        modelSupport = new TreeModelSupport(this);
    }

    public TreeModelSupport getModelSupport() {
        return modelSupport;
    }

    @Override
    public Object getChild(Object parent, int index) {
        TimerTask child = getChildrenFor(parent).get(index);
        return child;
    }

    @Override
    public int getChildCount(Object parent) {
        int count = getChildrenFor(parent).size();
        return count;
    }

    /**
     * Recupere la sous liste: data.getProjectsList() si parent = root
     * getSubTasks() sinon
     *
     * @param parent parent to task sublist
     * @return children
     */
    protected List<TimerTask> getChildrenFor(Object parent) {

        List<TimerTask> result = getFiteredSubListFor(parent);

        return result;
    }

    /**
     * Recupere la sous liste: data.getProjectsList() si parent = root
     * getSubTasks() sinon
     *
     * @param parent parent to task sublist
     * @return filtered list
     */
    protected List<TimerTask> getFiteredSubListFor(Object parent) {

        List<TimerTask> result = new ArrayList<>();

        // get correct list
        if (parent == root) { // case root node
            result.addAll(core.getData().getProjectsList());
        } else { // not root node
            TimerTask task = (TimerTask) parent;
            result.addAll(task.getSubTasks());
        }

        // filter list, if only show closed
        if (!showClosed) {
            result = result.stream()
                    .filter(task -> !task.isClosed())
                    .collect(Collectors.toList());
        }

        // Since sort is not supported by the table, do a manual sorting.
        result = TimerTaskHelper.sortTask(result);

        return result;
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int index = getChildrenFor(parent).indexOf(child);
        return index;
    }

    @Override
    public boolean isLeaf(Object node) {
        return getChildCount(node) == 0;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        modelSupport.addTreeModelListener(l);
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        modelSupport.removeTreeModelListener(l);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    /**
     * Show closed.
     *
     * @param closed closed value
     */
    public void setShowClosed(boolean closed) {
        if (log.isDebugEnabled()) {
            log.debug("Property changed showClosed : " + closed);
        }
        showClosed = closed;
        // FIXME change this
        modelSupport.fireNewRoot();
    }
}
