/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;

import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.Set;

/**
 * JCheckBox tree node cell editor.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class CheckBoxTreeCellEditor extends CheckBoxTreeCellComponent implements
        TreeCellEditor, ItemListener {

    /** serialVersionUID */
    private static final long serialVersionUID = 2497464481840318274L;

    /** log */
    private static Log log = LogFactory.getLog(CheckBoxTreeCellEditor.class);

    /** {@link CellEditorListener}s */
    protected Collection<CellEditorListener> cellEditorListeners;

    /**
     * Constructor.
     *
     * @param core core
     * @param parentTree tree
     * @param uncheckedTaskList common unselected tasks list
     */
    public CheckBoxTreeCellEditor(TimerCore core, JTree parentTree, Set<TimerTask> uncheckedTaskList) {
        super(core, parentTree, uncheckedTaskList);
        addItemListener(this);

        cellEditorListeners = new ArrayList<>();
    }

    @Override
    public Component getTreeCellEditorComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                boolean leaf, int row) {
        return getCheckBoxComponent(tree, value, selected, expanded, leaf, row);
    }

    @Override
    public Object getCellEditorValue() {
        return this;
    }

    @Override
    public void addCellEditorListener(CellEditorListener l) {
        cellEditorListeners.add(l);
    }

    @Override
    public void cancelCellEditing() {
        ChangeEvent e = new ChangeEvent(this);
        for (CellEditorListener cellEditorListener : cellEditorListeners) {
            cellEditorListener.editingCanceled(e);
        }
    }

    @Override
    public boolean isCellEditable(EventObject evt) {
        return true;
    }

    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        cellEditorListeners.remove(l);
    }

    /**
     * Cell selection.
     *
     * False, can click without select.
     */
    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return false;
    }

    @Override
    public boolean stopCellEditing() {
        ChangeEvent e = new ChangeEvent(this);
        for (CellEditorListener cellEditorListener : cellEditorListeners) {
            cellEditorListener.editingStopped(e);
        }
        return true;
    }

    /*
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    @Override
    public void itemStateChanged(ItemEvent e) {

        JCheckBox checkbox = (JCheckBox) e.getSource();

        if (log.isDebugEnabled()) {
            log.debug("Tree checkbox state changed on " + checkbox.getText()
                    + "(" + checkbox.isSelected() + ")");
        }

        TreePath editingPath = parentTree.getEditingPath();
        if (editingPath != null) {
            if (log.isDebugEnabled()) {
                log.debug("Current edition = " + editingPath);
            }
            updateChildren(editingPath, checkbox.isSelected());

            // too refresh selected/unselected checkbox
            parentTree.repaint();
        }

        // exit editing mode
        stopCellEditing();
    }

    /**
     * Recursive update task sub task.
     *
     * @param treePath current tree path
     * @param select select (true) or deselect
     */
    protected void updateChildren(TreePath treePath, boolean select) {
        TimerTask lastPathComponent = (TimerTask) treePath
                .getLastPathComponent();

        if (log.isDebugEnabled()) {
            log.debug("Recursive modification of = "
                    + lastPathComponent.getName());
        }

        if (select) {
            uncheckedTaskSet.remove(lastPathComponent);
        } else {
            uncheckedTaskSet.add(lastPathComponent);
        }

        // special case, root node selection
        if (parentTree.getModel().getRoot() == lastPathComponent) {
            for (TimerProject project : core.getData().getProjectsList()) {
                TreePath subTreePath = treePath.pathByAddingChild(project);
                updateChildren(subTreePath, select);
            }
        } else {
            for (TimerTask subtask : lastPathComponent.getSubTasks()) {
                TreePath subTreePath = treePath.pathByAddingChild(subtask);
                updateChildren(subTreePath, select);
            }
        }
    }
}
