/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tree;

import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.List;

/**
 * ProjectAndTaskTree.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ProjectsAndTasksTree extends JTree {

    /** serialVersionUID. */
    private static final long serialVersionUID = -6909972377431117193L;

    /**
     * Set selected task in tree.
     *
     * @param task task
     */
    public void setSelectedTask(TimerTask task) {
        // compute task tree path
        List<TimerTask> components = TimerTaskHelper.getPathFromParent(task);
        components.add(0, (TimerTask) getModel().getRoot());

        // select tree path in reverse order (from root to leaf)
        TreePath path = new TreePath(components.toArray());
        getSelectionModel().setSelectionPath(path);
    }
}
