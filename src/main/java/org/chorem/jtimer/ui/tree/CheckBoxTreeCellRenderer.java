/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.tree;

import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerTask;

import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;
import java.awt.Component;
import java.util.Set;

/**
 * JCheckBox tree node cell renderer.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class CheckBoxTreeCellRenderer extends CheckBoxTreeCellComponent
        implements TreeCellRenderer {

    /** serialVersionUID */
    private static final long serialVersionUID = 2497464481840318274L;

    /**
     * Constructor.
     *
     * @param core core
     * @param parentTree tree
     * @param uncheckedTaskList common unselected tasks list
     */
    public CheckBoxTreeCellRenderer(TimerCore core, JTree parentTree,
                                    Set<TimerTask> uncheckedTaskList) {
        super(core, parentTree, uncheckedTaskList);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {
        Component c = getCheckBoxComponent(tree, value, selected, expanded, leaf, row);

        // fix a strange bug where checkbox does have a "gray" background
        c.setBackground(tree.getBackground());

        return c;
    }
}
