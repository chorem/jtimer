/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2016 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.jtimer.ui;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.DataViolationException;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.ui.widget.DialogView;
import org.jdesktop.application.Action;

import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Window;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * New task panel used to ask for:
 * <ul>
 * <li>Task name</li>
 * <li>Task template (if available)</li>
 * </ul>
 *
 * @author Eric Chatellier
 */
public class NewTaskView extends DialogView {

    /** log */
    private static Log log = LogFactory.getLog(NewTaskView.class);

    protected JTimer parent;
    protected TimerCore core;
    protected ActionMap actionMap;
    protected TimerTask selectedTask;
    protected TimerTask newTask;

    protected JTextField newTaskField;
    protected JComboBox<String> newTaskTemplateBox;

    public NewTaskView(Window owner, JTimer parent, TimerCore core, TimerTask selectedTask) {
        super(owner, parent);
        this.parent = parent;
        this.core = core;
        this.selectedTask = selectedTask;
        actionMap = parent.getContext().getActionMap(this);

        // modify frame name
        // otherwise, get parent frame dimension
        getDialog().setName("newTask");
        getDialog().setTitle(getResourceMap().getString("newTaskTitle"));

        setComponent(getMainComponent(selectedTask));
    }

    protected JPanel getMainComponent(TimerTask selectedTask) {
        JPanel panel = new JPanel(new GridBagLayout());

        // icon
        JLabel newTaskIcon = new JLabel(new ImageIcon(getClass().getResource("resources/date_task.png")));
        panel.add(newTaskIcon, new GridBagConstraints(0, 0, 1, 4, 0, 0,
                GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        // task name
        String shortTaskName = StringUtils.abbreviate(selectedTask.getName(), 50);
        JLabel newTaskLabel = new JLabel(getResourceMap().getString("input.newTaskMessage", shortTaskName));
        panel.add(newTaskLabel, new GridBagConstraints(1, 0, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        newTaskField = new JTextField();
        panel.add(newTaskField, new GridBagConstraints(1, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 5, 0, 5), 0, 0));

        // Task template
        Map<String, Object> templates = JTimer.config.getTaskTemplates();

        newTaskTemplateBox = new JComboBox<>();
        if (templates != null && !templates.isEmpty()) {

            // model
            Set<String> templateNames = templates.keySet();
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(templateNames.toArray(new String[templateNames.size()]));
            model.insertElementAt(null, 0); // empty option
            newTaskTemplateBox.setModel(model);
            newTaskTemplateBox.setSelectedItem(null); // empty option

            JLabel newTaskTemplateLabel = new JLabel(getResourceMap().getString("input.newTaskTemplate"));
            panel.add(newTaskTemplateLabel, new GridBagConstraints(1, 2, 1, 1, 0, 0,
                    GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));

            panel.add(newTaskTemplateBox, new GridBagConstraints(1, 3, 1, 1, 0, 0,
                    GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(3, 5, 0, 5), 0, 0));
        }

        // ok cancel buttons
        JPanel buttonPanel = new JPanel(new GridLayout(0, 2, 5, 0));
        JButton okButton = new JButton();
        okButton.setAction(actionMap.get("valid"));
        buttonPanel.add(okButton);
        // default enter
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "ENTER");
        getRootPane().getActionMap().put("ENTER", okButton.getAction());

        JButton cancelButton = new JButton();
        cancelButton.setAction(actionMap.get("cancel"));
        buttonPanel.add(cancelButton);
        // default escape
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "ESCAPE");
        getRootPane().getActionMap().put("ESCAPE", cancelButton.getAction());

        panel.add(buttonPanel, new GridBagConstraints(0, 4, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        return panel;
    }

    @Action
    public void valid() {
        String taskName = newTaskField.getText();
        String taskTemplate = (String) newTaskTemplateBox.getSelectedItem();

        // remove unneeded spaces
        taskName = taskName.trim();

        newTask = new TimerTask(taskName);

        // Fix creation date
        newTask.setCreationDate(new Date());

        try {
            core.getData().addTask(selectedTask, newTask, taskTemplate);
        } catch (DataViolationException e) {
            parent.displayErrorMessage(e.getExceptionKey());
            newTask = null;
        }

        getApplication().hide(this);
    }

    @Action
    public void cancel() {
        getApplication().hide(this);
    }

    /**
     * Returning new task after creation.
     *
     * @return new task
     */
    public TimerTask getTask() {
        return newTask;
    }
}
