/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.chorem.jtimer.data.DataEventListener;
import org.chorem.jtimer.data.TimerDataManager;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceManager;
import org.jdesktop.application.ResourceMap;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.util.Collection;
import java.util.Date;

/**
 * StatusBar.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date: 2008-06-17 10:50:40 +0200 (mar., 17 juin 2008)
 * $ By : $Author$
 */
public class StatusBar extends JPanel implements DataEventListener {

    /** VersionUID */
    private static final long serialVersionUID = -1283108564414026297L;

    /** I18n resources map */
    protected ResourceMap resourceMap;

    /** DataManager */
    protected TimerDataManager dataManager;

    /** Message label */
    protected JLabel lblMessage;

    /** Time label */
    protected JLabel lblTime;

    /**
     * Reference how many tasks are running
     */
    protected int nbTasksRunning = 0;

    /**
     * Constucteur.
     *
     * @param application parent application
     * @param dataManager data manager
     */
    public StatusBar(Application application, TimerDataManager dataManager) {
        // set layout
        super(new GridLayout());

        // init resources map
        ApplicationContext ctxt = application.getContext();
        ResourceManager mgr = ctxt.getResourceManager();
        resourceMap = mgr.getResourceMap(StatusBar.class);

        // message label
        lblMessage = new JLabel(resourceMap.getString("loadingMessage"));
        lblMessage.setBorder(BorderFactory.createEtchedBorder());
        add(lblMessage);

        // time label
        lblTime = new JLabel(resourceMap.getString("todayTotalMessage", "--"));
        lblTime.setBorder(BorderFactory.createEtchedBorder());
        lblTime.setHorizontalAlignment(SwingConstants.CENTER);
        add(lblTime);

        // save tdm
        this.dataManager = dataManager;
    }

    /**
     * Udpate today time in status bar.
     */
    protected void updateTodayTime() {
        // refresh time
        long duration = 0L;
        for (TimerProject p : dataManager.getProjectsList()) {
            duration += TimerTaskHelper.getTotalTime(p, new Date());
        }
        lblTime.setText(resourceMap
                .getString("todayTotalMessage", DurationFormatUtils
                        .formatDuration(duration, "HH:mm:ss")));
    }

    @Override
    public void addProject(TimerProject project) {
        // can happen for synchronized project
        updateTodayTime();
    }

    @Override
    public void addTask(TimerTask task) {
        // can happen for synchronized project
        updateTodayTime();
    }

    @Override
    public void dataLoaded(Collection<TimerProject> projects) {
        lblMessage.setText(resourceMap.getString("idleMessage"));

        // and call modifyTask
        modifyTask(null);
    }

    @Override
    public void deleteProject(TimerProject project) {
        updateTodayTime();
    }

    @Override
    public void deleteTask(TimerTask task) {
        updateTodayTime();
    }

    @Override
    public void setTaskTime(TimerTask task, Date date, Long time) {
        modifyTask(task);
    }

    @Override
    public void modifyTask(TimerTask task) {
        updateTodayTime();
    }

    @Override
    public void startTask(TimerTask task) {
        startStopTask(task, true);
    }

    @Override
    public void stopTask(TimerTask task) {
        startStopTask(task, false);
    }

    /**
     * Start and stop task common code.
     *
     * @param task task
     * @param start start(true) or stop (false)
     */
    protected void startStopTask(TimerTask task, boolean start) {

        // increment or decrement task
        if (start) {
            ++nbTasksRunning;
        } else {
            --nbTasksRunning;
        }

        // display message
        String message;
        if (nbTasksRunning == 0) {
            message = resourceMap.getString("idleMessage");
        } else {
            if (nbTasksRunning == 1) {
                message = resourceMap.getString("runningTaskMessage", nbTasksRunning);
            } else {
                message = resourceMap.getString("runningTasksMessage", nbTasksRunning);
            }
        }
        lblMessage.setText(message);
    }
}
