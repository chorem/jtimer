/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.report;

import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.entities.TimerProject;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Report generator class (process code).
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ReportGenerator {

    /** Class logger */
    private static Log log = LogFactory.getLog(ReportGenerator.class);

    /**
     * Report type
     */
    public enum Type {
        BY_DAY_REPORT, BY_WEEK_REPORT, BY_MONTH_REPORT, BY_YEAR_REPORT, BY_PROJECT_REPORT
    }

    /** Freemarker */
    protected Configuration freemarkerConfiguration;

    /**
     * Constructor.
     *
     * Init freemarker.
     */
    public ReportGenerator() {

        freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_23);

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(ReportGenerator.class, "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);

        freemarkerConfiguration.setObjectWrapper(new BeansWrapper(Configuration.VERSION_2_3_23));
    }

    /**
     * Make report.
     *
     * @param reportType type, (daily, weekly...)
     * @param projects projects sub set
     * @param begin begin date
     * @param end end date
     * @param includeTime include time
     * @param includeAnnotate include annotations
     * @param includeAnnotateTime include annotations time
     * @param includeIntermediateTotalTime include intermediate total time
     *
     * @return report text
     */
    public String getReportText(Type reportType, List<TimerProject> projects, Date begin, Date end, boolean includeTime,
                                boolean includeAnnotate, boolean includeAnnotateTime,
                                boolean includeIntermediateTotalTime) {

        Template template = null;
        String content = null;

        try {
            switch (reportType) {
                case BY_DAY_REPORT:
                    template = freemarkerConfiguration.getTemplate("reportByDay.ftl");
                    break;
                case BY_WEEK_REPORT:
                    template = freemarkerConfiguration.getTemplate("reportByWeek.ftl");
                    break;
                case BY_MONTH_REPORT:
                    template = freemarkerConfiguration.getTemplate("reportByMonth.ftl");
                    break;
                case BY_YEAR_REPORT:
                    template = freemarkerConfiguration.getTemplate("reportByYear.ftl");
                    break;
                case BY_PROJECT_REPORT:
                    template = freemarkerConfiguration.getTemplate("reportByProject.ftl");
                    break;
            }

            content = getReportContent(template, projects, begin, end, includeTime,
                    includeAnnotate, includeAnnotateTime, includeIntermediateTotalTime);
        } catch (IOException | TemplateException e) {
            if (log.isErrorEnabled()) {
                log.error("Can't get freemarker template", e);
            }
        }

        return content;
    }

    /**
     * Get report content with freemarker template execution.
     *
     * @param projects projects sub set
     * @param begin begin date
     * @param end end date
     * @param template freemarker template to use
     * @param includeTime include time
     * @param includeAnnotate include annotations
     * @param includeAnnotateTime include annotations time
     * @param includeIntermediateTotalTime include middle man total time
     *
     * @return string content
     *
     * @throws IOException if i/o exception occurs
     * @throws TemplateException if freemarker template exception occurs
     *
     * @see Template
     */
    protected String getReportContent(Template template, List<TimerProject> projects, Date begin, Date end,
                                      boolean includeTime, boolean includeAnnotate, boolean includeAnnotateTime,
                                      boolean includeIntermediateTotalTime) throws TemplateException, IOException {

        // Create the root hash
        Map<String, Object> root = new HashMap<>();

        root.put("projects", projects);
        root.put("begin", begin);
        root.put("end", end);
        root.put("includeTime", includeTime);
        root.put("annotations", includeAnnotate);
        root.put("annotationsTime", includeAnnotateTime);
        root.put("intermediateTotalTime", includeIntermediateTotalTime);
        root.put("utils", new ReportUtils());

        Writer out = new StringWriter();
        template.process(root, out);
        out.flush();

        return out.toString();
    }

}
