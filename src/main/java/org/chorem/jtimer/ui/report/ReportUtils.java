/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.report;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Report utility class for use in freemarker context.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ReportUtils {

    /**
     * Get date list, between to date (1 day interval).
     *
     * @param begin begin date
     * @param end end date
     * @return dates list
     */
    public List<Date> getDailyDates(Date begin, Date end) {
        List<Date> days = getPeriodDates(begin, end, Calendar.DAY_OF_YEAR);

        return days;
    }

    /**
     * Get date list, between to date (1 week interval).
     *
     * @param begin begin date
     * @param end end date
     * @return dates list
     */
    public List<Date> getWeeklyDates(Date begin, Date end) {
        List<Date> weeks = getPeriodDates(begin, end, Calendar.WEEK_OF_YEAR);

        return weeks;
    }

    /**
     * Get date list, between to date (1 month interval).
     *
     * @param begin begin date
     * @param end end date
     * @return dates list
     */
    public List<Date> getMonthlyDates(Date begin, Date end) {
        List<Date> months = getPeriodDates(begin, end, Calendar.MONTH);

        return months;
    }

    /**
     * Get date list, between to date (1 year interval).
     *
     * @param begin begin date
     * @param end end date
     * @return dates list
     */
    public List<Date> getYearlyDates(Date begin, Date end) {
        List<Date> years = getPeriodDates(begin, end, Calendar.YEAR);

        return years;
    }

    /**
     * Get date list, between to date.
     *
     * @param begin begin date
     * @param end end date
     * @param calendarField calendar field to increment
     * @return dates list
     */
    protected List<Date> getPeriodDates(Date begin, Date end, int calendarField) {
        List<Date> periods = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.setTime(begin);

        // don't add if equals 0
        while (cal.getTime().compareTo(end) <= 0) {
            periods.add(cal.getTime());
            cal.add(calendarField, 1);
        }

        // finally add last end date
        // permet de fixer la derniere date
        // ex : entre le vendredi et le lundi suivant
        // il y a moins de 1 semaine.
        // mais on doit quand meme compter 2 semaine
        Calendar calEnd = Calendar.getInstance();
        calEnd.setTime(end);
        if (cal.get(calendarField) == calEnd.get(calendarField)) {
            periods.add(end);
        }

        return periods;
    }

    /**
     * Get task proper time for a day.
     *
     * @param task task
     * @param day day
     * @return duration in seconds
     */
    public long getDailyTaskTime(TimerTask task, Date day) {
        long result = task.getTime(day);

        return result;
    }

    /**
     * Get task proper time for a week.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the week <code>week</code>. Thus you can get weekly task time from tuesday to wednesday.
     *
     * @param task task
     * @param week week
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return duration in seconds
     */
    public long getWeeklyTaskTime(TimerTask task, Date week, Date fromDay,
                                  Date toDay) {
        long result = 0;

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(week);
        beginPeriodDate.set(Calendar.DAY_OF_WEEK, beginPeriodDate
                .getFirstDayOfWeek());

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.WEEK_OF_YEAR, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        Calendar loopPeriodDate = beginPeriodDate;

        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate
                    .getTime()) >= 0)) {
                result += task.getTime(loopPeriodDate.getTime());
            }
            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Get task proper time for a month.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the month <code>month</code>. Thus you can get monthly task time from third day to twelfth day.
     *
     * @param task task
     * @param month month
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return duration in seconds
     */
    public long getMonthlyTaskTime(TimerTask task, Date month, Date fromDay, Date toDay) {
        long result = 0;

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(month);
        beginPeriodDate.set(Calendar.DAY_OF_MONTH, 1);

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.MONTH, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        Calendar loopPeriodDate = beginPeriodDate;

        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate
                    .getTime()) >= 0)) {
                result += task.getTime(loopPeriodDate.getTime());
            }

            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Get task proper time for a year.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the year <code>year</code>. Thus you can get yearly task time from 03/01 to 31/07.
     *
     * @param task task
     * @param year year
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return duration in seconds
     */
    public long getYearlyTaskTime(TimerTask task, Date year, Date fromDay, Date toDay) {
        long result = 0;

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(year);
        beginPeriodDate.set(Calendar.DAY_OF_MONTH, 1);
        beginPeriodDate.set(Calendar.MONTH, Calendar.JANUARY);

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.YEAR, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        Calendar loopPeriodDate = beginPeriodDate;

        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate.getTime()) >= 0)) {
                result += task.getTime(loopPeriodDate.getTime());
            }

            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Get task total time (including subtask) for a day.
     *
     * @param task task
     * @param day day
     * @return duration in seconds
     */
    public long getDailyTotalTaskTime(TimerTask task, Date day) {
        long result = TimerTaskHelper.getTotalTime(task, day);

        return result;
    }

    /**
     * Get task total time (including subtask) for a week.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the week <code>week</code>. Thus you can get weekly task time from tuesday to wednesday.
     *
     * @param task task
     * @param week week
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return duration in seconds
     */
    public long getWeeklyTotalTaskTime(TimerTask task, Date week, Date fromDay, Date toDay) {
        long result = 0;

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(week);
        beginPeriodDate.set(Calendar.DAY_OF_WEEK, beginPeriodDate
                .getFirstDayOfWeek());

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.WEEK_OF_YEAR, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        Calendar loopPeriodDate = beginPeriodDate;

        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate.getTime()) >= 0)) {
                result += TimerTaskHelper.getTotalTime(task, loopPeriodDate.getTime());
            }
            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Get task total time (including subtask) for a month.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the month <code>month</code>. Thus you can get monthly task time from third day to twelfth day.
     *
     * @param task task
     * @param month month
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return duration in seconds
     */
    public long getMonthlyTotalTaskTime(TimerTask task, Date month, Date fromDay, Date toDay) {
        long result = 0;

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(month);
        beginPeriodDate.set(Calendar.DAY_OF_MONTH, 1);

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.MONTH, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        Calendar loopPeriodDate = beginPeriodDate;

        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate.getTime()) >= 0)) {
                result += TimerTaskHelper.getTotalTime(task, loopPeriodDate.getTime());
            }

            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Get task total time (including subtask) for a year.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the year <code>year</code>. Thus you can get yearly task time from 03/01 to 31/07.
     *
     * @param task task
     * @param year year
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return duration in seconds
     */
    public long getYearlyTotalTaskTime(TimerTask task, Date year, Date fromDay, Date toDay) {
        long result = 0;

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(year);
        beginPeriodDate.set(Calendar.DAY_OF_MONTH, 1);
        beginPeriodDate.set(Calendar.MONTH, Calendar.JANUARY);

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.YEAR, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        Calendar loopPeriodDate = beginPeriodDate;

        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate.getTime()) >= 0)) {
                result += TimerTaskHelper.getTotalTime(task, loopPeriodDate.getTime());
            }

            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Format duration in 00:00:00 format.
     *
     * @param duration duration in seconds
     * @return formated duration
     */
    public String formatDuration(long duration) {
        String result = DurationFormatUtils.formatDuration(duration, "HH:mm:ss");
        return result;
    }

    /**
     * Get task annotations for a day.
     *
     * @param task task
     * @param day day
     * @return annotations of the day
     */
    public Map<Date, String> getDailyTaskAnnotation(TimerTask task, Date day) {

        Map<Date, String> result = TimerTaskHelper.getAnnotationMap(task, day);

        return result;
    }

    /**
     * Get task annotations for a week.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the week <code>week</code>. Thus you can get weekly task annotations from tuesday to wednesday.
     *
     * @param task task
     * @param week week
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return annotations of the week
     */
    public Map<Date, String> getWeeklyTaskAnnotation(TimerTask task, Date week, Date fromDay, Date toDay) {

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(week);
        beginPeriodDate.set(Calendar.DAY_OF_WEEK, beginPeriodDate
                .getFirstDayOfWeek());

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.WEEK_OF_YEAR, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        return getPeriodTaskAnnotation(task, beginPeriodDate, endPeriodDate, fromDay, toDay);
    }

    /**
     * Get task annotations for a month.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the month <code>month</code>. Thus you can get monthly task annotations from third day to twelfth day.
     *
     * @param task task
     * @param month month
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return annotations of the month
     */
    public Map<Date, String> getMonthlyTaskAnnotation(TimerTask task, Date month, Date fromDay, Date toDay) {

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(month);
        beginPeriodDate.set(Calendar.DAY_OF_MONTH, 1);

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.MONTH, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        return getPeriodTaskAnnotation(task, beginPeriodDate, endPeriodDate, fromDay, toDay);
    }

    /**
     * Get task annotations for a year.
     * <code>fromDay</code> and <code>toDay</code> allows you to define lower and upper bound
     * in the year <code>year</code>. Thus you can get yearly task annotations from 03/01 to 31/07.
     *
     * @param task task
     * @param year year
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return annotations of the year
     */
    public Map<Date, String> getYearlyTaskAnnotation(TimerTask task, Date year, Date fromDay, Date toDay) {

        Calendar beginPeriodDate = Calendar.getInstance();
        beginPeriodDate.setTime(year);
        beginPeriodDate.set(Calendar.DAY_OF_MONTH, 1);
        beginPeriodDate.set(Calendar.MONTH, Calendar.JANUARY);

        Calendar endPeriodDate = (Calendar) beginPeriodDate.clone();
        endPeriodDate.add(Calendar.YEAR, 1);
        endPeriodDate.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        return getPeriodTaskAnnotation(task, beginPeriodDate, endPeriodDate, fromDay, toDay);
    }

    /**
     * Get task annotations for a period.
     *
     * @param task task
     * @param beginPeriodDate begin period calendar
     * @param endPeriodDate end period calendar
     * @param fromDay from day (inclusive)
     * @param toDay to day (inclusive)
     * @return annotations of the year
     */
    protected Map<Date, String> getPeriodTaskAnnotation(TimerTask task, Calendar beginPeriodDate, Calendar endPeriodDate, Date fromDay, Date toDay) {
        Map<Date, String> result = new HashMap<>();

        Calendar loopPeriodDate = beginPeriodDate;
        while (loopPeriodDate.compareTo(endPeriodDate) <= 0) {

            if ((fromDay == null || fromDay.compareTo(loopPeriodDate.getTime()) <= 0)
                    && (toDay == null || toDay.compareTo(loopPeriodDate
                    .getTime()) >= 0)) {
                Map<Date, String> anns = TimerTaskHelper
                        .getAnnotationMap(task, loopPeriodDate.getTime());
                result.putAll(anns);
            }

            loopPeriodDate.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    /**
     * Return task subtasks ordered by name.
     *
     * @param task task to get subtasks
     * @return subtask ordered by name
     * @since 1.4.0
     */
    public List<TimerTask> getSubTaskOrdered(TimerTask task) {
        List<TimerTask> subTask = new ArrayList<>(task.getSubTasks());
        TimerTaskHelper.sortTask(subTask);
        return subTask;
    }
}
