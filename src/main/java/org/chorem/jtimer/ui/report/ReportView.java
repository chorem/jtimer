/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.report;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.ui.report.ReportGenerator.Type;
import org.chorem.jtimer.ui.tree.CheckBoxTreeCellEditor;
import org.chorem.jtimer.ui.tree.CheckBoxTreeCellRenderer;
import org.chorem.jtimer.ui.tree.TaskTreeModel;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.FrameView;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Reports UI.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ReportView extends FrameView implements DocumentListener {

    /** Class logger */
    protected static Log log = LogFactory.getLog(ReportView.class);

    /** Timer core. */
    protected TimerCore core;

    /** Unselected task set. */
    protected Set<TimerTask> uncheckedTaskSet;

    /** Report sorting : day */
    protected JRadioButton radioByDay;
    /** Report sorting : month */
    protected JRadioButton radioByMonth;
    /** Report sorting : week */
    protected JRadioButton radioByWeek;
    /** Report sorting : year */
    protected JRadioButton radioByYear;
    /** Report sorting : project */
    protected JRadioButton radioByProject;

    /** Include task time on report. */
    protected JCheckBox checkIncludesTaskTimes;
    /** Include annotations on reports */
    protected JCheckBox checkIncludesAnnotations;
    /** Include annotations time on reports */
    protected JCheckBox checkIncludesAnnotationsTime;
    /** Include middle man total time on reports */
    protected JCheckBox checkIncludeIntermediateTotalTime;

    /** Date pickers, from... to */
    protected JXDatePicker datePickerFrom, datePickerTo;

    /** Project tree (with checkbox) */
    protected JTree projectsTree;

    /** Show hidden project box. */
    protected JCheckBox showHiddenProjectBox;

    /** Report process generator */
    protected ReportGenerator reportGenerator;

    /** Report output */
    protected JTextArea reportArea;

    /** Can send mail property. */
    protected boolean canSendMail;

    /**
     * Reposts constructor.
     *
     * @param application parent reference
     * @param core core reference
     */
    public ReportView(Application application, TimerCore core) {

        super(application);

        // modify frame name
        // otherwise, get parent frame dimension
        getFrame().setName("reportFrame");
        getFrame().setTitle(getResourceMap().getString("reportTitle"));

        this.core = core;
        this.uncheckedTaskSet = new HashSet<>();

        // set title
        //setTitle(resourceMap.getString("reportTitle"));

        setComponent(getMainComponent());

        reportGenerator = new ReportGenerator();

        // reset selection
        //uncheckedTaskSet.clear();
    }

    /**
     * Get main view component.
     *
     * TODO use less complicated UI (no gbl)
     * @return main component
     */
    protected JComponent getMainComponent() {

        JPanel configComponent = new JXTaskPaneContainer();

        // panel for options
        JXTaskPane panelGeneral = new JXTaskPane(getResourceMap().getString("reportGeneral"));
        panelGeneral.setLayout(new GridBagLayout());
        panelGeneral.setSpecial(true);

        // first date picker
        JLabel labelFrom = new JLabel(getResourceMap().getString("reportFrom"));
        panelGeneral.add(labelFrom, new GridBagConstraints(0, 0, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        datePickerFrom = new JXDatePicker();
        datePickerFrom.getMonthView().setDayForeground(Calendar.SUNDAY, Color.RED);
        datePickerFrom.setFormats(DateFormat.getDateInstance(DateFormat.FULL));
        panelGeneral.add(datePickerFrom, new GridBagConstraints(1, 0, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        // second date picker
        JLabel labelTo = new JLabel(getResourceMap().getString("reportTo"));
        panelGeneral.add(labelTo, new GridBagConstraints(0, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        datePickerTo = new JXDatePicker();
        datePickerTo.getMonthView().setDayForeground(Calendar.SUNDAY, Color.RED);
        datePickerTo.setFormats(DateFormat.getDateInstance(DateFormat.FULL));
        panelGeneral.add(datePickerTo, new GridBagConstraints(1, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        // set current week by default
        currentWeek();

        // action button to show current or previous week
        JButton currentWeekButton = new JButton();
        currentWeekButton.setBorder(BorderFactory.createEmptyBorder());
        currentWeekButton.setAction(getContext().getActionMap(this).get("currentWeek"));
        panelGeneral.add(currentWeekButton, new GridBagConstraints(2, 0, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        JButton previousWeekButton = new JButton();
        previousWeekButton.setBorder(BorderFactory.createEmptyBorder());
        previousWeekButton.setAction(getContext().getActionMap(this).get("previousWeek"));
        panelGeneral.add(previousWeekButton, new GridBagConstraints(2, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        // Option for period grouping
        JPanel typePanel = new JPanel(new GridLayout(0, 2));
        radioByDay = new JRadioButton(getResourceMap().getString("reportDaily"), true);
        radioByMonth = new JRadioButton(getResourceMap().getString("reportMonthly"));
        radioByWeek = new JRadioButton(getResourceMap().getString("reportWeekly"));
        radioByYear = new JRadioButton(getResourceMap().getString("reportYearly"));
        radioByProject = new JRadioButton(getResourceMap().getString("reportByProject"));

        ButtonGroup group = new ButtonGroup();
        group.add(radioByDay);
        group.add(radioByMonth);
        group.add(radioByWeek);
        group.add(radioByYear);
        group.add(radioByProject);

        typePanel.add(radioByDay);
        typePanel.add(radioByMonth);
        typePanel.add(radioByWeek);
        typePanel.add(radioByYear);
        typePanel.add(radioByProject);
        panelGeneral.add(typePanel, new GridBagConstraints(0, 2, 3, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        // miscellaneous 
        JXTaskPane panelOption = new JXTaskPane(getResourceMap().getString("reportOptions"));
        panelOption.setLayout(new GridBagLayout());

        checkIncludesTaskTimes = new JCheckBox(getResourceMap().getString("reportIncludeTime"));
        checkIncludesTaskTimes.setSelected(true);
        panelOption.add(checkIncludesTaskTimes, new GridBagConstraints(0, 0, 2, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        checkIncludesAnnotations = new JCheckBox();
        checkIncludesAnnotations.setAction(getContext().getActionMap(this).get("reportAnnotations"));
        panelOption.add(checkIncludesAnnotations, new GridBagConstraints(0, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        checkIncludesAnnotationsTime = new JCheckBox(getResourceMap().getString("reportAnnotationsTime"));
        checkIncludesAnnotationsTime.setEnabled(false);
        panelOption.add(checkIncludesAnnotationsTime, new GridBagConstraints(1, 1, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        checkIncludeIntermediateTotalTime = new JCheckBox(getResourceMap().getString("reportIntermediateTotalTime"));
        panelOption.add(checkIncludeIntermediateTotalTime, new GridBagConstraints(0, 2, 2, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));

        // panel form projects
        JPanel panelProjects = new JXTaskPane(getResourceMap().getString("reportProjects"));
        panelProjects.setLayout(new GridBagLayout());

        // show hidden box
        showHiddenProjectBox = new JCheckBox(getContext().getActionMap(this).get("showHiddenProjects"));
        panelProjects.add(showHiddenProjectBox, new GridBagConstraints(0, 0, 1, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        projectsTree = new JTree();
        projectsTree.setRootVisible(true);
        projectsTree.setEditable(true);
        projectsTree.setModel(new TaskTreeModel(core, getResourceMap().getString("reportProjectsList") + " :"));
        projectsTree.setCellEditor(new CheckBoxTreeCellEditor(core, projectsTree, uncheckedTaskSet));
        projectsTree.setCellRenderer(new CheckBoxTreeCellRenderer(core, projectsTree, uncheckedTaskSet));

        JScrollPane jspTable = new JScrollPane(projectsTree);
        panelProjects.add(jspTable, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 0, 0, 0), 0, 0));

        configComponent.add(panelGeneral);
        configComponent.add(panelOption);
        configComponent.add(panelProjects);

        // panel for report text
        JPanel panelReports = new JPanel(new GridBagLayout());

        reportArea = new JTextArea();
        reportArea.setFont(new Font("Courier", Font.PLAIN, 12));
        reportArea.getDocument().addDocumentListener(this);
        JScrollPane jspReport = new JScrollPane(reportArea);
        panelReports.add(jspReport, new GridBagConstraints(0, 0, 3, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));

        // buttons
        JButton generateButton = new JButton();
        generateButton.setAction(getContext().getActionMap(this).get("generateReport"));
        panelReports.add(generateButton, new GridBagConstraints(0, 1, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));

        JButton sendMailButton = new JButton();
        sendMailButton.setAction(getContext().getActionMap(this).get("sendMail"));
        panelReports.add(sendMailButton, new GridBagConstraints(1, 1, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));

        JButton closeButton = new JButton();
        closeButton.setAction(getContext().getActionMap(this).get("closeView"));
        panelReports.add(closeButton, new GridBagConstraints(2, 1, 1, 1, 1, 0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));

        // color fix on linux ?
        configComponent.setBackground(panelReports.getBackground());
        // set minimum size to prevent "packed size" (too big)
        configComponent.setMinimumSize(new Dimension(200, 0));

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, configComponent, panelReports);
        splitPane.setOneTouchExpandable(true);
        return splitPane;
    }

    /**
     * Select current week in date pickers.
     */
    @Action
    public void currentWeek() {
        fillPickerDates(0);
    }

    /**
     * Display previous week in date pickers.
     */
    @Action
    public void previousWeek() {
        fillPickerDates(-1);
    }

    /**
     * Fill picker date with predefined week selection (from current)
     * and apply a delay (-1 = previous week).
     *
     * @param delay delay to add to current week
     */
    protected void fillPickerDates(int delay) {
        // init dates
        Calendar calendarBegin = Calendar.getInstance();
        int firstDayOfWeek = JTimer.config.getReportFirstDayOfWeek();
        if (firstDayOfWeek <= 0 || firstDayOfWeek > 7) {
            firstDayOfWeek = calendarBegin.getFirstDayOfWeek();
        }
        calendarBegin.set(Calendar.DAY_OF_WEEK, firstDayOfWeek);
        calendarBegin.set(Calendar.HOUR, 0);
        calendarBegin.set(Calendar.MINUTE, 0);
        calendarBegin.set(Calendar.SECOND, 0);
        calendarBegin.set(Calendar.MILLISECOND, 0);

        // calendar must be in current week by default
        if (calendarBegin.getTime().after(new Date())) {
            calendarBegin.add(Calendar.WEEK_OF_YEAR, -1);
        }

        // get end of week
        // take calendarBegin and add a week time
        Calendar calendarEnd = (Calendar) calendarBegin.clone();
        calendarEnd.add(Calendar.WEEK_OF_YEAR, 1);
        calendarEnd.add(Calendar.DAY_OF_YEAR, -1); // take the day before

        // apply delay
        calendarBegin.add(Calendar.WEEK_OF_YEAR, delay);
        calendarEnd.add(Calendar.WEEK_OF_YEAR, delay);

        datePickerFrom.setDate(calendarBegin.getTime());
        datePickerTo.setDate(calendarEnd.getTime());
    }

    /**
     * Close action.
     */
    @Action
    public void closeView() {
        getApplication().hide(this);
    }

    /**
     * Show annotation checkbox checked.
     */
    @Action
    public void reportAnnotations() {
        checkIncludesAnnotationsTime.setEnabled(checkIncludesAnnotations.isSelected());
    }

    /**
     * Make report.
     *
     * Set content in {@link #reportArea} text area.
     */
    @Action
    public void generateReport() {

        // Choose selected project type
        Type reportType;
        if (radioByDay.isSelected()) {
            reportType = Type.BY_DAY_REPORT;
        } else if (radioByMonth.isSelected()) {
            reportType = Type.BY_MONTH_REPORT;
        } else if (radioByWeek.isSelected()) {
            reportType = Type.BY_WEEK_REPORT;
        } else if (radioByYear.isSelected()) {
            reportType = Type.BY_YEAR_REPORT;
        } else {
            reportType = Type.BY_PROJECT_REPORT;
        }

        // get filtered project list
        // without non selected project and tasks
        List<TimerProject> selectedProjects = getSelectedProjects(core
                .getData().getProjectsList(), uncheckedTaskSet);

        // make report
        String report = reportGenerator.getReportText(reportType, selectedProjects, datePickerFrom.getDate(),
                datePickerTo.getDate(), checkIncludesTaskTimes.isSelected(), checkIncludesAnnotations.isSelected(),
                checkIncludesAnnotationsTime.isSelected(), checkIncludeIntermediateTotalTime.isSelected());

        if (report != null && !report.isEmpty()) {
            reportArea.setText(report);
        } else {
            reportArea.setText("");
        }
    }

    /**
     * Get project list without project and task unselected in tree.
     *
     * @param projects original project list
     * @param uncheckedTaskList unselected task list
     * @return selected project list
     */
    protected List<TimerProject> getSelectedProjects(
            List<TimerProject> projects, Collection<TimerTask> uncheckedTaskList) {

        List<TimerProject> currentProjects = new ArrayList<>();
        // take care of show closed option
        projects.stream().filter(project -> !project.isClosed() || showHiddenProjectBox.isSelected()).forEach(project -> {
            List<TimerTask> subTasks = getSelectedTasks(project.getSubTasks(),
                    uncheckedTaskList);

            // add current project only of at least one subtask is selected
            // or current project is selected
            if (!uncheckedTaskList.contains(project) || !subTasks.isEmpty()) {
                TimerProject clonedProject = project.clone();
                clonedProject.getSubTasks().clear();
                clonedProject.getSubTasks().addAll(subTasks);
                currentProjects.add(clonedProject);
            }
        });

        return currentProjects;
    }

    /**
     * Get tasks list without tasks unselected in tree.
     *
     * @param tasks original project list
     * @param uncheckedTaskList unselected task list
     * @return selected project list
     */
    protected List<TimerTask> getSelectedTasks(List<? extends TimerTask> tasks,
                                               Collection<TimerTask> uncheckedTaskList) {

        List<TimerTask> currentTask = new ArrayList<>();
        for (TimerTask task : tasks) {

            // take care of show closed option
            if (!task.isClosed() || showHiddenProjectBox.isSelected()) {
                List<TimerTask> subTasks = getSelectedTasks(task.getSubTasks(),
                        uncheckedTaskList);

                // add current task only of at least one subtask is selected
                // or current task is selected
                if (!uncheckedTaskList.contains(task) || !subTasks.isEmpty()) {
                    TimerTask clonedTask;
                    if (uncheckedTaskList.contains(task)) {
                        // to not show time of not selected task in report
                        clonedTask = new TimerTask(task.getName());
                    } else {
                        clonedTask = task.clone();
                    }
                    clonedTask.getSubTasks().clear();
                    clonedTask.getSubTasks().addAll(subTasks);
                    currentTask.add(clonedTask);
                }
            }
        }

        return currentTask;
    }

    /**
     * Update tree, showing hidden projects or not.
     */
    @Action
    public void showHiddenProjects() {
        TaskTreeModel model = (TaskTreeModel) projectsTree.getModel();
        model.setShowClosed(showHiddenProjectBox.isSelected());
    }

    /**
     * Send report by mail.
     */
    @Action(enabledProperty = "sendMailEnabled")
    public void sendMail() {

        String report = reportArea.getText();

        try {
            String encodedReport = URLEncoder.encode(report, "UTF-8");

            // bugfix for spaces
            encodedReport = encodedReport.replaceAll("\\+", "%20");

            URI mailtoURI = new URI("mailto:?body=" + encodedReport);
            Desktop.getDesktop().mail(mailtoURI);
        } catch (IOException | UnsupportedOperationException e) {
            if (log.isWarnEnabled()) {
                log.warn("Cannot open link (maybe default browser in not configured ?)");
            }
            if (log.isDebugEnabled()) {
                log.debug("Error while opening link", e);
            }
        } catch (URISyntaxException e) {
            if (log.isErrorEnabled()) {
                log.error("Error while opening link", e);
            }
        }
    }

    /**
     * Can send mail ?
     *
     * @return true if can send mail
     */
    public boolean isSendMailEnabled() {
        return canSendMail;
    }

    /**
     * Change can send mail property.
     *
     * @param enabled can send mail
     */
    public void setSendMailEnabled(boolean enabled) {
        boolean oldValue = canSendMail;
        canSendMail = enabled;
        firePropertyChange("sendMailEnabled", oldValue, canSendMail);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        documentChanged();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        documentChanged();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        documentChanged();
    }

    /**
     * Document content changed.
     *
     * Update can send mail property.
     */
    protected void documentChanged() {
        setSendMailEnabled(reportArea.getText().trim().length() > 0);
    }
}
