/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.data.DataViolationException;
import org.chorem.jtimer.data.TimerCore;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.system.SystemInfoFactory;
import org.chorem.jtimer.ui.HelpFrame;
import org.chorem.jtimer.ui.NewTaskView;
import org.chorem.jtimer.ui.StatusBar;
import org.chorem.jtimer.ui.TimerTaskEditor;
import org.chorem.jtimer.ui.alert.AlertEditor;
import org.chorem.jtimer.ui.report.ReportView;
import org.chorem.jtimer.ui.systray.SystrayManager;
import org.chorem.jtimer.ui.tasks.IdleDialog;
import org.chorem.jtimer.ui.tasks.RefreshTreeTask;
import org.chorem.jtimer.ui.tasks.RunTaskJob;
import org.chorem.jtimer.ui.treetable.ProjectsAndTasksTable;
import org.chorem.jtimer.ui.widget.WindowProperty2;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationContext;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;

import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;

/**
 * Main jTimer application window.
 *
 * Respect JSR-296
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class JTimer extends SingleFrameApplication implements
        TreeSelectionListener, MouseListener {

    /** log. */
    private static Log log = LogFactory.getLog(JTimer.class);

    /** Timer core controller. */
    protected TimerCore core;

    /** Tree structure. */
    protected ProjectsAndTasksTable projectsAndTasksTable;

    /** Systray manager. */
    protected SystrayManager systrayManager;

    /** I18n resources map. */
    protected ResourceMap resourceMap;

    /** Jtimer application config. */
    public static JTimerConfig config;

    /** Single project selection property. */
    protected boolean selectedSingleProject;

    /** Single task selection property. */
    protected boolean selectedSingleTask;

    /** Single task or project selection. */
    protected boolean selectedSingleElement;

    /** Single running task selection. */
    protected boolean selectedSingleRunningTask;

    /** Single non running task selection. */
    protected boolean selectedSingleStoppedTask;

    /** Multiples tasks selection (at least 1). */
    protected boolean selectedTasks;

    /** Multiples projects selection (at least 1). */
    protected boolean selectedProjects;

    /** Multiples projects selection (at least 2). */
    protected boolean selectedMultiplesProjects;

    /** Multiples tasks selection (at least 2). */
    protected boolean selectedMultiplesTasks;

    /** Multiples elements selection (at least 2). */
    protected boolean selectedMultiplesElements;

    /**
     * Main. launch UI
     *
     * @param args args
     */
    public static void main(String[] args) {

        if (log.isInfoEnabled()) {
            log.info("Starting " + JTimer.class.getSimpleName() + " at " + new Date());
        }

        // load configuration and run actions
        loadConfiguration(args);
        config.doAction(JTimerConfig.STEP_BEFORE_UI);

        launch(JTimer.class, args);
    }

    /**
     * Initialize application. Called before UI build.
     *
     * @param args args
     * @see Application#initialize(String[])
     */
    @Override
    protected void initialize(String[] args) {

        // super, but does nothing
        super.initialize(args);

        // init resources map
        ApplicationContext ctxt = getContext();
        resourceMap = ctxt.getResourceMap();

        // native init
        SystemInfoFactory.getSystemInfo();

        // fix start in iconified mode
        ctxt.getSessionStorage().putProperty(JFrame.class, new WindowProperty2());

        // init timercore
        core = new TimerCore();

        // Systray mgr
        systrayManager = new SystrayManager(this);
        core.getData().addDataEventListener(systrayManager);

        IdleDialog.init(this, core);
    }

    /**
     * Load configuration.
     *
     * @param args args to parse command line options
     */
    protected static void loadConfiguration(String[] args) {

        config = new JTimerConfig();

        migrateConfiguration();

        // parse after file migration
        config.parse(args);
    }

    /**
     * Migrate configuration between version 1.3 and 1.4.
     */
    protected static void migrateConfiguration() {
        // add file migration for configuration file created before version 1.4
        File homeDir = new File(System.getProperty("user.home"), ".jtimer");
        File oldFile = new File(homeDir, "JTimer.properties");
        File newFile = new File(config.appConfig.getUserConfigDirectory(), config.appConfig.getConfigFileName());
        if (oldFile.isFile() && !newFile.isFile()) {
            if (log.isInfoEnabled()) {
                log.info("Migration configuration file location");
            }
            try {
                FileUtils.copyFile(oldFile, newFile);
            } catch (IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't copy config file to new location", ex);
                }
            }
        }
    }

    /**
     * startup.
     *
     * Create frame menu bar. Create main component.
     *
     * @see Application#startup()
     */
    @Override
    protected void startup() {

        // set Menu Bar
        getMainFrame().setJMenuBar(createMenuBar());

        // show main panel components
        show(createMainComponent());

    }

    /**
     * Create main component.
     *
     * Toolbar on top. Tree middle. Status bar on bottom.
     *
     * @return The component
     */
    protected JComponent createMainComponent() {

        // panel = main component
        JPanel panel = new JPanel(new BorderLayout());

        // toolbar on top (north)
        panel.add(createToolBar(), BorderLayout.NORTH);

        // tree middle (center-top)
        ProjectsAndTasksTable projectTreeTable = createTreeTable();
        JScrollPane scrollPaneProjectTreeTable = new JScrollPane(projectTreeTable);
        panel.add(scrollPaneProjectTreeTable, BorderLayout.CENTER);

        // status bar bottom
        StatusBar sb = new StatusBar(this, core.getData());
        // status bar ui will be notified from events
        core.getData().addDataEventListener(sb);
        panel.add(sb, BorderLayout.SOUTH);

        // taille par defaut au premier lancement de l'application
        // sera ecrasee par la restauration de la session
        panel.setPreferredSize(new Dimension(640, 480));

        return panel;
    }

    /**
     * Create complex tree table.
     *
     * @return ProjectsAndTaskTable instance
     */
    protected ProjectsAndTasksTable createTreeTable() {

        projectsAndTasksTable = new ProjectsAndTasksTable(this, core);

        // name used in properties files
        projectsAndTasksTable.setName("projectslist");
        projectsAndTasksTable.addTreeSelectionListener(this);
        projectsAndTasksTable.addMouseListener(this);
        projectsAndTasksTable.setShowClosed(config.isShowClosed());

        // since merge option, selection can be multiple
        projectsAndTasksTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        // remove F2 KeyStroke from table
        KeyStroke keyToRemove = KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0);
        InputMap imap = projectsAndTasksTable.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        while (imap != null) {
            imap.remove(keyToRemove);
            imap = imap.getParent();
        }

        // default lines are non visible
        projectsAndTasksTable.putClientProperty("JTree.lineStyle", "Angled");

        return projectsAndTasksTable;
    }

    /**
     * Create toolbar.
     *
     * @return tool bar builded
     */
    protected JComponent createToolBar() {
        String[] toolbarActionNames = {"newProject", "newTask", "---",
                "startTask", "stopTask", "---",
                "addAnnotation", "editAlert"};
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        for (String actionName : toolbarActionNames) {

            if (actionName.equals("---")) {
                toolBar.add(new JToolBar.Separator());
            } else {
                JButton button = new JButton();
                button.setAction(getAction(actionName));

                // hide text
                // button.setVerticalTextPosition(JButton.BOTTOM);
                // button.setHorizontalTextPosition(JButton.CENTER);
                button.setHideActionText(true);

                button.setFocusable(false);
                toolBar.add(button);
            }
        }
        return toolBar;
    }

    /**
     * Create application menu bar.
     *
     * @return menu bar
     */
    protected JMenuBar createMenuBar() {

        JMenuBar menuBar = new JMenuBar();

        String[] projectMenuActionNames = {"newProject", "editProject",
                "closeProject", "deleteProject", "---", "quit"};
        menuBar.add(createMenu("projectMenu", projectMenuActionNames));

        String[] taskMenuActionNames = {"newTask", "editTask", "closeTask",
                "deleteTask", "---", "startTask", "stopTask", "---",
                "addAnnotation", "editAlert", "increment1Task",
                "increment5Task", "increment30Task", "decrement1Task",
                "decrement5Task", "decrement30Task", "setToZero", "mergeTasks"};
        menuBar.add(createMenu("taskMenu", taskMenuActionNames));

        String[] reportMenuActionNames = {"makeReport"};
        menuBar.add(createMenu("reportMenu", reportMenuActionNames));

        JMenu optionmMenu = createOptionMenu();
        menuBar.add(optionmMenu);

        String[] helpMenuActionNames = {"about"};
        menuBar.add(createMenu("helpMenu", helpMenuActionNames));

        return menuBar;
    }

    /**
     * Create option dynamic menu.
     *
     * @return option menu
     */
    protected JMenu createOptionMenu() {
        JMenu menu = new JMenu();
        menu.setName("optionMenu");

        // show closed
        JMenuItem showClosedItem = new JCheckBoxMenuItem();
        showClosedItem.setAction(getAction("isShowClosed"));
        showClosedItem.setSelected(config.isShowClosed());
        showClosedItem.setIcon(null);
        menu.add(showClosedItem);

        // close to systray
        JMenuItem closeToSysItem = new JCheckBoxMenuItem();
        closeToSysItem.setAction(getAction("isCloseToSystray"));
        closeToSysItem.setSelected(config.isCloseToSystray());
        closeToSysItem.setIcon(null);
        menu.add(closeToSysItem);

        // close to systray
        JMenuItem showTaskNameItem = new JCheckBoxMenuItem();
        showTaskNameItem.setAction(getAction("isShowTaskNameInSystray"));
        showTaskNameItem.setSelected(config.isShowTaskNameInSystray());
        showTaskNameItem.setIcon(null);
        menu.add(showTaskNameItem);

        // report first day of week
        JMenu reportFDoW = new JMenu();
        reportFDoW.setName("optionReportFirstDayMenu");
        Calendar calendar = Calendar.getInstance();
        ButtonGroup bg = new ButtonGroup();
        // affiche la liste des jours dans l'ordre de la locale utilisateur
        for (int day = calendar.getFirstDayOfWeek(); day < calendar.getFirstDayOfWeek() + 7; day++) {
            int realDay = (day - 1) % 7 + 1;
            JRadioButtonMenuItem fdowItem = new JRadioButtonMenuItem();
            fdowItem.setAction(getAction("isReportFirstDayOfWeek" + realDay));
            fdowItem.setSelected(realDay == JTimer.config.getReportFirstDayOfWeek());
            fdowItem.setIcon(null);
            reportFDoW.add(fdowItem);
            bg.add(fdowItem);
        }
        menu.add(reportFDoW);

        return menu;
    }

    /**
     * Create single menu.
     *
     * @param menuName menu name
     * @param actionNames associated actions
     * @return menu
     */
    protected JMenu createMenu(String menuName, String[] actionNames) {
        JMenu menu = new JMenu();
        menu.setName(menuName);
        addActionToMenu(menu, actionNames);
        return menu;
    }

    /**
     * Add saf action to an existing menu.
     *
     * Menu have to be a JMenu or JPopupMenu.
     *
     * @param menu parent menu
     * @param actionNames action names
     */
    protected void addActionToMenu(JComponent menu, String[] actionNames) {
        for (String actionName : actionNames) {
            if (actionName.equals("---")) {
                menu.add(new JSeparator());
            } else if (actionName.startsWith("is")) {
                // if action name start by is
                // display it as CheckBox
                JMenuItem menuItem = new JCheckBoxMenuItem();
                // link to an @Action
                menuItem.setAction(getAction(actionName));
                menuItem.setIcon(null);
                menu.add(menuItem);
            } else {
                JMenuItem menuItem = new JMenuItem();
                // link to an @Action
                menuItem.setAction(getAction(actionName));
                menuItem.setIcon(null);
                menu.add(menuItem);
            }
        }
    }

    /**
     * Ready. Called when UI is ready and displayed.
     *
     * @see Application#ready()
     */
    @Override
    protected void ready() {

        // init core, load list, synchronization, etc...
        boolean init = core.init();

        if (init) {
            // schedule tree refresh at midnight
            scheduleTreeRefresh();

            // install icon (do it at last action)
            systrayManager.install();

            // run action after ui
            config.putObject(this);
            config.doAction(JTimerConfig.STEP_AFTER_UI);
        } else {
            String failTitle = resourceMap.getString("startFail.title");
            String failMessage = resourceMap.getString("startFail.message");
            JOptionPane.showMessageDialog(getMainFrame(), failMessage,
                    failTitle, JOptionPane.ERROR_MESSAGE);
            exit();
        }
    }

    /**
     * Called on application shutdown.
     *
     * Save context.
     *
     * @see SingleFrameApplication#shutdown()
     */
    @Override
    protected void shutdown() {
        log.debug("Shutdown called");
        core.exit();

        // save context
        // super, sauve le context des fenetres, etc...
        super.shutdown();
    }

    /**
     * Refresh tree at midnight.
     */
    protected void scheduleTreeRefresh() {

        // task used to refresh tree
        java.util.TimerTask refreshTreeTask = new RefreshTreeTask(core);

        Timer timer = new Timer();

        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(System.currentTimeMillis());
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        date.add(Calendar.DAY_OF_YEAR, 1); // run only next day

        // Schedule to run every day in midnight
        // task,firstTime,period
        timer.schedule(refreshTreeTask, date.getTime(), // at date
                1000 * 60 * 60 * 24 // every day
        );
    }

    /**
     * Display a popup error message.
     *
     * @param errorMessageKey saf error message key
     */
    public void displayErrorMessage(String errorMessageKey) {
        String title = resourceMap.getString("action.invalidActionTitle");
        String message = resourceMap.getString(errorMessageKey);

        // check untranslated string
        if (StringUtils.isEmpty(message)) {
            message = resourceMap.getString("action.missingErrorMessage",
                    errorMessageKey);
        }

        JOptionPane.showMessageDialog(getMainFrame(), message, title,
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * New project action.
     *
     * Ask user for project name
     */
    @Action
    public void newProject() {

        String projectName = JOptionPane.showInputDialog(getMainFrame(),
                resourceMap.getString("input.newProjectMessage"), resourceMap
                        .getString("input.newProjectTitle"),
                JOptionPane.QUESTION_MESSAGE);

        if (projectName != null) {

            // remove unneeded spaces
            projectName = projectName.trim();

            TimerProject p = new TimerProject(projectName);

            // add creation date
            p.setCreationDate(new Date());

            try {
                core.getData().addProject(p);
            } catch (DataViolationException e) {
                displayErrorMessage(e.getExceptionKey());
            }
        }
    }

    /**
     * Edit project
     *
     * Enabled when a project is selected
     */
    @Action(enabledProperty = "selectedSingleProject")
    public void editProject() {
        TimerProject project = projectsAndTasksTable.getSelectedProjects().get(
                0);

        String newProjectName = (String) JOptionPane.showInputDialog(getMainFrame(), resourceMap
                        .getString("input.editProjectMessage"), resourceMap
                        .getString("input.editProjectTitle"),
                JOptionPane.INFORMATION_MESSAGE, null, null, project.getName());

        if (newProjectName != null) {

            // remove unneeded spaces
            newProjectName = newProjectName.trim();

            try {
                core.getData().editProject(project, newProjectName);
            } catch (DataViolationException e) {
                displayErrorMessage(e.getExceptionKey());
            }
        }
    }

    /**
     * Create new task action.
     *
     * Enabled when a project or a task is selected
     */
    @Action(enabledProperty = "selectedSingleElement")
    public void newTask() {

        // select task to add new task
        TimerTask selectedTask = projectsAndTasksTable.getSelectedElements().get(0);

        NewTaskView newTaskPanel = new NewTaskView(this.getMainFrame(), this, core, selectedTask);
        show(newTaskPanel);

        TimerTask newTask = newTaskPanel.getTask();
        if (newTask != null) {
            selectTask(newTask);
        }
    }

    /**
     * Auto select task after creation.
     *
     * @param task task to select
     */
    public void selectTask(TimerTask task) {
        projectsAndTasksTable.setSelectedTask(task);
    }

    /**
     * Edit task.
     *
     * Enabled when a task is selected
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void editTask() {

        TimerTask task = projectsAndTasksTable.getSelectedTasks().get(0);

        TimerTaskEditor editor = new TimerTaskEditor(this, task, core);
        show(editor);
    }

    /**
     * Start selected task in tree.
     *
     * If it not already been running
     *
     * @return TimerTask scheduled for start
     * @see Task
     */
    @Action(enabledProperty = "selectedSingleStoppedTask")
    public Task<?, ?> startTask() {

        // search for selected task in tree
        // can't be null
        TimerTask task = projectsAndTasksTable.getSelectedTasks().get(0);

        RunTaskJob jobToRun = new RunTaskJob(this, task, core.getData());
        core.getData().startTask(task);

        return jobToRun;
    }

    /**
     * Start task pointed by taskPath.
     *
     * @param taskPath task path to start (from root to task)
     */
    public void startTask(String taskPath) {
        TimerTask task = core.getData().getTaskForPath(taskPath);
        if (task != null) {
            RunTaskJob jobToRun = new RunTaskJob(this, task, core.getData());
            getContext().getTaskService().execute(jobToRun);
            core.getData().startTask(task);
        } else {
            if (log.isWarnEnabled()) {
                log.warn("Can't find task '" + taskPath + "'");
            }
        }
    }

    /**
     * Called by task job manager when task as been started.
     *
     * @param task started task
     */
    public void startedTask(TimerTask task) {
        setSelectedSingleRunningTask(true);
        setSelectedSingleStoppedTask(false);
    }

    /**
     * Return job that manage running task.
     *
     * @param task timer task to get job
     * @return job or null
     */
    protected RunTaskJob getJobForRunningTask(TimerTask task) {
        RunTaskJob job = null;

        TaskMonitor tm = getContext().getTaskMonitor();
        for (Task<?, ?> t : tm.getTasks()) {
            TimerTask localtask = ((RunTaskJob) t).getTask();
            if (task.equals(localtask)) {
                job = (RunTaskJob) t;
            }
        }

        return job;
    }

    /**
     * Stop selected task in tree.
     * 
     * Verify if it has been started
     */
    @Action(enabledProperty = "selectedSingleRunningTask")
    public void stopTask() {

        // task can't be null
        TimerTask task = projectsAndTasksTable.getSelectedTasks().get(0);
        stopTask(task);

    }

    /**
     * Stop a task.
     *
     * Delegate method. Called by idle detect ?
     * Called by delete method too.
     *
     * @param task task to stop
     */
    public void stopTask(TimerTask task) {

        RunTaskJob rtt = getJobForRunningTask(task);
        // test if task is already running
        if (rtt != null) {
            rtt.wantToStop();
            core.getData().stopTask(task);

            // re-enable/disable buttons
            setSelectedSingleRunningTask(false);
            setSelectedSingleStoppedTask(true);
        }
    }

    /**
     * Stop all running tasks.
     */
    public void stopAllTasks() {

        TaskMonitor tm = getContext().getTaskMonitor();
        for (Task<?, ?> t : tm.getTasks()) {
            // task
            TimerTask ttask = ((RunTaskJob) t).getTask();
            stopTask(ttask);
        }
    }

    /**
     * Close project.
     */
    @Action(enabledProperty = "selectedSingleProject")
    public void closeProject() {
        TimerProject project = projectsAndTasksTable.getSelectedProjects().get(0);

        core.getData().changeProjectCloseState(project);
    }

    /**
     * Close task.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void closeTask() {
        TimerTask task = projectsAndTasksTable.getSelectedTasks().get(0);

        core.getData().changeTaskCloseState(task);
    }

    /**
     * Delete project.
     */
    @Action(enabledProperty = "selectedProjects")
    public void deleteProject() {

        List<TimerProject> projects = projectsAndTasksTable.getSelectedProjects();

        int confirm;
        if (projects.size() == 1) {
            TimerProject firstOne = projects.get(0);
            confirm = JOptionPane.showConfirmDialog(getMainFrame(),
                    resourceMap.getString("input.deleteProjectMessage", firstOne.getName()),
                    resourceMap.getString("input.deleteProjectTitle"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        } else {
            confirm = JOptionPane.showConfirmDialog(getMainFrame(),
                    resourceMap.getString("input.deleteProjectsMessage", projects.size()),
                    resourceMap.getString("input.deleteProjectTitle"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        }

        if (confirm == JOptionPane.YES_OPTION) { // approved
            for (TimerProject project : projects) {
                try {
                    core.getData().deleteProject(project);
                } catch (DataViolationException e) {
                    displayErrorMessage(e.getExceptionKey());
                }
            }
        }

    }

    /**
     * Delete task.
     */
    @Action(enabledProperty = "selectedTasks")
    public void deleteTask() {

        List<TimerTask> tasks = projectsAndTasksTable.getSelectedTasks();

        int confirm;
        if (tasks.size() == 1) {
            TimerTask firstOne = tasks.get(0);
            confirm = JOptionPane.showConfirmDialog(getMainFrame(),
                    resourceMap.getString("input.deleteTaskMessage", firstOne.getName()),
                    resourceMap.getString("input.deleteTaskTitle"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        } else {
            confirm = JOptionPane.showConfirmDialog(getMainFrame(),
                    resourceMap.getString("input.deleteTasksMessage", tasks.size()),
                    resourceMap.getString("input.deleteTaskTitle"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        }

        if (confirm == JOptionPane.YES_OPTION) { // approved
            for (TimerTask task : tasks) {
                try {
                    stopTask(task);
                    core.getData().deleteTask(task);
                } catch (DataViolationException e) {
                    displayErrorMessage(e.getExceptionKey());
                }
            }
        }
    }

    /**
     * Change show closed option.
     *
     * @param event action event
     */
    @Action
    public void isShowClosed(ActionEvent event) {
        JCheckBoxMenuItem source = (JCheckBoxMenuItem) event.getSource();
        boolean showClosed = source.isSelected();
        projectsAndTasksTable.setShowClosed(showClosed);
        config.setShowClosed(showClosed);
    }

    /**
     * Change close to systray option.
     *
     * @param event action event
     */
    @Action
    public void isCloseToSystray(ActionEvent event) {
        JCheckBoxMenuItem source = (JCheckBoxMenuItem) event.getSource();
        boolean closeToSystray = source.isSelected();
        config.setCloseToSystray(closeToSystray);
    }

    /**
     * Change show task name option.
     *
     * @param event action event
     */
    @Action
    public void isShowTaskNameInSystray(ActionEvent event) {
        JCheckBoxMenuItem source = (JCheckBoxMenuItem) event.getSource();
        boolean showTaskNameInSystray = source.isSelected();
        config.setShowTaskNameInSystray(showTaskNameInSystray);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek1() {
        config.setReportFirstDayOfWeek(1);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek2() {
        config.setReportFirstDayOfWeek(2);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek3() {
        config.setReportFirstDayOfWeek(3);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek4() {
        config.setReportFirstDayOfWeek(4);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek5() {
        config.setReportFirstDayOfWeek(5);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek6() {
        config.setReportFirstDayOfWeek(6);
    }

    /**
     * Change report first day of week.
     */
    @Action
    public void isReportFirstDayOfWeek7() {
        config.setReportFirstDayOfWeek(7);
    }

    /**
     * Increment task time.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void increment5Task() {
        incrementTaskTime(5 * 60000);
    }

    /**
     * Increment task time.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void increment30Task() {
        incrementTaskTime(30 * 60000);
    }

    /**
     * Decrement task time.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void decrement1Task() {
        incrementTaskTime(-60000);
    }

    /**
     * Decrement task time.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void decrement5Task() {
        incrementTaskTime(-5 * 60000);
    }

    /**
     * Decrement task time.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void decrement30Task() {
        incrementTaskTime(-30 * 60000);
    }

    /**
     * Increment task time.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void increment1Task() {
        incrementTaskTime(60000);
    }

    /**
     * Increment task time.
     *
     * To decrement, set negative increment:)
     *
     * @param increment increment in ms
     */
    protected void incrementTaskTime(long increment) {
        TimerTask selectedTask = projectsAndTasksTable.getSelectedTasks()
                .get(0);

        // task is not running
        Date now = new Date();
        long todayTime = selectedTask.getTime(now);

        long newTodayTime = todayTime + increment;

        try {
            // check if + negative increment still positive
            if (newTodayTime > 0) {
                core.getData().changeTaskTime(selectedTask, now,
                        newTodayTime);
            } else {
                // force to 0
                core.getData().changeTaskTime(selectedTask, now, 0L);
            }
        } catch (DataViolationException e) {
            displayErrorMessage(e.getExceptionKey());
        }
    }

    /**
     * Reset task time to zero.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void setToZero() {
        TimerTask task = projectsAndTasksTable.getSelectedTasks().get(0);
        incrementTaskTime(-task.getTime(new Date()));
    }

    /**
     * Merge multiples tasks action.
     */
    @Action(enabledProperty = "selectedMultiplesTasks")
    public void mergeTasks() {
        List<TimerTask> tasks = projectsAndTasksTable.getSelectedTasks();

        TimerTask destinationTask = tasks.get(0);
        List<TimerTask> otherTasks = tasks.subList(1, tasks.size());
        int confirm = JOptionPane.showConfirmDialog(getMainFrame(),
                resourceMap.getString("input.mergeTaskMessage", tasks.size(), destinationTask.getName()),
                resourceMap.getString("input.mergeTaskTitle"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (confirm == JOptionPane.YES_OPTION) {
            try {
                core.getData().mergeTasks(destinationTask, otherTasks);
            } catch (DataViolationException e) {
                displayErrorMessage(e.getExceptionKey());
            }
        }
    }

    /**
     * Add annotation action.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void addAnnotation() {
        // select task to add new annotation
        TimerTask selectedTask = projectsAndTasksTable.getSelectedTasks()
                .get(0);

        String annotation = JOptionPane.showInputDialog(getMainFrame(),
                resourceMap.getString("input.addAnnotationMessage",
                        selectedTask.getName()), resourceMap
                        .getString("input.addAnnotationTitle"),
                JOptionPane.QUESTION_MESSAGE);

        if (annotation != null) {
            // remove useless spaces
            annotation = annotation.trim();

            try {
                core.getData().addAnnotation(selectedTask, new Date(),
                        annotation);
            } catch (DataViolationException e) {
                displayErrorMessage(e.getExceptionKey());
            }
        }
    }

    /**
     * Edit alert action.
     */
    @Action(enabledProperty = "selectedSingleTask")
    public void editAlert() {
        // select task to edit alert
        TimerTask selectedTask = projectsAndTasksTable.getSelectedTasks()
                .get(0);

        AlertEditor alertEditor = new AlertEditor(this, core.getData(),
                selectedTask);
        show(alertEditor);
    }

    /**
     * Create report.
     */
    @Action
    public void makeReport() {
        ReportView view = new ReportView(this, core);
        show(view);
    }

    /**
     * Show about frame.
     */
    @Action
    public void about() {
        HelpFrame aboutFrame = new HelpFrame(this);
        show(aboutFrame);
    }

    /**
     * Is selected project.
     *
     * @return selected project property
     */
    public boolean isSelectedSingleProject() {
        return selectedSingleProject;
    }

    /**
     * Change selected project property.
     *
     * @param selectedSingleProject selected project property
     */
    public void setSelectedSingleProject(boolean selectedSingleProject) {
        boolean oldValue = this.selectedSingleProject;
        this.selectedSingleProject = selectedSingleProject;
        firePropertyChange("selectedSingleProject", oldValue,
                selectedSingleProject);
    }

    /**
     * Is selected task.
     *
     * @return selected task property
     */
    public boolean isSelectedSingleTask() {
        return selectedSingleTask;
    }

    /**
     * Change selected task property.
     *
     * @param selectedSingleTask selected task property
     */
    public void setSelectedSingleTask(boolean selectedSingleTask) {
        boolean oldValue = this.selectedSingleTask;
        this.selectedSingleTask = selectedSingleTask;
        firePropertyChange("selectedSingleTask", oldValue, selectedSingleTask);
    }

    /**
     * Is selected task or project.
     *
     * @return selected task or project property
     */
    public boolean isSelectedSingleElement() {
        return selectedSingleElement;
    }

    /**
     * Change selected task or project property.
     *
     * @param selectedSingleElement selected task or project property
     */
    public void setSelectedSingleElement(boolean selectedSingleElement) {
        boolean oldValue = this.selectedSingleElement;
        this.selectedSingleElement = selectedSingleElement;
        firePropertyChange("selectedSingleElement", oldValue,
                selectedSingleElement);
    }

    /**
     * Is selected running task.
     * 
     * @return the selectedSingleRunningTask
     */
    public boolean isSelectedSingleRunningTask() {
        return selectedSingleRunningTask;
    }

    /**
     * Change selected running task property.
     * 
     * @param selectedSingleRunningTask selected running task property
     */
    public void setSelectedSingleRunningTask(boolean selectedSingleRunningTask) {
        boolean oldValue = this.selectedSingleRunningTask;
        this.selectedSingleRunningTask = selectedSingleRunningTask;
        firePropertyChange("selectedSingleRunningTask", oldValue,
                selectedSingleRunningTask);
    }

    /**
     * Is selected non running task.
     *
     * @return selected non running task
     */
    public boolean isSelectedSingleStoppedTask() {
        return selectedSingleStoppedTask;
    }

    /**
     * Change selected non running task property.
     *
     * @param selectedSingleStoppedTask selected non running task
     */
    public void setSelectedSingleStoppedTask(boolean selectedSingleStoppedTask) {
        boolean oldValue = this.selectedSingleStoppedTask;
        this.selectedSingleStoppedTask = selectedSingleStoppedTask;
        firePropertyChange("selectedSingleStoppedTask", oldValue,
                selectedSingleStoppedTask);
    }

    /**
     * Is selected multiples projects.
     *
     * @return selected multiples projects
     */
    public boolean isSelectedMultiplesProjects() {
        return selectedMultiplesProjects;
    }

    /**
     * Change selected multiples projects property.
     *
     * @param selectedMultiplesProjects selected multiples projects
     */
    public void setSelectedMultiplesProjects(boolean selectedMultiplesProjects) {
        boolean oldValue = this.selectedMultiplesProjects;
        this.selectedMultiplesProjects = selectedMultiplesProjects;
        firePropertyChange("selectedMultiplesProjects", oldValue,
                selectedMultiplesProjects);
    }

    /**
     * Is selected multiples tasks.
     *
     * @return selected multiples tasks
     */
    public boolean isSelectedMultiplesTasks() {
        return selectedMultiplesTasks;
    }

    /**
     * Change selected multiples tasks property.
     *
     * @param selectedMultiplesTasks selected multiples tasks
     */
    public void setSelectedMultiplesTasks(boolean selectedMultiplesTasks) {
        boolean oldValue = this.selectedMultiplesTasks;
        this.selectedMultiplesTasks = selectedMultiplesTasks;
        firePropertyChange("selectedMultiplesTasks", oldValue,
                selectedMultiplesTasks);
    }

    /**
     * Is selected multiples elements.
     *
     * @return selected multiples elements
     */
    public boolean isSelectedMultiplesElements() {
        return selectedMultiplesElements;
    }

    /**
     * Change selected multiples elements property.
     *
     * @param selectedMultiplesElements selected multiples elements
     */
    public void setSelectedMultiplesElements(boolean selectedMultiplesElements) {
        boolean oldValue = this.selectedMultiplesElements;
        this.selectedMultiplesElements = selectedMultiplesElements;
        firePropertyChange("selectedMultiplesElements", oldValue,
                selectedMultiplesElements);
    }

    /**
     * Change selected tasks property.
     *
     * @param selectedTasks selected tasks property
     */
    public void setSelectedTasks(boolean selectedTasks) {
        boolean oldValue = this.selectedTasks;
        this.selectedTasks = selectedTasks;
        firePropertyChange("selectedTasks", oldValue, selectedTasks);
    }

    /**
     * Is selected task property.
     *
     * @return selected tasks property
     */
    public boolean isSelectedTasks() {
        return selectedTasks;
    }

    /**
     * Change selected projects property.
     *
     * @param selectedProjects selected projects property
     */
    public void setSelectedProjects(boolean selectedProjects) {
        boolean oldValue = this.selectedProjects;
        this.selectedProjects = selectedProjects;
        firePropertyChange("selectedProjects", oldValue, selectedProjects);
    }

    /**
     * Is selected task property.
     *
     * @return selected tasks property
     */
    public boolean isSelectedProjects() {
        return selectedProjects;
    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        // get selected elements
        List<TimerTask> elements = projectsAndTasksTable.getSelectedElements();
        List<TimerProject> projects = projectsAndTasksTable
                .getSelectedProjects();
        List<TimerTask> tasks = projectsAndTasksTable.getSelectedTasks();

        // notify application that tree selection has changed
        if (tasks.size() == 1 && projects.size() == 0) {
            TimerTask task = tasks.get(0);
            setSelectedSingleTask(true);
            setSelectedTasks(true);

            // ask for SAF job manager if task is running
            RunTaskJob job = getJobForRunningTask(task);
            if (job == null || job.isStopping()) {
                setSelectedSingleStoppedTask(true);
                setSelectedSingleRunningTask(false);
            } else {
                setSelectedSingleStoppedTask(false);
                setSelectedSingleRunningTask(true);
            }
            setSelectedMultiplesTasks(false);
            setSelectedSingleProject(false);
            setSelectedProjects(false);
            setSelectedMultiplesProjects(false);
        } else {
            setSelectedSingleTask(false);
            setSelectedSingleStoppedTask(false);
            setSelectedSingleRunningTask(false);

            if (tasks.size() > 1) {
                setSelectedMultiplesTasks(projects.size() == 0);
                setSelectedTasks(projects.size() == 0);
            } else {
                setSelectedSingleProject(tasks.size() == 0
                        && projects.size() == 1);
                setSelectedMultiplesProjects(tasks.size() == 0
                        && projects.size() > 1);
                setSelectedProjects(tasks.size() == 0
                        && projects.size() >= 1);
            }
        }
        setSelectedSingleElement(elements.size() == 1);
        setSelectedMultiplesElements(elements.size() > 1);
    }

    /**
     * Get action for named component.
     *
     * Util method.
     *
     * @param actionName action name
     * @return swing action
     */
    protected javax.swing.Action getAction(String actionName) {
        return getContext().getActionMap().get(actionName);
    }

    /**
     * Show window
     */
    @Action
    public void show() {
        // TODO better code ? that use SAF methods ?
        getMainFrame().setVisible(true);
        getMainFrame().toFront();
    }

    /**
     * Show window
     */
    @Action
    public void hide() {
        // TODO better code ? that use SAF methods ?
        getMainFrame().setVisible(false);
    }

    /**
     * Notified by job on pre idle detect.
     */
    public void preIdleDetect() {
        systrayManager.preIdleDetect();
    }

    /**
     * Notified by job on post idle detect.
     */
    public void postIdleDetect() {
        systrayManager.postIdleDetect();
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if (log.isDebugEnabled()) {
            log.debug("Mouse clicked (" + e.getClickCount()
                    + " clics), source = "
                    + e.getSource().getClass().getName());
        }

        switch (e.getButton()) {

            // clic gauche
            case MouseEvent.BUTTON1:
                // clic sur l'arbre des projets
                if (e.getSource() == projectsAndTasksTable) {
                    // demarre la tache lors d'un double clic dessus
                    if (e.getClickCount() == 2) {
                        startOrStopCurrentSelectedTask();
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("Single clic on tree, do nothing");
                        }
                    }
                }
                break;

            // gestion du clic droit
            case MouseEvent.BUTTON3:

                // force task selection on rigth clic
                TreePath path = projectsAndTasksTable.getPathForLocation(e.getX(),
                        e.getY());
                int selectedRow = projectsAndTasksTable.getRowForPath(path);
                projectsAndTasksTable.getSelectionModel().setSelectionInterval(
                        selectedRow, selectedRow);

                // fix item selection
                JPopupMenu menu = new JPopupMenu();

                String[] actionNames = null;

                // construct menu in function of selection
                if (isSelectedSingleProject()) {
                    actionNames = new String[]{"newTask", "---", "newProject",
                            "editProject", "closeProject", "deleteProject"};
                }

                if (isSelectedSingleTask()) {
                    actionNames = new String[]{"startTask", "stopTask", "---",
                            "newTask", "editTask", "closeTask", "deleteTask",
                            "---", "addAnnotation", "editAlert", "increment1Task",
                            "increment5Task", "increment30Task", "decrement1Task",
                            "decrement5Task", "decrement30Task", "setToZero"};
                }

                if (isSelectedMultiplesTasks()) {
                    actionNames = new String[]{"mergeTasks"};
                }

                // case, right clic, but nothing selected
                if (actionNames != null) {
                    addActionToMenu(menu, actionNames);

                    menu.show(e.getComponent(), e.getX(), e.getY());
                }
        }
    }

    public void startOrStopCurrentSelectedTask() {
        if (isSelectedSingleStoppedTask()) { // can only launch non running tasks
            Task<?, ?> appTask = startTask();
            if (appTask != null) {

                // first, on dlb click stop all running tasks
                stopAllTasks();

                getContext().getTaskService().execute(appTask);
            }
        } else if (isSelectedSingleTask()) {
            TimerTask task = projectsAndTasksTable.getSelectedTasks().get(0);
            stopTask(task);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Non selected non running task to launch");
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
