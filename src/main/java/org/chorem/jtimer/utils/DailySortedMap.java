/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.utils;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * SortedMap that return a unique value for all date for a day.
 *
 * @see java.util.SortedMap
 * @param <T> values type
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 *
 * @deprecated since 1.5, this class can now be replaced by TreeMap&ltLocalDate, Long&gt;
 */
@Deprecated
public class DailySortedMap<T> extends TreeMap<Date, T> {

    /** serialVersionUID */
    private static final long serialVersionUID = 5736472379626976185L;

    /**
     * Constructs a new, empty tree map, using the natural ordering
     * of its keys.
     */
    public DailySortedMap() {
    }

    /**
     * Constructs a new, empty tree map, ordered according to
     * the given comparator.
     *
     * @param comparator comparator
     */
    public DailySortedMap(Comparator<? super Date> comparator) {
        super(comparator);
    }

    /**
     * Constructs a new tree map containing the same mappings
     * as the given map, ordered according to the natural ordering of its keys.
     *
     * @param m init map
     */
    public DailySortedMap(Map<? extends Date, ? extends T> m) {
        super(m);
    }

    /**
     * Constructs a new tree map containing the same mappings and
     * using the same ordering as the specified sorted map.
     *
     * @param m init map
     */
    public DailySortedMap(SortedMap<Date, ? extends T> m) {
        super(m);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map.Entry<Date, T> ceilingEntry(Date key) {
        Date dayKey = getDayDate(key);
        Map.Entry<Date, T> result = super.ceilingEntry(dayKey);
        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date ceilingKey(Date key) {
        Date dayKey = getDayDate(key);
        Date result = super.ceilingKey(dayKey);
        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsKey(Object key) {
        boolean result = false;

        if (key instanceof Date) {
            Date lDate = getDayDate((Date) key);
            result = super.containsKey(lDate);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map.Entry<Date, T> floorEntry(Date key) {
        Date dayKey = getDayDate(key);
        Map.Entry<Date, T> result = super.floorEntry(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date floorKey(Date key) {
        Date dayKey = getDayDate(key);
        Date result = super.floorKey(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(Object key) {
        T result = null;

        if (key instanceof Date) {
            Date lDate = getDayDate((Date) key);
            result = super.get(lDate);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NavigableMap<Date, T> headMap(Date toKey, boolean inclusive) {
        Date dayKey = getDayDate(toKey);
        NavigableMap<Date, T> result = super.headMap(dayKey, inclusive);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedMap<Date, T> headMap(Date toKey) {
        Date dayKey = getDayDate(toKey);
        SortedMap<Date, T> result = super.headMap(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map.Entry<Date, T> higherEntry(Date key) {
        Date dayKey = getDayDate(key);
        Map.Entry<Date, T> result = super.higherEntry(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date higherKey(Date key) {
        Date dayKey = getDayDate(key);
        Date result = super.higherKey(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map.Entry<Date, T> lowerEntry(Date key) {
        Date dayKey = getDayDate(key);
        Map.Entry<Date, T> result = super.lowerEntry(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date lowerKey(Date key) {
        Date dayKey = getDayDate(key);
        Date result = super.lowerKey(dayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T put(Date key, T value) {
        Date lDate = getDayDate(key);
        T result = super.put(lDate, value);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putAll(Map<? extends Date, ? extends T> map) {
        for (Map.Entry<? extends Date, ? extends T> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NavigableMap<Date, T> subMap(Date fromKey, boolean fromInclusive, Date toKey, boolean toInclusive) {
        Date fromDayKey = getDayDate(fromKey);
        Date toDayKey = getDayDate(toKey);
        NavigableMap<Date, T> result = super.subMap(fromDayKey, fromInclusive,
                toDayKey, toInclusive);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedMap<Date, T> subMap(Date fromKey, Date toKey) {
        Date fromDayKey = getDayDate(fromKey);
        Date toDayKey = getDayDate(toKey);
        SortedMap<Date, T> result = super.subMap(fromDayKey, toDayKey);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NavigableMap<Date, T> tailMap(Date fromKey, boolean inclusive) {
        Date fromDayKey = getDayDate(fromKey);
        NavigableMap<Date, T> result = super.tailMap(fromDayKey, inclusive);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedMap<Date, T> tailMap(Date fromKey) {
        Date fromDayKey = getDayDate(fromKey);
        SortedMap<Date, T> result = super.tailMap(fromDayKey);
        return result;
    }

    /**
     * Get date at 0h00:00.000.
     *
     * @param date date to convert
     * @return day date
     */
    protected Date getDayDate(Date date) {
        Date ldate = (Date) date.clone();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ldate);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date result = calendar.getTime();

        return result;
    }
}
