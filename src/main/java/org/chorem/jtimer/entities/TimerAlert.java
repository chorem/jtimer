/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.entities;

import java.io.Serializable;

/**
 * Alert.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TimerAlert implements Serializable, Cloneable {

    /** serialVersionUID. */
    private static final long serialVersionUID = 584921087501157113L;

    /**
     * Alert type.
     */
    public enum Type {
        REACH_DAILY_TIME,
        REACH_TOTAL_TIME
    }

    /** Alert type. */
    protected Type type;

    /** Alert duration. (in ms) */
    protected long duration;

    /**
     * Constructor.
     */
    public TimerAlert() {

    }

    /**
     * Constructor.
     *
     * @param type type
     * @param duration duration (ms)
     */
    public TimerAlert(Type type, long duration) {
        this();
        this.type = type;
        this.duration = duration;
    }

    /**
     * Get alert type.
     *
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * Set alert type.
     *
     * @param type the type to set
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Get duration.
     *
     * @return the duration in seconds
     */
    public long getDuration() {
        return duration;
    }

    /**
     * Set duration.
     *
     * @param duration the duration to set (in seconds)
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public TimerAlert clone() {
        TimerAlert clone;
        try {
            clone = (TimerAlert) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Can't clone", e);
        }
        return clone;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        result = 31 * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof TimerAlert) {
            TimerAlert other = (TimerAlert) obj;

            result = duration == other.duration;
            if (type != null) {
                result &= type.equals(other.type);
            } else {
                result &= other.type == null;
            }
        }
        return result;
    }
}
