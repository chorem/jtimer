/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Helper to remove process code from entity
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public class TimerTaskHelper {

    /**
     * Constructor.
     */
    protected TimerTaskHelper() {

    }

    /**
     * Timer task comparator.
     */
    protected static Comparator<TimerTask> timerTaskComparator = (o1, o2) -> {
        if (o1 == null && o2 == null) {
            return 0;
        }
        if (o1 == null) {
            return 1;
        }
        if (o2 == null) {
            return -1;
        }
        return o1.getName().compareTo(o2.getName());
    };

    /**
     * Get tasks ordered alphabetically.
     *
     * @param tasks tasks
     * @return {@code tasks}
     */
    public static List<TimerTask> sortTask(List<TimerTask> tasks) {
        Collections.sort(tasks, timerTaskComparator);
        return tasks;
    }

    /**
     * Get total task time without subtasks time.
     *
     * @param task task to get time
     * @return the alldays task time
     */
    public static long getTaskTime(TimerTask task) {

        long totalTime = 0L;

        for (Long t : task.getAllDaysAndTimes().values()) {
            totalTime += t;
        }

        return totalTime;
    }

    /**
     * Get total task time including subtasks total time.
     *
     * @param task task to get time
     * @return the alldaysTime
     */
    public static long getAllTotalTime(TimerTask task) {

        long totalTime = 0L;

        for (Long t : task.getAllDaysAndTimes().values()) {
            totalTime += t;
        }

        // add allday subtask time
        for (TimerTask t2 : task.getSubTasks()) {
            totalTime += getAllTotalTime(t2);
        }

        return totalTime;
    }

    /**
     * Get time for specified date including subtasks time.
     *
     * @param task task
     * @param date date for day
     * @return time in seconds
     */
    public static long getTotalTime(TimerTask task, Date date) {
        long result;

        // only task time
        result = task.getTime(date);

        // add today subtask time
        for (TimerTask t2 : task.getSubTasks()) {
            result += getTotalTime(t2, date);
        }

        return result;
    }

    /**
     * Get first date of timing for a task.
     *
     * @param task task to find time
     * @return date
     */
    public static Date getTaskFirstDateOfTiming(TimerTask task) {

        Date date = null;

        SortedMap<Date, Long> allTiming = task.getAllDaysAndTimes();
        if (!allTiming.isEmpty()) {
            date = allTiming.firstKey();
        }

        return date;
    }

    /**
     * Get last date of timing for a task.
     *
     * @param task task to find time
     * @return date
     */
    public static Date getTaskLastDateOfTiming(TimerTask task) {

        Date date = null;

        SortedMap<Date, Long> allTiming = task.getAllDaysAndTimes();
        if (!allTiming.isEmpty()) {
            date = allTiming.lastKey();
        }

        return date;
    }

    /**
     * Return project associated to a task.
     *
     * Explore recursively all parent.
     *
     * @param task task
     * @return associated project
     */
    public static TimerProject getTaskProject(TimerTask task) {

        TimerTask currentTask = task;
        while (currentTask.getParent() != null) {
            currentTask = currentTask.getParent();
        }

        return (TimerProject) currentTask;
    }

    /**
     * Build task path.
     *
     * @param task task
     * @return a list containing all ancestor task name (and task itself)
     * ordered by ancestorness.
     */
    public static List<String> getTaskPath(TimerTask task) {
        List<String> result = new ArrayList<>();
        if (task.getParent() != null) {
            result.addAll(getTaskPath(task.getParent()));
            result.add(task.getName());
        } else {
            result.add(task.getName());
        }
        return result;
    }

    /**
     * Get all annotations of the day.
     *
     * @param date date
     * @param task the task
     *
     * @return all annotations of the specified day
     */
    public static Map<Date, String> getAnnotationMap(TimerTask task, Date date) {

        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(date);
        beginCal.set(Calendar.HOUR_OF_DAY, 0);
        beginCal.set(Calendar.MINUTE, 0);
        beginCal.set(Calendar.SECOND, 0);
        beginCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = (Calendar) beginCal.clone();
        endCal.add(Calendar.DAY_OF_YEAR, 1);

        SortedMap<Date, String> annotations = new TreeMap<>();

        task.getAllDaysAnnotations().keySet().stream()
                .filter(day -> day.compareTo(beginCal.getTime()) >= 0 && day.compareTo(endCal.getTime()) < 0)
                .forEach(day -> annotations.put(day, task.getAllDaysAnnotations().get(day)));

        return annotations;
    }

    /**
     * Get all annotations of the day.
     *
     * @param date date
     * @param task the task
     *
     * @return annotation list
     */
    public static List<String> getAnnotation(TimerTask task, Date date) {

        List<String> annotations = new ArrayList<>();

        Map<Date, String> annotationMap = getAnnotationMap(task, date);
        annotations.addAll(annotationMap.values());

        return annotations;
    }

    /**
     * Remove all annotations of the day.
     *
     * @param task the task
     * @param date date
     */
    public static void removeAnnotation(TimerTask task, Date date) {

        Calendar beginCal = Calendar.getInstance();
        beginCal.setTime(date);
        beginCal.set(Calendar.HOUR_OF_DAY, 0);
        beginCal.set(Calendar.MINUTE, 0);
        beginCal.set(Calendar.SECOND, 0);
        beginCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = (Calendar) beginCal.clone();
        endCal.add(Calendar.DAY_OF_YEAR, 1);

        for (Iterator<Date> iterator = task.getAllDaysAnnotations().keySet().iterator(); iterator.hasNext(); ) {
            Date day = iterator.next();
            if (day.compareTo(beginCal.getTime()) >= 0
                    && day.compareTo(endCal.getTime()) < 0) {
                iterator.remove();
            }
        }
    }

    /**
     * Check if task is present in tasks collection and recursive subtasks.
     *
     * @param tasks task collection to search into
     * @param task task to search
     * @return {@code true} if task has been found
     */
    public static boolean collectionContainsTask(Collection<TimerTask> tasks, TimerTask task) {

        boolean present = false;

        for (TimerTask oneTask : tasks) {
            if (task == oneTask) {
                present = true;
            }

            if (collectionContainsTask(oneTask.getSubTasks(), task)) {
                present = true;
            }
        }

        return present;
    }

    /**
     * Get list of all task parent with current task.
     *
     * @param task current task
     * @return of task parent (ordered) including current task
     */
    public static List<TimerTask> getPathFromParent(TimerTask task) {
        // compute task tree path
        List<TimerTask> components = new ArrayList<>();
        TimerTask current = task;
        while (current != null) {
            components.add(current);
            current = current.getParent();
        }

        Collections.reverse(components);

        return components;
    }
}
