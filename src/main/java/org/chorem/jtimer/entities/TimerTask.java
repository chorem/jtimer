/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.entities;

import org.chorem.jtimer.utils.DailySortedMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

/**
 * Represents a task.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TimerTask implements Cloneable,
        Comparable<TimerTask>, Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = -7590755569706702695L;

    /** Task uuid used to managed task equality. */
    protected String uuid = UUID.randomUUID().toString();

    /** Task number. */
    protected int number;

    /** Task name. */
    protected String name;

    /** Creation date. */
    protected Date creationDate;

    /** Closed task. */
    protected boolean closed;

    /**
     * Parent Task.
     * More convenient
     */
    protected TimerTask parent;

    /**
     * Map calendar of day -> time (ms). (ordered on keys)
     */
    protected SortedMap<Date, Long> allDaysTimes;

    /**
     * Map date -> annotation text.
     */
    protected SortedMap<Date, String> allDaysAnnotations;

    /**
     * Sub tasks.
     */
    protected List<TimerTask> subTasks;

    /**
     * Task alerts.
     */
    protected List<TimerAlert> alerts;

    /**
     * Constructor.
     */
    public TimerTask() {
        allDaysTimes = new DailySortedMap<>();
        // les annotation sont à la seconde pres
        allDaysAnnotations = new TreeMap<>();
        subTasks = new ArrayList<>();
        alerts = new ArrayList<>();
        // wrong value to detect bug
        number = -1;
    }

    /**
     * Constructor with name.
     *
     * @param name task name
     */
    public TimerTask(String name) {
        this();
        this.name = name;
    }

    /**
     * Get task number.
     *
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * Set task number.
     *
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Get task name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set task name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get task creation date.
     *
     * @return task creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Set task creation date.
     *
     * @param creationDate creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Get closed task state.
     *
     * @return <tt>true</tt> if task is closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * Set closed.
     *
     * @param closed closed
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * Get parent.
     *
     * Can be null if there is no parent.
     *
     * @return the parent
     */
    public TimerTask getParent() {
        return parent;
    }

    /**
     * Set parent.
     *
     * @param parent the parent to set
     */
    protected void setParent(TimerTask parent) {
        // will cause an infinite loop
        if (parent == this) {
            throw new IllegalArgumentException("Parent can't be current task");
        }
        this.parent = parent;

    }

    /**
     * Get task's subtasks.
     *
     * @return the subTasks
     */
    public List<TimerTask> getSubTasks() {
        return subTasks;
    }

    /**
     * Add task's subtask.
     *
     * Also add parent reference.
     *
     * @param t the task to add
     * @return success flag
     */
    public boolean addTask(TimerTask t) {

        // set parent
        t.setParent(this);
        boolean result = subTasks.add(t);
        return result;
    }

    /**
     * Add time.
     *
     * @param date date
     * @param time time in ms
     */
    public void setTime(Date date, Long time) {
        allDaysTimes.put(date, time);
    }

    /**
     * Get time at date.
     *
     * @param date date
     * @return time at specified date in ms
     */
    public long getTime(Date date) {
        long result = 0;

        Long t = allDaysTimes.get(date);
        if (t != null) {
            result = t;
        }

        return result;
    }

    /**
     * Return all data. Sorted on date.
     *
     * @return total duration of all projects
     */
    public SortedMap<Date, Long> getAllDaysAndTimes() {
        return allDaysTimes;
    }

    /**
     * Add annotation.
     *
     * @param date date
     * @param note note text
     */
    public void addAnnotation(Date date, String note) {
        allDaysAnnotations.put(date, note);
    }

    /**
     * Return all annotation, sorted on date.
     *
     * @return annotations
     */
    public SortedMap<Date, String> getAllDaysAnnotations() {
        return allDaysAnnotations;
    }

    /**
     * Add alert.
     *
     * @param alert
     */
    public void addAlert(TimerAlert alert) {
        alerts.add(alert);
    }

    /**
     * Get alert list.
     *
     * @return alerts
     */
    public List<TimerAlert> getAlerts() {
        return alerts;
    }

    /**
     * Set alert.
     *
     * @param alerts new alerts list
     */
    public void setAlert(List<TimerAlert> alerts) {
        this.alerts = alerts;
    }

    @Override
    public String toString() {
        return name + subTasks.toString();
    }

    @Override
    public int hashCode() {
        int result = uuid.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof TimerTask)) {
            return false;
        }

        return uuid.equals(((TimerTask) o).uuid);
    }

    /**
     * Clone task.
     *
     * @return task copy
     */
    @Override
    public TimerTask clone() {

        TimerTask task;

        try {
            task = (TimerTask) super.clone();

            // copy date
            task.creationDate = creationDate == null ? null
                    : (Date) creationDate.clone();

            // make new list instance
            task.allDaysTimes = new DailySortedMap<>(allDaysTimes);
            task.allDaysAnnotations = new TreeMap<>(allDaysAnnotations);
            task.subTasks = new ArrayList<>(subTasks);
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Can't clone", e);
        }

        return task;
    }

    @Override
    public int compareTo(TimerTask o) {

        int result;

        if (getName() == null) {
            result = -1;
        } else if (o.getName() == null) {
            result = 1;
        } else {
            // sort on name
            result = getName().compareTo(o.getName());
        }

        return result;
    }
}
