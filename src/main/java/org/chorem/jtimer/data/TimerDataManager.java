/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Gere les donnees. Des objets peuvent s'enregistrer pour etre notifies des
 * changements de donnees.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date: 2008-06-19 10:17:31 +0200 (jeu., 19 juin 2008)$
 * By : $Author$
 */
public class TimerDataManager {

    /** log. */
    private static Log log = LogFactory.getLog(TimerDataManager.class);

    /** Project list. */
    protected List<TimerProject> projectList;

    /** For change notification */
    protected Collection<DataEventListener> dataEventListeners;

    /** For change notification */
    protected Collection<VetoableDataEventListener> vetoableDataEventListeners;

    /**
     * Constructor.
     */
    public TimerDataManager() {

        // init data list
        projectList = new ArrayList<>();

        // init support list
        dataEventListeners = new ArrayList<>();
        vetoableDataEventListeners = new ArrayList<>();
    }

    /**
     * Add listener.
     *
     * @param listener listener
     */
    public void addDataEventListener(DataEventListener listener) {
        dataEventListeners.add(listener);
    }

    /**
     * Remove listener.
     *
     * @param listener listener
     */
    public void removeDataEventListener(DataEventListener listener) {
        dataEventListeners.remove(listener);
    }

    /**
     * Add vetoable listener.
     *
     * @param listener listener
     */
    public void addVetoableDataEventListener(
            VetoableDataEventListener listener) {
        vetoableDataEventListeners.add(listener);
    }

    /**
     * Remove vetoable listener.
     *
     * @param listener listener
     */
    public void removeVetoableDataEventListener(
            VetoableDataEventListener listener) {
        vetoableDataEventListeners.remove(listener);
    }

    /**
     * Add single project.
     *
     * @param project a project
     */
    public void addProject(TimerProject project) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkAddProject(project);
        }

        projectList.add(project);

        // fire data event
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.addProject(project);
        }
    }

    /**
     * Add single task.
     *
     * @param parent parent task
     * @param task task to add
     * @param taskTemplate task template to apply for subtasks
     */
    public void addTask(TimerTask parent, TimerTask task, String taskTemplate) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkAddTask(parent, task);
        }

        parent.addTask(task);

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.addTask(task);
        }

        if (taskTemplate != null) {
            Object subTasks = JTimer.config.getTaskTemplates().get(taskTemplate);
            createSubTasks(subTasks, task);
        }
    }

    /**
     * Recursive method to apply tempate to current task.
     *
     * @param subTasks template to apply
     * @param task current subtask
     */
    protected void createSubTasks(Object subTasks, TimerTask task) {
        if (subTasks != null && subTasks instanceof Map) {
            Map<String, Object> realSubTasks = (Map<String, Object>) subTasks;
            for (Map.Entry<String, Object> entry : realSubTasks.entrySet()) {
                String name = entry.getKey();
                Object newSubTasks = entry.getValue();

                TimerTask newTask = new TimerTask(name);

                // Fix creation date
                newTask.setCreationDate(new Date());

                task.addTask(newTask);

                // send notification
                for (DataEventListener dataEventListener : dataEventListeners) {
                    dataEventListener.addTask(newTask);
                }

                createSubTasks(newSubTasks, newTask);
            }
        }
    }

    /**
     * Add many projects.
     *
     * @param projects project collection
     */
    public void addAllProjects(Collection<TimerProject> projects) {
        if (projects != null) {
            projectList.clear();
            projectList.addAll(projects);

            // send notification
            for (DataEventListener dataEventListener : dataEventListeners) {
                dataEventListener.dataLoaded(projects);
            }
        }
    }

    /**
     * Get projects list.
     *
     * synchronized to prevent manipulation during save.
     *
     * @return list of projects
     */
    public List<TimerProject> getProjectsList() {
        return projectList;
    }

    /**
     * Change time for the given date.
     *
     * @param task the task to change time
     * @param date date to change time
     * @param value new time in ms
     */
    public void changeTaskTime(TimerTask task, Date date,
                               long value) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkSetTaskTime(task, date, value);
        }

        task.setTime(date, value);

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.setTaskTime(task, date, value);
        }
    }

    /**
     * Start task.
     *
     * @param task task to start
     */
    public void startTask(TimerTask task) {
        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.startTask(task);
        }
    }

    /**
     * Stop task.
     *
     * @param task task to stop
     */
    public void stopTask(TimerTask task) {
        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.stopTask(task);
        }
    }

    /**
     * Delete task.
     *
     * @param task task to delete
     */
    public void deleteTask(TimerTask task) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkDeleteTask(task);
        }

        // task deletion
        task.getParent().getSubTasks().remove(task);

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.deleteTask(task);
        }
    }

    /**
     * Delete project.
     *
     * @param project project to delete
     */
    public void deleteProject(TimerProject project) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkDeleteProject(project);
        }

        projectList.remove(project);

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.deleteProject(project);
        }
    }

    /**
     * Close project.
     *
     * @param project project to close
     */
    public void changeProjectCloseState(TimerProject project) {

        // fire vetoable event
        //Iterator<VetoableDataEventListener> itVetoableDataEventListener = vetoableDataEventListeners
        //        .iterator();
        //while (itVetoableDataEventListener.hasNext()) {
        //    itVetoableDataEventListener.next().checkChangeClosedState(project);
        //}

        //// send notification
        //Iterator<DataEventListener> itDataEventListener = dataEventListeners
        //       .iterator();
        //while (itDataEventListener.hasNext()) {
        //    itDataEventListener.next().preChangeClosedState(project);
        //}

        //project.setClosed(!project.isClosed());

        //// send notification
        //itDataEventListener = dataEventListeners
        //       .iterator();
        //while (itDataEventListener.hasNext()) {
        //    itDataEventListener.next().postChangeClosedState(project);
        //}

        changeTaskCloseState(project);
    }

    /**
     * Close task.
     *
     * @param task task to close
     */
    public void changeTaskCloseState(TimerTask task) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkChangeClosedState(task);
        }

        task.setClosed(!task.isClosed());

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.changeClosedState(task);
        }
    }

    /**
     * Edit project.
     *
     * @param project project to edit
     * @param newProjectName new project name
     *
     * @deprecated since 1.5.1 this is duplicated with modifyProject
     */
    @Deprecated
    public void editProject(TimerProject project,
                            String newProjectName) {

        // fire vetoable event
        TimerProject newProject = project.clone();
        newProject.setName(newProjectName);
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkModifyProject(newProject);
        }

        project.setName(newProjectName);

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.modifyProject(project);
        }
    }

    /**
     * Edit task.
     *
     * @param task task to edit
     * @param newTaskName new task name
     *
     * @deprecated since 1.5.1 this is duplicated with modifyTask
     */
    @Deprecated
    public void editTask(TimerTask task, String newTaskName) {

        // fire vetoable event
        TimerTask newTask = task.clone();
        newTask.setName(newTaskName);
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkModifyTask(newTask);
            vetoableDataEventListener.checkChangeClosedState(newTask);
        }

        task.setName(newTaskName);

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.modifyTask(task);
        }
    }

    /**
     * Modify project.
     *
     * @param project project to edit
     */
    public void modifyProject(TimerProject project) {

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.modifyProject(project);
        }
    }

    /**
     * Modify task.
     *
     * @param task task to edit
     */
    public void modifyTask(TimerTask task) {

        // send notification
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.modifyTask(task);
        }
    }

    /**
     * Move task.
     *
     * @param destination task to move to
     * @param tasksToMove tasks to move
     */
    public void moveTask(TimerTask destination, Collection<TimerTask> tasksToMove) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkMoveTask(destination, tasksToMove);
        }

        for (TimerTask taskToMove : tasksToMove) {

            // send notification (pre)
            Iterator<DataEventListener> itDataEventListener = dataEventListeners
                    .iterator();
            while (itDataEventListener.hasNext()) {
                itDataEventListener.next().preMoveTask(taskToMove);
            }

            // move task
            TimerTask actualParent = taskToMove.getParent();
            actualParent.getSubTasks().remove(taskToMove);
            destination.addTask(taskToMove);

            // send notification (post)
            itDataEventListener = dataEventListeners.iterator();
            while (itDataEventListener.hasNext()) {
                itDataEventListener.next().moveTask(taskToMove);
            }
        }
    }

    /**
     * Merge tasks.
     *
     * @param destinationTask task where task will be merged
     * @param otherTasks task to merge in first task
     */
    public void mergeTasks(TimerTask destinationTask, List<TimerTask> otherTasks) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkMergeTasks(destinationTask, otherTasks);
        }

        // send pre notification
        Iterator<DataEventListener> itDataEventListener = dataEventListeners
                .iterator();
        while (itDataEventListener.hasNext()) {
            itDataEventListener.next().preMergeTasks(destinationTask, otherTasks);
        }

        for (TimerTask otherTask : otherTasks) {
            mergeTwoTasks(destinationTask, otherTask);
        }

        // send post notification
        itDataEventListener = dataEventListeners.iterator();
        while (itDataEventListener.hasNext()) {
            itDataEventListener.next().postMergeTasks(destinationTask, otherTasks);
        }
    }

    /**
     * Merge two task together.
     *
     * @param destinationTask task where task will be merged
     * @param otherTask task to merge in first task
     */
    protected void mergeTwoTasks(TimerTask destinationTask, TimerTask otherTask) {

        if (log.isDebugEnabled()) {
            log.debug("Merging two task : " + destinationTask.getName() + " and " + otherTask.getName());
        }

        // task is modified during merge, deep clone it

        // make sub task list copy (concurrency)
        Collection<TimerTask> otherTaskSubTasks = new ArrayList<>(otherTask.getSubTasks());
        for (TimerTask otherTaskSubTask : otherTaskSubTasks) {

            // first case to care about, a task with same
            // same already exists in destination
            TimerTask sameTaskNameTask = null;
            for (TimerTask destSubTask : destinationTask.getSubTasks()) {
                if (destSubTask.getName().equals(otherTaskSubTask.getName())) {
                    sameTaskNameTask = destSubTask;
                }
            }

            // no similar task found
            if (sameTaskNameTask == null) {
                if (log.isDebugEnabled()) {
                    log.debug("Moving task " + otherTaskSubTask.getName() + " to " + destinationTask.getName());
                }
                // just move
                moveTask(destinationTask, Collections.singleton(otherTaskSubTask));
            } else {
                // task must be merged
                if (log.isDebugEnabled()) {
                    log.debug("Sub-merging of " + sameTaskNameTask.getName() + " and " + otherTaskSubTask.getName());
                }

                // recursive merge
                mergeTwoTasks(sameTaskNameTask, otherTaskSubTask);

                // TODO possible bug here, task times may not be saved
            }
        }

        // copy otherTask times to current task
        for (Entry<Date, Long> times : otherTask.getAllDaysAndTimes().entrySet()) {
            Long currentDuration = destinationTask.getTime(times.getKey());
            currentDuration += times.getValue();
            destinationTask.setTime(times.getKey(), currentDuration);
        }

        // copy annotations
        for (Entry<Date, String> note : otherTask.getAllDaysAnnotations().entrySet()) {
            Date noteKey = note.getKey();
            while (destinationTask.getAllDaysAnnotations().containsKey(noteKey)) {
                // les deux taches ont des notes au meme moments
                // on ne deplace à la prochaine seconde

                if (log.isDebugEnabled()) {
                    log.debug("Annotation collision detected, try next second");
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(noteKey);
                calendar.add(Calendar.SECOND, 1);
                noteKey = calendar.getTime();
            }
            destinationTask.addAnnotation(noteKey, note.getValue());
        }

        // copy alerts
        otherTask.getAlerts().forEach(destinationTask::addAlert);

        // finally otherTask still exist
        // empty, but still exist
        deleteTask(otherTask);
    }

    /**
     * Add annotation on task for specified calendar, and send event.
     *
     * @param task task
     * @param date day of annotation
     * @param annotation annotation
     */
    public void addAnnotation(TimerTask task, Date date,
                              String annotation) {

        // fire vetoable event
        for (VetoableDataEventListener vetoableDataEventListener : vetoableDataEventListeners) {
            vetoableDataEventListener.checkSetAnnotation(task, date,
                    annotation);
        }

        task.addAnnotation(date, annotation);

        // send event
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.setAnnotation(task, date, annotation);
        }
    }

    /**
     * Notify alert modification.
     *
     * @param task task where alert has been modified
     */
    public void modifyAlert(TimerTask task) {

        // send event
        for (DataEventListener dataEventListener : dataEventListeners) {
            dataEventListener.modifyTask(task);
        }

    }

    /**
     * Return the corresponding task for path.
     *
     * @param taskPath task path to find
     * @return corresponding task for path
     */
    public TimerTask getTaskForPath(String taskPath) {

        String[] paths = taskPath.split("/");

        // make sure can't select only project
        if (paths.length <= 1) {
            throw new IllegalArgumentException("Can't parse argument '" + taskPath + "' as task");
        }

        return findTask(projectList, paths);
    }

    /**
     * Find task in tasks collections.
     *
     * @param tasks task collection
     * @param paths task path to find
     * @return found task
     */
    protected TimerTask findTask(Collection<? extends TimerTask> tasks, String[] paths) {
        String current = paths[0];

        TimerTask foundTask = null;
        for (TimerTask subtask : tasks) {
            if (subtask.getName().equals(current)) {
                foundTask = subtask;
            }
        }

        if (foundTask != null && paths.length > 1) {
            String[] subPaths = ArrayUtils.subarray(paths, 1, paths.length);
            foundTask = findTask(foundTask.getSubTasks(), subPaths);
        }

        return foundTask;
    }
}
