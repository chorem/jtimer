/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;

import java.util.Collection;
import java.util.List;

/**
 * Common jtimer vetoable politics.
 *
 * Check that:
 * - a task name doesn't appears twice in the same level.
 * - creation
 * - modification
 * - move
 */
public class CommonVetoable implements VetoableDataEventListener {

    /** log. */
    private static Log log = LogFactory.getLog(CommonVetoable.class);

    /**
     * Duplicated project violation key.
     */
    protected static final String DUPLICATED_PROJECT_VIOLATION = "vetoable.common.duplicated.project.name";

    /**
     * Duplicated task violation key.
     */
    protected static final String DUPLICATED_TASK_VIOLATION = "vetoable.common.duplicated.task.name";

    /**
     * Violation if try to move project into task.
     */
    protected static final String MOVE_INVALID_TYPES_VIOLATION = "vetoable.common.move.invalid.types";

    /**
     * Violation if try to merge task and project.
     */
    protected static final String MERGE_INVALID_TYPES_VIOLATION = "vetoable.common.merge.invalid.types";

    /**
     * Data manager.
     */
    protected TimerDataManager manager;

    /**
     * Constructor.
     *
     * @param manager data manager
     */
    public CommonVetoable(TimerDataManager manager) {
        this.manager = manager;
    }

    /**
     * Test if task name is found is tasks list.
     *
     * @param task  task to check
     * @param tasks tasks list to search into
     * @return founded
     */
    protected boolean isSameTaskName(TimerTask task,
                                     List<? extends TimerTask> tasks) {
        boolean found = false;

        for (TimerTask currentTask : tasks) {
            if (currentTask.getName().equals(task.getName())) {
                found = true;
            }
        }

        return found;
    }

    @Override
    public void checkAddProject(TimerProject project) {

        // check duplicated project name
        if (isSameTaskName(project, manager.getProjectsList())) {
            if (log.isDebugEnabled()) {
                log.debug("Duplicated name, checkAddProject won't pass");
            }
            throw new DataViolationException("Can't add project", DUPLICATED_PROJECT_VIOLATION);
        }
    }

    @Override
    public void checkAddTask(TimerTask parent, TimerTask task) {

        // check duplicated task name
        if (isSameTaskName(task, parent.getSubTasks())) {
            if (log.isDebugEnabled()) {
                log.debug("Duplicated name, checkAddTask won't pass");
            }
            throw new DataViolationException("Can't add task", DUPLICATED_TASK_VIOLATION);
        }
    }

    @Override
    public void checkModifyProject(TimerProject project) {

        // check duplicated project name
        if (isSameTaskName(project, manager.getProjectsList())) {
            if (log.isDebugEnabled()) {
                log.debug("Duplicated name, checkModifyProject won't pass");
            }
            throw new DataViolationException("Can't modify project", DUPLICATED_PROJECT_VIOLATION);
        }
    }

    @Override
    public void checkModifyTask(TimerTask task) {

        // check duplicated task name
        if (isSameTaskName(task, task.getSubTasks())) {
            if (log.isDebugEnabled()) {
                log.debug("Duplicated name, checkModifyTask won't pass");
            }
            throw new DataViolationException("Can't modify task", DUPLICATED_TASK_VIOLATION);
        }

    }

    @Override
    public void checkMoveTask(TimerTask destination, Collection<TimerTask> tasksToMove) {

        // check if destination is one task to move
        // no sens
        if (TimerTaskHelper.collectionContainsTask(tasksToMove, destination)) {
            if (log.isDebugEnabled()) {
                log.debug("Move task into himself, impossible");
            }
            throw new DataViolationException("Can't move task into himself", MOVE_INVALID_TYPES_VIOLATION);
        }

        for (TimerTask taskToMove : tasksToMove) {

            // can't move projects
            if (taskToMove instanceof TimerProject) {
                if (log.isDebugEnabled()) {
                    log.debug("Move project, impossible");
                }
                throw new DataViolationException("Can't move project", MOVE_INVALID_TYPES_VIOLATION);
            }

            // check duplicated task name
            if (isSameTaskName(taskToMove, destination.getSubTasks())) {
                if (log.isDebugEnabled()) {
                    log.debug("Duplicated name, checkMoveTask won't pass");
                }
                throw new DataViolationException("Can't move task", DUPLICATED_TASK_VIOLATION);
            }
        }
    }

    @Override
    public void checkMergeTasks(TimerTask destinationTask,
                                List<TimerTask> otherTasks) {

        // tous les taches sont des projets
        if (destinationTask instanceof TimerProject) {
            for (TimerTask otherTask : otherTasks) {
                if (!(otherTask instanceof TimerProject)) {
                    throw new DataViolationException("Can't merge task", MERGE_INVALID_TYPES_VIOLATION);
                }
            }
        } else {
            // ou toutes des taches
            for (TimerTask otherTask : otherTasks) {
                if (otherTask instanceof TimerProject) {
                    throw new DataViolationException("Can't merge task", MERGE_INVALID_TYPES_VIOLATION);
                }
            }

            // mais pas une combinaison des deux
        }
    }
}
