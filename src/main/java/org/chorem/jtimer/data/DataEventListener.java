/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;

import java.util.Collection;
import java.util.Date;
import java.util.EventListener;
import java.util.List;

/**
 * Data event listener.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public interface DataEventListener extends EventListener {

    /**
     * Add project event.
     *
     * @param project project to add
     */
    default void addProject(TimerProject project) {

    }

    /**
     * Add task event.
     *
     * @param task task to add
     */
    default void addTask(TimerTask task) {

    }

    /**
     * Modify project.
     *
     * @param project modified project
     */
    default void modifyProject(TimerProject project) {

    }

    /**
     * Modify task.
     *
     * @param task modified task
     */
    default void modifyTask(TimerTask task) {

    }

    /**
     * Delete project.
     *
     * @param project deleted project
     */
    default void deleteProject(TimerProject project) {

    }

    /**
     * Delete task.
     *
     * @param task deleted task
     */
    default void deleteTask(TimerTask task) {

    }

    /**
     * Add annotation.
     *
     * @param task       task where annotation is changed
     * @param date       date
     * @param annotation new annotation value
     */
    default void setAnnotation(TimerTask task, Date date, String annotation) {

    }

    /**
     * Set task time.
     *
     * @param task task where time is changed
     * @param date date
     * @param time task time
     */
    default void setTaskTime(TimerTask task, Date date, Long time) {

    }

    /**
     * Change task state.
     *
     * @param task task
     */
    default void changeClosedState(TimerTask task) {

    }

    /**
     * Pre move task.
     *
     * @param task task
     */
    default void preMoveTask(TimerTask task) {

    }

    /**
     * Post move task.
     *
     * @param task task
     */
    default void moveTask(TimerTask task) {

    }

    /**
     * Pre merge task.
     *
     * @param destinationTask task where all other task will be merged
     * @param otherTasks      other tasks to merge
     */
    default void preMergeTasks(TimerTask destinationTask, List<TimerTask> otherTasks) {

    }

    /**
     * Post merge task.
     *
     * @param destinationTask task where all other task will be merged
     * @param otherTasks      other tasks to merge
     */
    default void postMergeTasks(TimerTask destinationTask, List<TimerTask> otherTasks) {

    }

    /**
     * Start task.
     *
     * @param task started task
     */
    default void startTask(TimerTask task) {

    }

    /**
     * Stop task.
     *
     * @param task stopped task
     */
    default void stopTask(TimerTask task) {

    }

    /**
     * All data loaded.
     *
     * @param projects projects collection
     */
    default void dataLoaded(Collection<TimerProject> projects) {

    }
}

