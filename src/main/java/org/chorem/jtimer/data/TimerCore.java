/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2018 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.JTimer;
import org.chorem.jtimer.JTimerFactory;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.io.DataLockingException;
import org.chorem.jtimer.io.Saver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * TimerCore
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TimerCore {

    /** log. */
    private static Log log = LogFactory.getLog(TimerCore.class);

    /** Timer data. */
    protected TimerDataManager data;

    /** saver io controller. */
    protected Saver saver;

    /**
     * Constructor.
     */
    public TimerCore() {

        // migrate if necessary
        try {
            migrateDirectoryStructure();
        } catch (IOException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't migrate directories", ex);
            }
        }

        // init data
        data = new TimerDataManager();

        // add commmon vetoable
        CommonVetoable commonVetoable = new CommonVetoable(data);
        data.addVetoableDataEventListener(commonVetoable);

        // init saver implementation
        saver = JTimerFactory.getFileSaver();
        if (saver != null) {
            data.addVetoableDataEventListener(saver);
            data.addDataEventListener(saver);
        }
    }

    /**
     * Migrate file structure for jtimer 1.5 directories changes.
     *
     * @throws IOException
     */
    protected void migrateDirectoryStructure() throws IOException {
        File newDataDirectory = JTimer.config.getDataDirectory();
        File newBackupDirectory = JTimer.config.getBackupDirectory();
        File oldBackupDirectory = JTimer.config.getGtimerBackupDirectory();
        File oldDataDirectory = JTimer.config.getGtimerDataDirectory();

        // migrate data
        if (!newDataDirectory.isDirectory() && oldDataDirectory.isDirectory()) {
            newDataDirectory.mkdirs();

            if (log.isInfoEnabled()) {
                log.info("Migrate data directory from " + oldDataDirectory + " to " + newDataDirectory);
            }

            // exclude internal backup directory from recursion
            FileUtils.copyDirectory(oldDataDirectory, newDataDirectory, pathName -> !pathName.equals(oldBackupDirectory));
        }

        // migrate backup
        if (!newBackupDirectory.isDirectory() && oldBackupDirectory.isDirectory()) {
            newBackupDirectory.mkdirs();

            if (log.isInfoEnabled()) {
                log.info("Migrate backup directory from " + oldBackupDirectory + " to " + newBackupDirectory);
            }

            FileUtils.copyDirectory(oldBackupDirectory, newBackupDirectory);
        }
    }

    /**
     * Init Load and launch synchronization.
     *
     * @return true if init has gone successfully
     */
    public boolean init() {

        boolean initSucceded = false;

        // log
        if (log.isInfoEnabled()) {
            log.info("Init core");
        }

        try {
            saver.lock();

            load();

            initSucceded = true;
        } catch (DataLockingException e) {
            // can occurs if data save directory is locked
            if (log.isDebugEnabled()) {
                log.error("Data can't have been loaded");
            }
        }

        return initSucceded;
    }

    /**
     * Get data.
     *
     * @return timer data
     */
    public TimerDataManager getData() {
        return data;
    }

    /**
     * Load project list from.
     */
    protected void load() {

        // log
        if (log.isInfoEnabled()) {
            log.info("Load local data");
        }

        Collection<TimerProject> projects = saver.load();

        // sort loaded collection
        List<TimerProject> projectsList = new ArrayList<>(projects);
        Collections.sort(projectsList);

        data.addAllProjects(projectsList);
    }

    /**
     * Save.
     */
    public void exit() {
        if (log.isInfoEnabled()) {
            log.info("Exiting application");
        }
        // unlock fs directory
        try {
            saver.unlock();
        } catch (DataLockingException e) {
            if (log.isErrorEnabled()) {
                log.error("Error on unlocking", e);
            }
        }
    }
}
