/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;

import java.util.Collection;
import java.util.Date;
import java.util.EventListener;
import java.util.List;

/**
 * Vetoable Data event listener.
 *
 * @author chorlet
 * @version $Revision$
 *
 * Last update : $Date$ By : $Author$
 */
public interface VetoableDataEventListener extends EventListener {

    /**
     * Check add project.
     *
     * @param project project to add
     */
    default void checkAddProject(TimerProject project) {

    }

    /**
     * Check add task.
     *
     * @param parent parent task
     * @param task   task to add
     */
    default void checkAddTask(TimerTask parent, TimerTask task) {

    }

    /**
     * Check modify project.
     *
     * @param project modified project
     */
    default void checkModifyProject(TimerProject project) {

    }

    /**
     * Check modify task.
     *
     * @param task modified task
     */
    default void checkModifyTask(TimerTask task) {

    }

    /**
     * Check delete project.
     *
     * @param project deleted project
     */
    default void checkDeleteProject(TimerProject project) {

    }

    /**
     * Check delete task.
     *
     * @param task deleted task
     */
    default void checkDeleteTask(TimerTask task) {

    }

    /**
     * Check update task annotation.
     *
     * @param task  task to update
     * @param date  day of change
     * @param value new annotation
     */
    default void checkSetAnnotation(TimerTask task, Date date, String value) {

    }

    /**
     * Check update task time.
     *
     * @param task  task to update
     * @param date  day of change
     * @param value new time in seconds
     */
    default void checkSetTaskTime(TimerTask task, Date date, Long value) {

    }

    /**
     * Check change task state.
     *
     * @param task task
     */
    default void checkChangeClosedState(TimerTask task) {

    }

    /**
     * Check move task.
     *
     * @param destination task to move to
     * @param tasksToMove tasks to move
     */
    default void checkMoveTask(TimerTask destination, Collection<TimerTask> tasksToMove) {

    }

    /**
     * Check merge task.
     *
     * @param destinationTask task result of merge
     * @param otherTasks      other task to merge
     */
    default void checkMergeTasks(TimerTask destinationTask, List<TimerTask> otherTasks) {

    }
}
