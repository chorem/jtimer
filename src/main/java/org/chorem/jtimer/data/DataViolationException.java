/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

/**
 * Exception thrown when a data violation is detected.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class DataViolationException extends RuntimeException {

    /** serialVersionUID. */
    private static final long serialVersionUID = 7541490706942030715L;

    /**
     * Exception key message;
     */
    protected String exceptionKey;

    /**
     * Constructor with text message.
     *
     * @param message text message
     * @param exceptionKey exception key
     */
    public DataViolationException(String message, String exceptionKey) {
        super(message);
        this.exceptionKey = exceptionKey;
    }

    /**
     * Constructor with text message and cause.
     *
     * @param message text message
     * @param exceptionKey exception key
     * @param cause parent cause
     */
    public DataViolationException(String message, String exceptionKey, Throwable cause) {
        super(message, cause);
        this.exceptionKey = exceptionKey;
    }

    /**
     * Get exception key.
     *
     * @return the exceptionKey
     */
    public String getExceptionKey() {
        return exceptionKey;
    }
}
