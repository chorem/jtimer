/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.jtimer.system.win32;

import com.sun.jna.Native;
import com.sun.jna.Structure;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import java.util.Arrays;
import java.util.List;

/**
 * Win User32 JNA Interface.
 *
 * TODO it not the same code as jna's User32 class.
 *
 * @author chatellier
 * @version $Revision$
 *
 * @see <a href="http://msdn.microsoft.com/en-us/library/ms646272">Windows API</a>
 *
 * Last update : $Date$
 * By : $Author$
 */
public interface User32 extends StdCallLibrary {

    /** Instance. */
    User32 INSTANCE = Native.load("user32", User32.class, W32APIOptions.DEFAULT_OPTIONS);

    /**
     * Contains the time of the last input.
     */
    class LASTINPUTINFO extends Structure {
        public int cbSize = 8;

        // / Tick count of when the last input event was received.
        public int dwTime;

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("cbSize", "dwTime");
        }
    }

    /**
     * Retrieves the time of the last input event.
     *
     * @param result time of the last input event, in milliseconds
     * @return boolean flag
     */
    boolean GetLastInputInfo(LASTINPUTINFO result);
}
