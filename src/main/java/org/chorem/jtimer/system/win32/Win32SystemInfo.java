/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2019 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.system.win32;

import org.chorem.jtimer.system.SystemInfo;

/**
 * Win32 System info.
 *
 * From http://ochafik.free.fr/blog/?p=98
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class Win32SystemInfo implements SystemInfo {

    /** {@link User32.LASTINPUTINFO}. */
    protected User32.LASTINPUTINFO lastInputInfo;

    /**
     * Constructor.
     *
     * Package visibility
     */
    public Win32SystemInfo() {
        lastInputInfo = new User32.LASTINPUTINFO();
    }

    /**
     * Get the amount of milliseconds that have elapsed since the last input
     * event (mouse or keyboard)
     *
     * @return idle time in milliseconds
     */
    public int getIdleTimeMillisWin32() {
        User32.INSTANCE.GetLastInputInfo(lastInputInfo);
        return Kernel32.INSTANCE.GetTickCount() - lastInputInfo.dwTime;
    }

    @Override
    public long getIdleTime() {
        long millisTime = getIdleTimeMillisWin32();
        return millisTime;
    }
}
