/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.system;

import com.sun.jna.Platform;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.system.macos.MacOSSystemInfo;
import org.chorem.jtimer.system.unix.UnixSystemInfo;
import org.chorem.jtimer.system.win32.Win32SystemInfo;

import java.util.Optional;

/**
 * Build system info determined from system.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class SystemInfoFactory {

    /** log */
    private static Log log = LogFactory.getLog(SystemInfoFactory.class);

    /** Single instance */
    protected static SystemInfo instance;

    /**
     * SystemInfoFactory Constructor.
     */
    protected SystemInfoFactory() {

    }

    /**
     * Return system info depending on system.
     *
     * @return SystemInfo instance
     * @see SystemInfo
     */
    public static Optional<SystemInfo> getSystemInfo() {

        if (instance == null) {
            String os = System.getProperty("os.name");

            // log it
            if (log.isInfoEnabled()) {
                log.info("Try do build system info for system : " + os);
            }

            // try windows
            if (Platform.isWindows()) {
                instance = new Win32SystemInfo();
            } else if (Platform.isLinux()) {
                instance = new UnixSystemInfo();
            } else if (Platform.isMac()) {
                instance = new MacOSSystemInfo();
            }
        }

        return Optional.ofNullable(instance);
    }
}
