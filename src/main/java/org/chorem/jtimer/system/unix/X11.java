/*
 * %%Ignore-License
 * Copyright (c) 2007 Timothy Wall, All Rights Reserved
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * <p/>
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package org.chorem.jtimer.system.unix;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;

/** Definition (incomplete) of the X library. */
public interface X11 extends Library {

    class Drawable extends NativeLong {
        private static final long serialVersionUID = 1L;
    }

    class Window extends Drawable {
        private static final long serialVersionUID = 1L;
    }

    class Display extends PointerType {
    }

    X11 INSTANCE = Native.load("X11", X11.class);

    Display XOpenDisplay(String name);

    Window XDefaultRootWindow(Display display);

    int XFree(Pointer data);

    int XCloseDisplay(Display display);

}
