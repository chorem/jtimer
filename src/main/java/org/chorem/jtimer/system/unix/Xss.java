/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.system.unix;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Structure;
import com.sun.jna.ptr.IntByReference;

import java.util.Arrays;
import java.util.List;

/**
 * libXss JNA interface.
 *
 * Incomplete definition.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public interface Xss extends Library {

    /** Xss Instance */
    Xss INSTANCE = Native.load("Xss", Xss.class);

    /**
     * XScreenSaverInfo struct
     */
    class XScreenSaverInfo extends Structure {
        public X11.Window window; /* screen saver window - may not exist */
        public int state; /* ScreenSaverOff, ScreenSaverOn, ScreenSaverDisabled */
        public int kind; /* ScreenSaverBlanked, ...Internal, ...External */
        public NativeLong til_or_since; /* time til or since screen saver */
        public NativeLong idle; /* total time since last user input */
        public NativeLong eventMask; /* currently selected events for this client */

        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList("window", "state", "kind", "til_or_since", "idle", "eventMask");
        }

    }

    /**
     * Information sur l'activation de l'ecran de veille de X11.
     *
     * @param display display
     * @param event_base event_base
     * @param error_base error_base
     *
     * @return true if XScreenSaver extension is loaded
     *
     * @see IntByReference
     */
    boolean XScreenSaverQueryExtension(X11.Display display,
                                       IntByReference event_base, IntByReference error_base);

    /**
     * XScreenSaverAllocInfo.
     *
     * @return XScreenSaverInfo instance
     */
    XScreenSaverInfo XScreenSaverAllocInfo();

    /**
     * XScreenSaverQueryInfo.
     *
     * @param display
     * @param drawable
     * @param infos
     * @return status flag
     */
    int XScreenSaverQueryInfo(X11.Display display,
                              X11.Drawable drawable, XScreenSaverInfo infos);
}
