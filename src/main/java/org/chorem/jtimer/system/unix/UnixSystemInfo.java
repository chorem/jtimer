/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2019 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.system.unix;

import com.sun.jna.ptr.IntByReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.system.SystemInfo;

import java.awt.Toolkit;

/**
 * Linux system info.
 *
 * Based on
 * http://java.net/projects/jdic/sources/svn/content/trunk/src/incubator/systeminfo/src/unix/native/jni/systeminfo.c source code.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class UnixSystemInfo implements SystemInfo {

    /** log. */
    private static Log log = LogFactory.getLog(UnixSystemInfo.class);

    @Override
    public long getIdleTime() {

        long idleTime;

        try {
            idleTime = getXssIdleTime();
        } catch (Exception e) {
            // TODO catch more specific exception
            if (log.isErrorEnabled()) {
                log.error("Can't detect idle time : ", e);
            }
            idleTime = 0;
        }

        return idleTime;
    }

    /**
     * Get idle time via Xss.
     *
     * @return idle time
     */
    protected long getXssIdleTime() {

        long idleTime = 0;

        // get X11 display
        X11.Display display = X11.INSTANCE.XOpenDisplay(null);
        if (display == null) {
            log.error("Can't open X11 display");
        } else {
            // Display is opened

            // check if screensaver extension is enabled
            IntByReference eventBase = new IntByReference();
            IntByReference errorBase = new IntByReference();

            if (!Xss.INSTANCE.XScreenSaverQueryExtension(display, eventBase,
                    errorBase)) {
                if (log.isErrorEnabled()) {
                    log.error("Can't find xscreensaver extension, "
                            + "idle time can't be detected");
                }
            } else {
                // screensaver is enabled

                Xss.XScreenSaverInfo screenSaverInfo = Xss.INSTANCE
                        .XScreenSaverAllocInfo();

                if (screenSaverInfo == null) {
                    log.error("Could not alloc screen saver info");
                } else {
                    // get root window
                    X11.Window rootWindow = X11.INSTANCE
                            .XDefaultRootWindow(display);

                    if (rootWindow == null) {
                        log.error("Could not query root window");
                    } else {

                        // FIX : without this, cause random NPE
                        screenSaverInfo.window = rootWindow;

                        Xss.INSTANCE.XScreenSaverQueryInfo(display, rootWindow,
                                screenSaverInfo);
                        if (screenSaverInfo.idle == null) {
                            log.error("screenSaverInfo.idle is null");
                        } else {
                            idleTime = screenSaverInfo.idle.longValue();
                        }
                    }

                    // free resources
                    X11.INSTANCE.XFree(screenSaverInfo.getPointer());
                }
            }

            // free resources
            X11.INSTANCE.XCloseDisplay(display);
        }

        return idleTime;
    }

}
