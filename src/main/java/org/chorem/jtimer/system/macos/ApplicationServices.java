/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2010 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.system.macos;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * Quartz 2D API.
 *
 * @author chatellier
 * @version $Revision$
 * @since 1.3.2
 *
 * Last update : $Date$
 * By : $Author: chatellier $
 */
public interface ApplicationServices extends Library {

    ApplicationServices INSTANCE = Native.load("ApplicationServices", ApplicationServices.class);

    /** Constants that specify an input event. */
    int kCGAnyInputEventType = -1;
    /** Specifies that an event source should use the event state table that reflects the combined state of all event sources posting to the current user login session. */
    int kCGEventSourceStateCombinedSessionState = 0;

    /**
     * Returns the elapsed time since the last event for a Quartz event source.
     *
     * @param sourceStateId The source state to access
     * @param eventType The event type to access
     * @return the time in seconds since the previous input event of the specified type
     *
     * @see <a href="http://developer.apple.com/mac/library/documentation/Carbon/Reference/QuartzEventServicesRef/Reference/reference.html#//apple_ref/c/func/CGEventSourceSecondsSinceLastEventType">Quartz API</a>
     */
    double CGEventSourceSecondsSinceLastEventType(int sourceStateId, int eventType);
}
