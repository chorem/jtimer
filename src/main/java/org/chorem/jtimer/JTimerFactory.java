/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2007 - 2020 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.io.Saver;

import java.lang.reflect.InvocationTargetException;

/**
 * JTimer config class.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class JTimerFactory {

    /** Logger */
    private static Log log = LogFactory.getLog(JTimerFactory.class);

    /** Saver */
    protected static Saver saver;

    /**
     * Constructeur.
     */
    protected JTimerFactory() {
        // disable instantiation
    }

    /**
     * Get saver manager.
     *
     * @return saver manager
     */
    public static Saver getFileSaver() {

        if (saver == null) {

            Class<?> saverClass = JTimer.config.getIOSaverClass();

            // log
            if (log.isInfoEnabled()) {
                log.info("Using saver class : " + saverClass);
                log.info(" with saver home directory : " + JTimer.config.getDataDirectory());
                log.info(" with auto save delay : " + JTimer.config.getIOSaverAutoSaveDelay());
            }

            try {
                // get instance
                Class<?>[] empty = {};
                saver = (Saver) saverClass.getConstructor(empty).newInstance();

                // set delay to saver
                saver.setAutoSaveDelay(JTimer.config.getIOSaverAutoSaveDelay() * 1000);

            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't instantiate saver : " + saverClass, e);
                }
            }
        }

        return saver;
    }
}
