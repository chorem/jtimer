/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2012 - 2018 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

/**
 * JTimer configuration.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class JTimerConfig {

    private static final Log log = LogFactory.getLog(JTimerConfig.class);

    /** To tag action to be run before ui start. */
    protected static final int STEP_BEFORE_UI = 0;
    /** To tag action to be run after ui start. */
    protected static final int STEP_AFTER_UI = 1;

    protected ApplicationConfig appConfig;

    public JTimerConfig() {
        appConfig = new ApplicationConfig();
        for (JTimerOption o : JTimerOption.values()) {
            if (o.defaultValue != null) {
                appConfig.setDefaultOption(o.key, o.defaultValue);
            }
        }
        for (JTimerAction a : JTimerAction.values()) {
            for (String alias : a.aliases) {
                appConfig.addActionAlias(alias, a.action);
            }
        }
    }

    public void parse(String... args) {
        try {
            appConfig.parse(args);
        } catch (ArgumentsParserException ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't parse configuration", ex);
            }
        }
    }

    /**
     * Put new custom object into app config context.
     *
     * @param o object value
     */
    public void putObject(Object o) {
        appConfig.putObject(o);
    }

    /**
     * Run command line actions for step.
     *
     * @param step action step
     */
    public void doAction(int step) {
        try {
            appConfig.doAction(step);
        } catch (Exception ex) {
            if (log.isErrorEnabled()) {
                log.error("Can't run command line actions", ex);
            }
        }
    }

    /**
     * Get IO class implementation.
     *
     * @return IO save class
     */
    public Class<?> getIOSaverClass() {
        return appConfig.getOptionAsClass(JTimerOption.IO_SAVER_CLASS.key);
    }

    /**
     * Get jtimer data directory.
     *
     * @since 1.5.1
     * @return jtimer 1.5.1 sync directory
     */
    public File getHomeDirectory() {
        return appConfig.getOptionAsFile(JTimerOption.HOME_DIRECTORY.key);
    }

    /**
     * Get jtimer data directory.
     *
     * @since 1.5
     * @return jtimer 1.5 data directory
     */
    public File getDataDirectory() {
        return appConfig.getOptionAsFile(JTimerOption.DATA_DIRECTORY.key);
    }

    /**
     * Get jtimer backup directory.
     *
     * @since 1.5
     * @return jtimer 1.5 backup directory
     */
    public File getBackupDirectory() {
        return appConfig.getOptionAsFile(JTimerOption.BACKUP_DIRECTORY.key);
    }

    /**
     * Get gtimer data directory.
     *
     * @since 1.5
     * @return jtimer &lt; 1.5 data directory
     */
    public File getGtimerDataDirectory() {
        return appConfig.getOptionAsFile(JTimerOption.GTIMER_DATA_DIRECTORY.key);
    }

    /**
     * Get gtimer backup directory.
     *
     * @since 1.5
     * @return jtimer &lt; 1.5 backup directory
     */
    public File getGtimerBackupDirectory() {
        return appConfig.getOptionAsFile(JTimerOption.GTIMER_BACKUP_DIRECTORY.key);
    }

    /**
     * Return auto save delay in seconds.
     *
     * @return auto save delay
     */
    public long getIOSaverAutoSaveDelay() {
        return appConfig.getOptionAsLong(JTimerOption.IO_SAVER_AUTOSAVEDELAY.key);
    }

    /**
     * Return user idle time threshold in seconds.
     *
     * @return idle time threshold
     */
    public long getIdleTime() {
        return appConfig.getOptionAsLong(JTimerOption.UI_IDLE_TIME.key);
    }

    /**
     * Show closed project on main ui tree.
     *
     * @return show closed project
     */
    public boolean isShowClosed() {
        return appConfig.getOptionAsBoolean(JTimerOption.UI_SHOW_CLOSED.key);
    }

    /**
     * Set show closed option.
     *
     * @param showClosed show closed
     */
    public void setShowClosed(boolean showClosed) {
        appConfig.setOption(JTimerOption.UI_SHOW_CLOSED.key, String.valueOf(showClosed));
        appConfig.saveForUser();
    }

    /**
     * Get task template from configuration.
     *
     * @return task template as map
     */
    public Map<String, Object> getTaskTemplates() {
        Map<String, Object> result = new TreeMap<>();

        String tplDirectory = appConfig.getOption(JTimerOption.TEMPLATE_DIRECTORY.key);
        Path tplDir = FileSystems.getDefault().getPath(tplDirectory);

        if (Files.exists(tplDir)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(tplDir)) {
                Yaml yaml = new Yaml();
                for (Path file : stream) {
                    try (InputStream is = new FileInputStream(file.toFile())) {
                        Object r = yaml.load(is);
                        String name = FilenameUtils.removeExtension(file.getFileName().toString());
                        result.put(name, r);
                    }
                }
            } catch (DirectoryIteratorException | IOException ex) {
                if (log.isErrorEnabled()) {
                    log.error("Can't create backup", ex);
                }
            }
        }

        return result;
    }

    /**
     * Get close to systray option.
     *
     * @return close to systray
     */
    public boolean isCloseToSystray() {
        return appConfig.getOptionAsBoolean(JTimerOption.UI_CLOSE_TO_SYSTRAY.key);
    }

    /**
     * Set close to systray option
     *
     * @param closeToSystray close to systray
     */
    public void setCloseToSystray(boolean closeToSystray) {
        appConfig.setOption(JTimerOption.UI_CLOSE_TO_SYSTRAY.key, String.valueOf(closeToSystray));
        appConfig.saveForUser();
    }

    /**
     * Set showTaskNameInSystray option
     *
     * @param showTaskNameInSystray showTaskNameInSystray
     */
    public void setShowTaskNameInSystray(boolean showTaskNameInSystray) {
        appConfig.setOption(JTimerOption.UI_SHOW_TASK_NAME_IN_SYSTRAY.key, String.valueOf(showTaskNameInSystray));
        appConfig.saveForUser();
    }

    /**
     * Get showTaskNameInSystray option.
     *
     * @return showTaskNameInSystray
     */
    public boolean isShowTaskNameInSystray() {
        return appConfig.getOptionAsBoolean(JTimerOption.UI_SHOW_TASK_NAME_IN_SYSTRAY.key);
    }

    /**
     * Return first day of week.
     * Default to {@code -1} (no preference).
     *
     * @return first day of week in report
     */
    public int getReportFirstDayOfWeek() {
        return appConfig.getOptionAsInt(JTimerOption.UI_REPORT_FIRSTDAYOFWEEK.key);
    }

    /**
     * Report first day of week.
     *
     * @param firstDayOfWeek first day of week
     */
    public void setReportFirstDayOfWeek(int firstDayOfWeek) {
        appConfig.setOption(JTimerOption.UI_REPORT_FIRSTDAYOFWEEK.key, String.valueOf(firstDayOfWeek));
        appConfig.saveForUser();
    }

    protected enum JTimerOption {
        CONFIG_FILENAME(ApplicationConfig.CONFIG_FILE_NAME, "jtimer.properties"),

        HOME_DIRECTORY("jtimer.home.directory", "${user.home}/.jtimer"),
        DATA_DIRECTORY("jtimer.data.directory", "${jtimer.home.directory}/data"),
        BACKUP_DIRECTORY("jtimer.backup.directory", "${jtimer.home.directory}/backups"),
        TEMPLATE_DIRECTORY("jtimer.templates.directory", "${jtimer.home.directory}/templates"),

        IO_SAVER_CLASS("jtimer.io.saver.class", "org.chorem.jtimer.io.GTimerIncrementalSaver"),
        // for migration purpose (keep old keys and values for migration)
        GTIMER_DATA_DIRECTORY("jtimer.io.saver.directory", "${user.home}/.gtimer"),
        GTIMER_BACKUP_DIRECTORY("jtimer.io.backup.directory", "${jtimer.io.saver.directory}/backups"),
        IO_SAVER_AUTOSAVEDELAY("jtimer.io.saver.autosavedelay", "300"),

        UI_IDLE_TIME("jtimer.ui.idletime", "300"),
        UI_SHOW_CLOSED("jtimer.ui.showclosed", "false"),
        UI_CLOSE_TO_SYSTRAY("jtimer.ui.closetosystray", "true"),
        UI_SHOW_TASK_NAME_IN_SYSTRAY("jtimer.ui.showtasknameinsystray", "false"),
        UI_REPORT_FIRSTDAYOFWEEK("jtimer.ui.report.firstdayofweek", "0");

        protected String key;
        protected String defaultValue;

        JTimerOption(String key, String defaultValue) {
            this.key = key;
            this.defaultValue = defaultValue;
        }
    }

    protected enum JTimerAction {
        HELP("Show help", JTimerActions.class.getName() + "#help", "--help", "-h"),
        VERSION("Display application version", JTimerActions.class.getName() + "#version", "--version", "-v"),
        START("Start task", JTimerActions.class.getName() + "#start", "--start", "-s");

        protected String description;
        protected String action;
        protected String[] aliases;

        JTimerAction(String description, String action, String... aliases) {
            this.description = description;
            this.action = action;
            this.aliases = aliases;
        }
    }
}
