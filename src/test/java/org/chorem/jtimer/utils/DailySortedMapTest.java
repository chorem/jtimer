/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.utils;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Test for DailySortedMap class.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class DailySortedMapTest {

    protected static DateFormat df;

    /**
     * Init date format instance.
     */
    @BeforeClass
    public static void init() {
        df = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);
    }

    /**
     * Test le contructeur par defaut.
     */
    @Test
    public void testConstructor() {
        DailySortedMap<Long> map = new DailySortedMap<>();
        Assert.assertNotNull(map);
    }

    /**
     * Test le constructeur avec comparateur.
     * @throws ParseException
     */
    @Test
    public void testConsctuctorComparator() throws ParseException {

        DailySortedMap<Long> map = new DailySortedMap<>(Date::compareTo);

        Date d1 = df.parse("December 30, 2008");
        Date d2 = df.parse("December 29, 2008");

        map.put(d1, 1l);
        map.put(d2, 2l);
        map.put(d1, 3l);

        Assert.assertNotNull(map);
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.firstEntry().getValue().longValue(), 2l);
        Assert.assertEquals(map.lastEntry().getValue().longValue(), 3l);
    }

    /**
     * Test du constructeur avec une sorted map.
     * @throws ParseException
     */
    @Test
    public void testConstructorMap() throws ParseException {
        Date d1 = df.parse("December 30, 2008, 12:00:00 GMT");
        Date d2 = df.parse("December 30, 2008, 13:00:00 GMT");
        Date d3 = df.parse("December 31, 2008, 12:00:00 GMT");
        Date d4 = df.parse("December 31, 2008, 13:00:00 GMT");

        SortedMap<Date, Long> map = new TreeMap<>();
        map.put(d1, 1l);
        map.put(d2, 2l);
        map.put(d3, 3l);
        map.put(d4, 4l);

        // d2 erase d1, and d4 erase d3
        DailySortedMap<Long> newMap = new DailySortedMap<>(map);
        Assert.assertEquals(newMap.size(), 2);
        Assert.assertEquals(newMap.firstEntry().getValue().longValue(), 2l);
        Assert.assertEquals(newMap.lastEntry().getValue().longValue(), 4l);
    }

    /**
     * Test du constructeur avec une map.
     * @throws ParseException
     */
    @Test
    public void testConstructorSortedMap() throws ParseException {
        Date d1 = df.parse("December 30, 2008, 12:00:00 GMT");
        Date d2 = df.parse("December 30, 2008, 13:00:00 GMT");
        Date d3 = df.parse("December 31, 2008, 12:00:00 GMT");
        Date d4 = df.parse("December 31, 2008, 13:00:00 GMT");

        Map<Date, Long> map = new HashMap<>();
        map.put(d1, 1l);
        map.put(d2, 2l);
        map.put(d3, 3l);
        map.put(d4, 4l);

        // d2 erase d1, and d4 erase d3
        DailySortedMap<Long> newMap = new DailySortedMap<>(map);
        Assert.assertEquals(newMap.size(), 2);
        Assert.assertEquals(newMap.firstEntry().getValue().longValue(), 2l);
        Assert.assertEquals(newMap.lastEntry().getValue().longValue(), 4l);
    }

    /**
     * Test ceilingEntry.
     * @throws ParseException
     */
    @Test
    public void ceilingEntryTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Entry<Date, Long> entry = map.ceilingEntry(d2);

        Assert.assertEquals(entry.getKey(), d3);
        Assert.assertEquals(entry.getValue().longValue(), 13l);
    }

    /**
     * Test ceilingKey.
     * @throws ParseException
     */
    @Test
    public void ceilingKeyTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Date d = map.ceilingKey(d2);

        Assert.assertEquals(d, d3);
    }

    /**
     * Test containsKey.
     * @throws ParseException
     */
    @Test
    public void containsKeyTest() throws ParseException {
        Date d1 = df.parse("December 30, 2008, 12:00:00 GMT");

        DailySortedMap<Long> map = new DailySortedMap<>();
        map.put(d1, 1l);

        Assert.assertTrue(map.containsKey(d1));

        Date d2 = df.parse("December 31, 2008, 12:00:00 GMT");
        Assert.assertFalse(map.containsKey(d2));

        Date d3 = df.parse("December 30, 2008, 18:48:00 GMT");
        Assert.assertTrue(map.containsKey(d3));

        // non date object
        Assert.assertFalse(map.containsKey("test false"));
    }

    /**
     * Test floorEntry.
     * @throws ParseException
     */
    @Test
    public void floorEntryTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Entry<Date, Long> entry = map.floorEntry(d2);

        Assert.assertEquals(entry.getKey(), d1);
        Assert.assertEquals(entry.getValue().longValue(), 3l);
    }

    /**
     * Test floorKey.
     * @throws ParseException
     */
    @Test
    public void floorKeyTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Date d = map.floorKey(d2);

        Assert.assertEquals(d, d1);
    }

    /**
     * Test get().
     * @throws ParseException
     */
    @Test
    public void getTest() throws ParseException {
        Date d1 = df.parse("December 30, 2008, 12:00:00 GMT");
        Date d2 = df.parse("December 31, 2008, 12:00:00 GMT");

        DailySortedMap<Long> map = new DailySortedMap<>();
        map.put(d1, 1l);
        map.put(d2, 2l);

        Assert.assertEquals(1l, map.get(d1).longValue());
        Assert.assertEquals(2l, map.get(d2).longValue());

        Date d3 = df.parse("December 29, 2008, 12:00:00 GMT");
        Assert.assertNull(map.get(d3));

        Date d4 = df.parse("December 30, 2008, 18:48:00 GMT");
        Assert.assertEquals(map.get(d4).longValue(), 1l);

        // non date object
        Assert.assertNull(map.get("test false"));
    }

    /**
     * Test headMap.
     * @throws ParseException
     */
    @Test
    public void headMapTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d2, 34l);
        map.put(d3, 13l);

        SortedMap<Date, Long> newMap = map.headMap(df.parse("June 12, 2009, 18:44:59 GMT"));

        Assert.assertEquals(newMap.size(), 1);
    }

    /**
     * Test headMap.
     * @throws ParseException
     */
    @Test
    public void headMapBooleanTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d2, 34l);
        map.put(d3, 13l);

        SortedMap<Date, Long> newMap = map.headMap(df.parse("June 12, 2009, 18:44:59 GMT"), true);

        Assert.assertEquals(newMap.size(), 2);
    }

    /**
     * Test higherEntry.
     * @throws ParseException
     */
    @Test
    public void higherEntryTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        //Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Entry<Date, Long> entry = map.higherEntry(df.parse("June 12, 2009, 18:44:59 GMT"));

        Assert.assertEquals(entry.getKey(), d3);
        Assert.assertEquals(entry.getValue().longValue(), 13l);
    }

    /**
     * Test higherKey.
     * @throws ParseException
     */
    @Test
    public void higherKeyTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        //Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Date d = map.higherKey(df.parse("June 12, 2009, 18:44:59 GMT"));

        Assert.assertEquals(d, d3);
    }

    /**
     * Test lowerEntry.
     * @throws ParseException
     */
    @Test
    public void lowerEntryTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        //Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Entry<Date, Long> entry = map.lowerEntry(df.parse("June 12, 2009, 18:44:59 GMT"));

        Assert.assertEquals(entry.getKey(), d1);
        Assert.assertEquals(entry.getValue().longValue(), 3l);
    }

    /**
     * Test lowerKey.
     * @throws ParseException
     */
    @Test
    public void lowerKeyTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        //Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d3, 13l);

        Date d = map.lowerKey(df.parse("June 12, 2009, 18:44:59 GMT"));

        Assert.assertEquals(d, d1);
    }

    /**
     * Test put().
     * @throws ParseException
     */
    @Test
    public void putTest() throws ParseException {
        Date d1 = df.parse("December 30, 2008, 12:00:00 GMT");
        Date d2 = df.parse("December 30, 2008, 14:00:00 GMT");
        Date d3 = df.parse("December 31, 2008, 12:00:00 GMT");
        Date d4 = df.parse("December 31, 2008, 18:48:00 GMT");
        Date d5 = df.parse("December 31, 2008, 00:00:00 GMT");

        DailySortedMap<Long> map = new DailySortedMap<>();
        map.put(d1, 1l);
        map.put(d2, 2l);
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(d1).longValue(), 2l);

        map.put(d3, 3l);
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(d3).longValue(), 3l);

        map.put(d4, 4l);
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(d3).longValue(), 4l);

        map.put(d5, 5l);
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(d3).longValue(), 5l);
    }

    /**
     * Test putAll().
     * @throws ParseException
     */
    @Test
    public void putAllTest() throws ParseException {

        Date d1 = df.parse("December 30, 2008, 12:00:00 GMT");
        Date d2 = df.parse("December 30, 2008, 14:00:00 GMT");
        Date d3 = df.parse("December 31, 2008, 12:00:00 GMT");
        Date d4 = df.parse("December 31, 2008, 18:48:00 GMT");
        Date d5 = df.parse("December 31, 2008, 00:00:00 GMT");

        Map<Date, Long> map = new HashMap<>();
        map.put(d1, 1l);
        map.put(d2, 2l);
        map.put(d3, 3l);
        map.put(d4, 4l);
        map.put(d5, 5l);
        Assert.assertEquals(2, map.size());

        DailySortedMap<Long> newMap = new DailySortedMap<>();
        newMap.putAll(map);
        Assert.assertEquals(newMap.size(), 2);
        Assert.assertEquals(newMap.get(d1).longValue(), 2l);
        Assert.assertEquals(newMap.get(d3).longValue(), 5l);
    }

    /**
     * Test subMap.
     * @throws ParseException
     */
    @Test
    public void subMapTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d2, 3l);
        map.put(d3, 13l);

        SortedMap<Date, Long> newMap = map.subMap(df.parse("May 1, 2009, 11:02:33 GMT"),
                df.parse("October 11, 2009, 18:44:59 GMT"));

        Assert.assertEquals(newMap.size(), 2);
    }

    /**
     * Test subMap.
     * @throws ParseException
     */
    @Test
    public void subMapBooleanTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d2, 3l);
        map.put(d3, 13l);

        SortedMap<Date, Long> newMap = map.subMap(df.parse("May 09, 2009, 15:45:00 GMT"), true,
                df.parse("December 07, 2009, 17:56:12 GMT"), false);

        Assert.assertEquals(newMap.size(), 2);
    }

    /**
     * Test tailMap.
     * @throws ParseException
     */
    @Test
    public void tailMapTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d2, 3l);
        map.put(d3, 13l);

        SortedMap<Date, Long> newMap = map.tailMap(df.parse("July 15, 2009, 15:10:00 GMT"));

        Assert.assertEquals(newMap.size(), 1);
    }

    /**
     * Test tailMap.
     * @throws ParseException
     */
    @Test
    public void tailMapBooleanTest() throws ParseException {
        DailySortedMap<Long> map = new DailySortedMap<>();

        Date d1 = df.parse("May 09, 2009, 12:00:00 GMT");
        Date d2 = df.parse("June 12, 2009, 12:00:00 GMT");
        Date d3 = df.parse("December 07, 2009, 12:00:00 GMT");

        map.put(d1, 3l);
        map.put(d2, 3l);
        map.put(d3, 13l);

        SortedMap<Date, Long> newMap = map.tailMap(df.parse("December 07, 2009, 13:58:00 GMT"), false);

        Assert.assertEquals(newMap.size(), 0);
    }
}
