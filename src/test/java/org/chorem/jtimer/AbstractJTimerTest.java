/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.HiddenFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.io.Saver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Locale;
import java.util.Properties;

/**
 * Surcharge la factory du code principal.
 *
 * Redefinie les propriétés de test.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public abstract class AbstractJTimerTest {

    /** Class log */
    private static Log log = LogFactory.getLog(AbstractJTimerTest.class);

    /** Saver instance on specific test directory. */
    protected Saver testSaver;

    /** Test directory. */
    protected File testDataDirectory;

    /**
     * Return specific temp dir.
     *
     * @return system temp dir
     */
    public static String getTempDirPath() {
        String tempdir = System.getProperty("java.io.tmpdir");

        if (!(tempdir.endsWith("/") || tempdir.endsWith("\\"))) {
            tempdir += File.separator;
        }

        return tempdir;
    }

    /**
     * Redefinit les proprietes utilisées pour les tests.
     *
     * @throws IOException
     */
    protected void initDataDirectory() throws IOException {

        JTimer.config = new JTimerConfig();

        URL testConfigFile = AbstractJTimerTest.class.getResource("/jtimertest.properties");
        Properties props = new Properties(JTimer.config.appConfig.getOptions());
        props.load(testConfigFile.openStream());

        // overload system wide options
        testDataDirectory = new File(getTempDirPath(), String.valueOf(System.currentTimeMillis()));
        props.put("jtimer.home.directory", testDataDirectory.getAbsolutePath());
        props.put("jtimer.io.saver.directory", testDataDirectory.getParent() + File.separator + ".gtimer");

        JTimer.config.appConfig.setOptions(props); // not call parse in test

        if (log.isDebugEnabled()) {
            log.debug("Copy resource test directory to "
                    + testDataDirectory.getAbsolutePath());
        }

        // HiddenFileFilter.VISIBLE is used to not
        // copy .svn folders
        FileUtils.copyDirectory(new File("src/test/resources/testdata"),
                new File(testDataDirectory, "data"), HiddenFileFilter.VISIBLE);

        // force null, to force new instance
        JTimerFactory.saver = null;
        testSaver = JTimerFactory.getFileSaver();

    }

    /**
     * Delete data directory defined in saver.
     *
     * @throws IOException
     */
    protected void deleteDataDirectory() throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Removing test directory");
        }

        FileUtils.deleteDirectory(testDataDirectory);
    }

    /**
     * Before each test, set up test data directory.
     *
     * @throws IOException
     */
    @BeforeMethod
    public void beforeTest() throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Set up test");
        }

        // jtimer code use default system locale
        // test are coded in en_US
        Locale.setDefault(Locale.US);

        initDataDirectory();
    }

    /**
     * After each test, remove test data directory.
     *
     * @throws IOException
     */
    @AfterMethod
    public void afterTest() throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Clean after test");
        }
        deleteDataDirectory();
        testSaver = null;
    }

    /**
     * Get A specific task with is path in tree.
     *
     * @param tasks tasks list to search into
     * @param path path to search
     * @return found task, or <tt>null</tt> if not found
     */
    public static TimerTask findTask(Collection<? extends TimerTask> tasks,
                                     String path) {

        String[] paths = path.split("/");
        String current = paths[0];

        TimerTask foundTask = null;
        for (TimerTask subtask : tasks) {
            if (subtask.getName().equals(current)) {
                foundTask = subtask;
            }
        }

        if (foundTask != null && paths.length > 1) {
            // +1 == /
            String newPath = path.substring(current.length() + 1);
            foundTask = findTask(foundTask.getSubTasks(), newPath);

        }

        return foundTask;
    }

    /**
     * Get a specific project in project list.
     *
     * @param projects projects list
     * @param path path to search
     * @return found project, or <tt>null</tt> if not found
     */
    public static TimerProject findProject(Collection<TimerProject> projects,
                                           String path) {

        TimerProject foundProject = null;
        for (TimerProject project : projects) {
            if (project.getName().equals(path)) {
                foundProject = project;
            }
        }

        return foundProject;
    }

    /**
     * Get projects count.
     *
     * @param projects projects collection
     * @return project count
     */
    protected static int getProjectsCount(Collection<TimerProject> projects) {
        return projects.size();
    }

    /**
     * Get task count.
     *
     * @param projects projects collection
     * @return task count
     */
    protected static int getTasksCount(Collection<TimerProject> projects) {
        int tasksCount = 0;

        for (TimerProject project : projects) {
            tasksCount += getRecursiveTasksCount(project.getSubTasks());
        }
        return tasksCount;
    }

    /**
     * Get task count.
     *
     * @param tasks tasks collection
     * @return task count
     */
    protected static int getRecursiveTasksCount(Collection<TimerTask> tasks) {
        int tasksCount = 0;

        for (TimerTask task : tasks) {
            tasksCount++; // count me
            tasksCount += getRecursiveTasksCount(task.getSubTasks());
        }
        return tasksCount;
    }

    /**
     * Get annotations count.
     *
     * @param projects projects collection
     * @return annotations count
     */
    protected static int getAnnotationsCount(Collection<TimerProject> projects) {
        int annotationsCount = 0;

        for (TimerProject project : projects) {
            annotationsCount += getRecursiveAnnotationsCount(project.getSubTasks());
        }
        return annotationsCount;
    }

    /**
     * Get annotations count.
     *
     * @param tasks tasks collection
     * @return annotations count
     */
    protected static int getRecursiveAnnotationsCount(Collection<TimerTask> tasks) {
        int annotationsCount = 0;

        for (TimerTask task : tasks) {
            annotationsCount += task.getAllDaysAnnotations().size();
            annotationsCount += getRecursiveAnnotationsCount(task.getSubTasks());
        }
        return annotationsCount;
    }

    /**
     * Get alert count.
     *
     * @param projects projects collection
     * @return annotations count
     */
    protected static int getAlertsCount(Collection<TimerProject> projects) {
        int annotationsCount = 0;

        for (TimerProject project : projects) {
            annotationsCount += getRecursiveAlertsCount(project.getSubTasks());
        }
        return annotationsCount;
    }

    /**
     * Get alert count.
     *
     * @param tasks tasks collection
     * @return annotations count
     */
    protected static int getRecursiveAlertsCount(Collection<TimerTask> tasks) {
        int annotationsCount = 0;

        for (TimerTask task : tasks) {
            annotationsCount += task.getAlerts().size();
            annotationsCount += getRecursiveAlertsCount(task.getSubTasks());
        }
        return annotationsCount;
    }
}
