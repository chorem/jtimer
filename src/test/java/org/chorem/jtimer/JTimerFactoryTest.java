/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.io.GTimerIncrementalSaver;
import org.chorem.jtimer.io.Saver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * Test for JTimerFactory.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class JTimerFactoryTest extends AbstractJTimerTest {

    /** Class log */
    private static Log log = LogFactory.getLog(JTimerFactoryTest.class);

    /**
     * Verifie que les options par default sont bien chargées.
     */
    @Test
    public void testInitConfiguration() {

        if (log.isInfoEnabled()) {
            log.info("Test property loading " + JTimerFactoryTest.class);
        }

        Assert.assertEquals(JTimer.config.getIdleTime(), 299);

        Assert.assertEquals(JTimer.config.getIOSaverClass(), GTimerIncrementalSaver.class);
        // Temp dir is dynamic
        //Assert.assertEquals(AbstractJTimerTest.getTempDirPath() + "/testngdata",
        //        JTimer.config.getIOSaverDirectory());
        Assert.assertEquals(JTimer.config.getIOSaverAutoSaveDelay(), 199);

    }

    /**
     * Test json parsing of config property.
     */
    @Test
    public void testJsonTaskTemplates() {
        Map<String, Object> templates = JTimer.config.getTaskTemplates();
        Map<String, Object> p1Tpl = (Map<String, Object>) templates.get("project1");
        Assert.assertNotNull(p1Tpl);
        Map<String, Object> p2Tpl = (Map<String, Object>) templates.get("project2");
        Assert.assertNotNull(p2Tpl);
        Object p3Tpl = templates.get("project3");
        Assert.assertNull(p3Tpl);

        Assert.assertEquals(p1Tpl.size(), 5);
        Assert.assertEquals(p2Tpl.size(), 3);
    }

    /**
     * Test que le saver de fichier s'est bien initialise.
     */
    @Test
    public void getFileSaverTest() {
        Saver saver = JTimerFactory.getFileSaver();

        Assert.assertNotNull(saver);
    }

}
