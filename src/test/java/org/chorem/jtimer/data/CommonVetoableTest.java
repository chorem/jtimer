/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Test that all forbidden operation by {@link CommonVetoable} throws
 * excepted exception.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class CommonVetoableTest extends AbstractJTimerTest {

    /**
     * Test create with already existing name.
     */
    @Test(expectedExceptions = DataViolationException.class)
    public void testAddProject() {
        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();

        // add new project
        TimerProject project = new TimerProject("jTimer");
        project.setCreationDate(new Date());
        dataManager.addProject(project);
    }

    /**
     * Test create task with already existing name.
     */
    @Test(expectedExceptions = DataViolationException.class)
    public void testAddTask() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "IsisFish/UserInterface");

        // add a new task
        TimerTask newTask = new TimerTask("Debug");
        newTask.setCreationDate(new Date());
        dataManager.addTask(task1, newTask, null);
    }

    /**
     * Test single task move, trying to move projects into tasks.
     */
    @Test(expectedExceptions = DataViolationException.class)
    public void testMoveTask() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findProject(projectsBefore, "Topia");
        TimerTask task2 = findTask(projectsBefore, "jTimer/Add workspace support");

        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);

        // move task
        dataManager.moveTask(task2, Collections.singleton(task1));
    }

    /**
     * Test single task move, into another task where name already exists.
     */
    @Test(expectedExceptions = DataViolationException.class)
    public void testMoveTask2() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "Chorem/Add webservice");
        TimerTask task2 = findTask(projectsBefore, "jTimer");

        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);

        // rename tank before move
        // (task jTimer/Add workspace support exists !)
        dataManager.editTask(task1, "Add workspace support");

        // move task
        dataManager.moveTask(task2, Collections.singleton(task1));
    }

    /**
     * Test multiples tasks move move into one task to move.
     */
    @Test(expectedExceptions = DataViolationException.class)
    public void testMoveMultiplesTasks() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "jTimer/Add workspace support");
        TimerTask task2 = findTask(projectsBefore, "jTimer/Interact with chorem services");
        TimerTask task3 = findTask(projectsBefore, "jTimer/Refactoring");
        TimerTask task4 = findTask(projectsBefore, "jTimer/Unit tests");

        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);
        Assert.assertNotNull(task3);
        Assert.assertNotNull(task4);

        Collection<TimerTask> tasksToMove = new HashSet<>();
        tasksToMove.add(task1);
        tasksToMove.add(task2);
        tasksToMove.add(task3);
        tasksToMove.add(task4);

        // move task 3 in task1 : forbidden
        dataManager.moveTask(task1, tasksToMove);

    }

    /**
     * Merge two projects and tasks.
     */
    @Test(expectedExceptions = DataViolationException.class)
    public void testMergeTasks() {
        // first load all
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "Chorem");
        TimerTask task2 = findTask(projectsBefore, "jTimer/Unit tests/UI tests");

        // merge tasks
        dataManager.mergeTasks(task2, Collections.singletonList(task1));
    }
}
