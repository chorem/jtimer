/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.data;

import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.entities.TimerAlert;
import org.chorem.jtimer.entities.TimerAlert.Type;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Test class for TimerDataManager.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TimerDataManagerTest extends AbstractJTimerTest {

    /**
     * Test create project.
     */
    @Test
    public void testAddProject() {
        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerProject project1 = findProject(projectsBefore, "Test project");

        Assert.assertNull(project1);
        Assert.assertEquals(projectsBefore.size(), 6);

        // add new project
        TimerProject project = new TimerProject("Test project");
        project.setCreationDate(new Date());
        dataManager.addProject(project);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerProject project1a = findProject(projectsAfter, "Test project");

        Assert.assertNotNull(project1a);
        Assert.assertEquals(projectsBefore.size(), 7);
    }

    /**
     * Test edit project.
     */
    @Test
    public void testEditProject() {
        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerProject project1 = findProject(projectsBefore, "jTimer");

        Assert.assertNotNull(project1);
        Assert.assertEquals(project1.getSubTasks().size(), 4);

        // edit project
        dataManager.editProject(project1, "Edit Name");
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();

        TimerProject project1a = findProject(projectsBefore, "Edit Name");
        Assert.assertNull(findProject(projectsAfter, "jTimer"));
        Assert.assertNotNull(project1a);
        Assert.assertEquals(project1a.getSubTasks().size(), 4);
    }

    /**
     * Test delete project.
     */
    @Test
    public void testDeleteProject() {
        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerProject project1 = findProject(projectsBefore, "jTimer");

        Assert.assertNotNull(project1);
        Assert.assertEquals(projectsBefore.size(), 6);

        // delete project
        dataManager.deleteProject(project1);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();

        Assert.assertNull(findProject(projectsAfter, "jTimer"));
        Assert.assertEquals(projectsAfter.size(), 5);
    }

    /**
     * Test create task.
     */
    @Test
    public void testAddTask() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "IsisFish/UserInterface");

        Assert.assertNotNull(task1);
        Assert.assertEquals(task1.getSubTasks().size(), 2);

        // add a new task
        TimerTask newTask = new TimerTask("new task");
        newTask.setCreationDate(new Date());
        dataManager.addTask(task1, newTask, null);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "IsisFish/UserInterface");
        TimerTask task2a = findTask(projectsAfter, "IsisFish/UserInterface/new task");

        Assert.assertNotNull(task1a);
        Assert.assertEquals(task1a.getSubTasks().size(), 3);
        Assert.assertNotNull(task2a);
    }

    /**
     * Test edit task.
     */
    @Test
    public void testEditTask() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "IsisFish/UserInterface");

        Assert.assertNotNull(task1);
        Assert.assertEquals(task1.getSubTasks().size(), 2);

        // edit task name
        dataManager.editTask(task1, "UI");
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "IsisFish/UI");
        TimerTask task2a = findTask(projectsAfter, "IsisFish/UI/Debug");

        Assert.assertNotNull(task1a);
        Assert.assertEquals(task1a.getSubTasks().size(), 2);
        Assert.assertNotNull(task2a);
    }

    /**
     * Test edit task.
     */
    @Test
    public void testDeleteTask() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerProject project1 = findProject(projectsBefore, "IsisFish");
        TimerTask task1 = findTask(projectsBefore, "IsisFish/UserInterface");

        Assert.assertNotNull(project1);
        Assert.assertEquals(project1.getSubTasks().size(), 3);
        Assert.assertNotNull(task1);

        // delete task
        dataManager.deleteTask(task1);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerProject project1a = findProject(projectsAfter, "IsisFish");
        TimerTask task1a = findTask(projectsAfter, "IsisFish/UserInterface");

        Assert.assertNotNull(project1a);
        Assert.assertEquals(project1a.getSubTasks().size(), 2);
        Assert.assertNull(task1a);
    }

    /**
     * Test single task move.
     */
    @Test
    public void testMoveTask() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "Topia");
        TimerTask task2 = findTask(projectsBefore, "jTimer/Add workspace support");
        TimerTask task3 = findTask(projectsBefore, "Topia/Add workspace support");

        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);
        Assert.assertNull(task3);

        // move task 3 in jTimer/3
        dataManager.moveTask(task1, Collections.singleton(task2));
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "Topia");
        TimerTask task2a = findTask(projectsAfter, "jTimer/Add workspace support");
        TimerTask task3a = findTask(projectsAfter, "Topia/Add workspace support");

        Assert.assertNotNull(task1a);
        Assert.assertNull(task2a);
        Assert.assertNotNull(task3a);
    }

    /**
     * Test multiples tasks move.
     */
    @Test
    public void testMoveMultiplesTasks() {

        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerProject project1 = findProject(projectsBefore, "Chorem");
        TimerTask task1 = findTask(projectsBefore, "jTimer/Add workspace support");
        TimerTask task2 = findTask(projectsBefore, "jTimer/Interact with chorem services");
        TimerTask task3 = findTask(projectsBefore, "jTimer/Refactoring");
        TimerTask task4 = findTask(projectsBefore, "jTimer/Unit tests");

        Assert.assertNotNull(project1);
        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);
        Assert.assertNotNull(task3);
        Assert.assertNotNull(task4);

        Collection<TimerTask> tasksToMove = new HashSet<>();
        tasksToMove.add(task1);
        tasksToMove.add(task2);
        tasksToMove.add(task3);
        tasksToMove.add(task4);

        // move task 3 in jTimer/3
        dataManager.moveTask(project1, tasksToMove);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "Chorem/Add workspace support");
        TimerTask task2a = findTask(projectsAfter, "Chorem/Interact with chorem services");
        TimerTask task3a = findTask(projectsAfter, "Chorem/Refactoring");
        TimerTask task4a = findTask(projectsAfter, "Chorem/Unit tests");
        TimerProject project1a = findProject(projectsBefore, "jTimer");

        Assert.assertNotNull(task1a);
        Assert.assertNotNull(task2a);
        Assert.assertNotNull(task3a);
        Assert.assertNotNull(task4a);
        Assert.assertEquals(0, project1a.getSubTasks().size());
    }

    /*
     * Merge two project together.
     * 
     * Can't merge 2 projects.
     *
    public void testMergeProjects() {
        // first load all
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();
        
        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerProject project1 = findProject(projectsBefore, "Topia");
        TimerProject project2 = findProject(projectsBefore, "jTimer");
        
        Assert.assertNotNull(project1);
        Assert.assertNotNull(project2);
        Assert.assertEquals(project1.getSubTasks().size(), 3);
        Assert.assertEquals(project2.getSubTasks().size(), 4);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(project1), 62005);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(project2), 160280);
        
        // merge projects
        dataManager.mergeProjects(project1, Collections.singletonList(project2));
        core.exit();
        
        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerProject project1a = findProject(projectsAfter, "Topia");
        TimerProject project2a = findProject(projectsAfter, "jTimer");
        
        Assert.assertNotNull(project1a);
        Assert.assertNull(project2a);
        Assert.assertEquals(project1.getSubTasks().size(), 7);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(project1), 62005 + 160280);
    }*/

    /**
     * Merge two tasks together.
     */
    @Test
    public void testMergeTasks() {
        // first load all
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "jTimer/Refactoring");
        TimerTask task2 = findTask(projectsBefore, "jTimer/Unit tests/UI tests");
        TimerTask task3 = findTask(projectsBefore, "jTimer/Unit tests");

        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);
        Assert.assertEquals(task1.getSubTasks().size(), 2);
        Assert.assertEquals(getRecursiveAnnotationsCount(Collections.singleton(task1)), 5);
        Assert.assertEquals(task2.getSubTasks().size(), 2);
        Assert.assertEquals(getRecursiveAnnotationsCount(Collections.singleton(task2)), 2);
        Assert.assertEquals(task3.getSubTasks().size(), 4);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task1), 55035000);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task2), 65602000);

        // merge tasks
        dataManager.mergeTasks(task1, Collections.singletonList(task2));
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "jTimer/Refactoring");
        TimerTask task2a = findTask(projectsAfter, "jTimer/Unit tests/UI tests");
        TimerTask task3a = findTask(projectsAfter, "jTimer/Unit tests");

        Assert.assertNotNull(task1a);
        Assert.assertNull(task2a);
        Assert.assertNotNull(task3a);
        Assert.assertEquals(task1a.getSubTasks().size(), 4);
        Assert.assertEquals(getRecursiveAnnotationsCount(Collections.singleton(task1a)), 5 + 2);
        Assert.assertEquals(task3a.getSubTasks().size(), 3);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task1a), 55035000 + 65602000);
    }

    /**
     * Merge two tasks together.
     *
     * In both task subtask, there is a name conflict (same subtask name).
     */
    @Test
    public void testMergeTasksWithConflict() {
        // first load all
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "jTimer/Refactoring");
        Assert.assertNotNull(task1);

        // add THE conflict task name
        TimerTask tTreeTests = new TimerTask("Tree tests");
        tTreeTests.setCreationDate(new Date());
        tTreeTests.setTime(new Date(), 200000L);
        dataManager.addTask(task1, tTreeTests, null);

        Assert.assertEquals(task1.getSubTasks().size(), 3);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task1), 55035000 + 200000);

        TimerTask task2 = findTask(projectsBefore, "jTimer/Unit tests/UI tests");
        Assert.assertEquals(task2.getSubTasks().size(), 2);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task2), 65602000);

        // merge tasks
        dataManager.mergeTasks(task1, Collections.singletonList(task2));
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "jTimer/Refactoring");
        TimerTask task2a = findTask(projectsAfter, "jTimer/Unit tests/UI tests");

        Assert.assertNotNull(task1a);
        Assert.assertNull(task2a);
        // 4 = conflict resolved
        Assert.assertEquals(task1a.getSubTasks().size(), 4);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task1a),
                55035000 + 65602000 + 200000);
    }

    /**
     * Test add alert.
     *
     * Alert reloading was buggy when multiple alert types were defined.
     * (i.e. two alert type REACH_DAILY_TIME)
     */
    @Test
    public void testAddAlert() {
        // first load all
        // and make a move operation
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "jTimer/Refactoring");

        Assert.assertNotNull(task1);
        Assert.assertEquals(getRecursiveAlertsCount(Collections
                .singleton(task1)), 0);

        // add new project
        TimerAlert alert1 = new TimerAlert(TimerAlert.Type.REACH_DAILY_TIME, 3600000);
        TimerAlert alert2 = new TimerAlert(TimerAlert.Type.REACH_DAILY_TIME, 7200000);
        TimerAlert alert3 = new TimerAlert(TimerAlert.Type.REACH_TOTAL_TIME, 10800000);
        task1.addAlert(alert1);
        task1.addAlert(alert2);
        task1.addAlert(alert3);
        dataManager.modifyAlert(task1);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "jTimer/Refactoring");

        Assert.assertNotNull(task1a);
        Assert.assertEquals(getRecursiveAlertsCount(Collections
                .singleton(task1a)), 3);

        // test correct alert loading
        boolean alert1found = false;
        boolean alert2found = false;
        boolean alert3found = false;
        for (TimerAlert alert : task1a.getAlerts()) {
            if (alert.getType().equals(Type.REACH_DAILY_TIME) && alert.getDuration() == 3600000) {
                alert1found = true;
            }
            if (alert.getType().equals(Type.REACH_DAILY_TIME) && alert.getDuration() == 7200000) {
                alert2found = true;
            }
            if (alert.getType().equals(Type.REACH_TOTAL_TIME) && alert.getDuration() == 10800000) {
                alert3found = true;
            }
        }
        Assert.assertTrue(alert1found, "Missing first alert");
        Assert.assertTrue(alert2found, "Missing second alert");
        Assert.assertTrue(alert3found, "Missing third alert");
    }

    /**
     * Merge two tasks (tests alerts merge).
     */
    @Test
    public void testMergeTasksWithAlerts() {
        // first load all
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();
        List<TimerProject> projectsBefore = dataManager.getProjectsList();
        TimerTask task1 = findTask(projectsBefore, "IsisFish/Storage");
        TimerTask task2 = findTask(projectsBefore, "IsisFish/Support");
        TimerTask task3 = findTask(projectsBefore, "IsisFish/UserInterface");

        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);
        Assert.assertNotNull(task3);
        Assert.assertEquals(getRecursiveAlertsCount(Collections.singleton(task1)), 1);
        Assert.assertEquals(getRecursiveAlertsCount(Collections.singleton(task2)), 0);
        Assert.assertEquals(getRecursiveAlertsCount(Collections.singleton(task3)), 3);

        // merge tasks
        List<TimerTask> othersTask = new ArrayList<>();
        othersTask.add(task1);
        othersTask.add(task3);
        dataManager.mergeTasks(task2, othersTask);
        core.exit();

        // second reload
        // and test reloaded data
        //core.init();
        core.load();
        List<TimerProject> projectsAfter = dataManager.getProjectsList();
        TimerTask task1a = findTask(projectsAfter, "IsisFish/Storage");
        TimerTask task2a = findTask(projectsAfter, "IsisFish/Support");
        TimerTask task3a = findTask(projectsAfter, "IsisFish/UserInterface");

        Assert.assertNull(task1a);
        Assert.assertNotNull(task2a);
        Assert.assertNull(task3a);
        Assert.assertEquals(getRecursiveAlertsCount(Collections.singleton(task2)), 4);
    }

    /**
     * Test to find task by path.
     */
    @Test
    public void testGetTaskForPath() {
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();

        TimerTask task = dataManager.getTaskForPath("jTimer/Unit tests/UI tests");
        Assert.assertEquals(task.getName(), "UI tests");
        task = dataManager.getTaskForPath("jTimer/Unit tests/fake");
        Assert.assertNull(task);
    }

    /**
     * Test to find task by path.
     */
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testGetTaskForPathProject() {
        TimerCore core = new TimerCore();
        TimerDataManager dataManager = core.getData();

        //core.init();
        core.load();

        TimerTask task = dataManager.getTaskForPath("jTimer");
        Assert.assertNotNull(task);
    }
}
