/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.report;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.AbstractJTimerTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Test for ReportUtils class.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ReportUtilsTest extends AbstractJTimerTest {

    /** Class log */
    private static Log log = LogFactory.getLog(ReportUtilsTest.class);

    protected static DateFormat df;

    /**
     * Init date format instance.
     */
    @BeforeClass
    public static void init() {
        df = DateFormat.getDateInstance(DateFormat.FULL, Locale.FRENCH);
    }

    /**
     * Get date list, between to date (1 day interval).
     *
     * Test qu'il y a bien 46 jours entre date102008 et date112008.
     *
     * @throws ParseException
     */
    @Test
    public void testGetDailyDates() throws ParseException {

        Date date102008 = df.parse("vendredi 10 octobre 2008");
        Date date112008 = df.parse("lundi 24 novembre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> daylyDates = utils.getDailyDates(date102008, date112008);

        Assert.assertNotNull(daylyDates);
        Assert.assertEquals(46, daylyDates.size());
        Assert.assertEquals(date102008, daylyDates.get(0));
        Assert.assertEquals(date112008, daylyDates.get(daylyDates.size() - 1));
    }

    /**
     * Get date list, between to date (1 day interval).
     *
     * @throws ParseException
     */
    @Test
    public void testGetDailyDates2() throws ParseException {

        Date date1 = df.parse("lundi 6 octobre 2008");
        Date date2 = df.parse("mardi 7 octobre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> daylyDates = utils.getDailyDates(date1, date2);

        Assert.assertNotNull(daylyDates);
        Assert.assertEquals(2, daylyDates.size());
        Assert.assertEquals(date1, daylyDates.get(0));
        Assert.assertEquals(date2, daylyDates.get(daylyDates.size() - 1));
    }

    /**
     * Get date list, between to date (1 day interval).
     *
     * @throws ParseException
     */
    @Test
    public void testGetDailyDates3() throws ParseException {

        Date date1 = df.parse("vendredi 10 octobre 2008");
        Date date2 = df.parse("lundi 13 octobre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> daylyDates = utils.getDailyDates(date1, date2);

        Assert.assertNotNull(daylyDates);
        Assert.assertEquals(4, daylyDates.size());
        Assert.assertEquals(date1, daylyDates.get(0));
        Assert.assertEquals(date2, daylyDates.get(daylyDates.size() - 1));
    }

    /**
     * Get date list, between to date (1 week interval).
     *
     * Test qu'il y a bien 46 jours entre date102008 et date112008.
     * @throws ParseException
     */
    @Test
    public void testGetWeeklyDates() throws ParseException {

        Date date102008 = df.parse("vendredi 10 octobre 2008");
        Date date112008 = df.parse("lundi 24 novembre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> weeklyDates = utils.getWeeklyDates(date102008, date112008);

        Assert.assertNotNull(weeklyDates);
        Assert.assertEquals(8, weeklyDates.size());
        Assert.assertEquals(date102008, weeklyDates.get(0));
        //Assert.assertEquals(date112008, weeklyDates.get(weeklyDates.size()-1));
    }

    /**
     * Get date list, between to date (1 week interval).
     * @throws ParseException
     */
    @Test
    public void testGetWeeklyDates2() throws ParseException {

        Date date1 = df.parse("lundi 6 octobre 2008");
        Date date2 = df.parse("vendredi 10 octobre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> weeklyDates = utils.getWeeklyDates(date1, date2);

        Assert.assertNotNull(weeklyDates);
        Assert.assertEquals(1, weeklyDates.size());
        Assert.assertEquals(date1, weeklyDates.get(0));
        //Assert.assertEquals(date2, weeklyDates.get(weeklyDates.size()-1));
    }

    /**
     * Get date list, between to date (1 week interval).
     * @throws ParseException
     */
    @Test
    public void testGetWeeklyDates3() throws ParseException {

        Date date1 = df.parse("vendredi 10 octobre 2008");
        Date date2 = df.parse("lundi 13 octobre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> weeklyDates = utils.getWeeklyDates(date1, date2);

        Assert.assertNotNull(weeklyDates);
        Assert.assertEquals(2, weeklyDates.size());
        Assert.assertEquals(date1, weeklyDates.get(0));
        Assert.assertEquals(date2, weeklyDates.get(weeklyDates.size() - 1));
    }

    /**
     * Get date list, between to date (1 month interval).
     *
     * 16 mois.
     * @throws ParseException
     */
    @Test
    public void testGetMonthlyDates() throws ParseException {

        Date date1 = df.parse("lundi 24 novembre 2008");
        Date date2 = df.parse("mercredi 17 mars 2010");

        ReportUtils utils = new ReportUtils();

        List<Date> monthlyDates = utils.getMonthlyDates(date1, date2);

        Assert.assertNotNull(monthlyDates);
        Assert.assertEquals(17, monthlyDates.size());
        Assert.assertEquals(date1, monthlyDates.get(0));
        //Assert.assertEquals(date2, monthlyDates.get(monthlyDates.size()-1));

    }

    /**
     * Get date list, between to date (1 month interval).
     * @throws ParseException
     */
    @Test
    public void testGetMonthlyDates2() throws ParseException {

        Date date1 = df.parse("lundi 3 novembre 2008");
        Date date2 = df.parse("mardi 4 novembre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> monthlyDates = utils.getMonthlyDates(date1, date2);

        Assert.assertNotNull(monthlyDates);
        Assert.assertEquals(1, monthlyDates.size());
        Assert.assertEquals(date1, monthlyDates.get(0));
        //Assert.assertEquals(date2, monthlyDates.get(monthlyDates.size()-1));

    }

    /**
     * Get date list, between to date (1 month interval).
     * @throws ParseException
     */
    @Test
    public void testGetMonthlyDates4() throws ParseException {

        Date date1 = df.parse("lundi 27 octobre 2008");
        Date date2 = df.parse("lundi 10 novembre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> monthlyDates = utils.getMonthlyDates(date1, date2);

        Assert.assertNotNull(monthlyDates);
        Assert.assertEquals(2, monthlyDates.size());
        Assert.assertEquals(date1, monthlyDates.get(0));
        Assert.assertEquals(date2, monthlyDates.get(monthlyDates.size() - 1));

    }

    /**
     * Get date list, between to date (1 year interval).
     * @throws ParseException
     */
    @Test
    public void testGetYearlyDates() throws ParseException {

        Date date112008 = df.parse("lundi 24 novembre 2008");
        Date date032010 = df.parse("mercredi 17 mars 2010");

        ReportUtils utils = new ReportUtils();

        List<Date> yearlyDates = utils.getYearlyDates(date112008, date032010);

        log.debug(yearlyDates);

        Assert.assertNotNull(yearlyDates);
        Assert.assertEquals(3, yearlyDates.size());
        Assert.assertEquals(date112008, yearlyDates.get(0));
        //Assert.assertEquals(date032010, yearlyDates.get(yearlyDates.size()-1));
    }

    /**
     * Get date list, between to date (1 year interval).
     * @throws ParseException
     */
    @Test
    public void testGetYearlyDates2() throws ParseException {

        Date date1 = df.parse("lundi 24 novembre 2008");
        Date date2 = df.parse("vendredi 28 novembre 2008");

        ReportUtils utils = new ReportUtils();

        List<Date> yearlyDates = utils.getYearlyDates(date1, date2);

        log.debug(yearlyDates);

        Assert.assertNotNull(yearlyDates);
        Assert.assertEquals(1, yearlyDates.size());
        Assert.assertEquals(date1, yearlyDates.get(0));
        //Assert.assertEquals(date2, yearlyDates.get(yearlyDates.size()-1));
    }

    /**
     * Get date list, between to date (1 year interval).
     * @throws ParseException
     */
    @Test
    public void testGetYearlyDates3() throws ParseException {

        Date date1 = df.parse("lundi 24 novembre 2008");
        Date date2 = df.parse("lundi 5 janvier 2009");

        ReportUtils utils = new ReportUtils();

        List<Date> yearlyDates = utils.getYearlyDates(date1, date2);

        log.debug(yearlyDates);

        Assert.assertNotNull(yearlyDates);
        Assert.assertEquals(2, yearlyDates.size());
        Assert.assertEquals(date1, yearlyDates.get(0));
        //Assert.assertEquals(date2, yearlyDates.get(yearlyDates.size()-1));
    }
}
