/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.report;

import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.entities.TimerProject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Test for ReportGenerator class.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class ReportGeneratorTest extends AbstractJTimerTest {

    /** Class log */
    private static Log log = LogFactory.getLog(ReportGeneratorTest.class);

    protected static DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.ENGLISH);

    /**
     * Test report generation.
     *
     * @throws ParseException
     */
    @Test
    public void getReportTextDaily() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("January 1, 2008");
        Date d2 = df.parse("December 31, 2008");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_DAY_REPORT,
                projects, d1, d2, false, false, false, false);

        if (log.isDebugEnabled()) {
            log.debug("Daily report = " + content);
        }

        Assert.assertNotNull(content);
        Assert.assertFalse(content.indexOf("IsisFish") > 0);
        Assert.assertFalse(content.indexOf("jRST") > 0);
        Assert.assertTrue(content.indexOf("Topia") > 0);

        // no present task
        Assert.assertFalse(content.indexOf("Add webservice") > 0);
        Assert.assertFalse(content.indexOf("Add workspace support") > 0);

        // present task
        Assert.assertTrue(content.indexOf("Add service layer") > 0);
        Assert.assertTrue(content.indexOf("Calendar to date") > 0);
        Assert.assertTrue(content.indexOf("Tree tests") > 0);

        // no annotations
        Assert.assertFalse(content.indexOf("Test task and subtask time") > 0);
        Assert.assertFalse(content.indexOf("Not easy work") > 0);
        Assert.assertFalse(content.indexOf("Reports tests") > 0);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test report generation with annotation time.
     *
     * @throws ParseException
     */
    @Test
    public void getReportTextDailyAnnotationTime() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("November 1, 2008");
        Date d2 = df.parse("March 31, 2009");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_DAY_REPORT,
                projects, d1, d2, false, true, true, false);

        if (log.isDebugEnabled()) {
            log.debug("Daily report = " + content);
        }

        Assert.assertNotNull(content);

        // with annotations and time (* is important in test)
        Assert.assertTrue(content.indexOf("* 2:10 PM : Test task and subtask time") > 0);
        Assert.assertTrue(content.indexOf("* 12:00 AM : Not easy work") > 0);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test report generation.
     *
     * @throws ParseException
     */
    @Test
    public void getReportTextWeekly() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("November 1, 2008");
        Date d2 = df.parse("March 31, 2009");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_WEEK_REPORT,
                projects, d1, d2, true, true, false, false);

        if (log.isDebugEnabled()) {
            log.debug("Weekly report = " + content);
        }

        Assert.assertNotNull(content);
        Assert.assertTrue(content.indexOf("jTimer") > 0);
        Assert.assertTrue(content.indexOf("Topia") > 0);

        // no present task
        Assert.assertFalse(content.indexOf("Add service layer") > 0);

        // present task
        Assert.assertTrue(content.indexOf("Add webservice") > 0);
        Assert.assertTrue(content.indexOf("Add workspace support") > 0);

        // with annotations
        Assert.assertTrue(content.indexOf("* Test task and subtask time") > 0);
        Assert.assertTrue(content.indexOf("* Not easy work") > 0);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test report generation with annotation time.
     *
     * @throws ParseException
     */
    @Test
    public void getReportTextMonthlyAnnotationTime() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("November 1, 2008");
        Date d2 = df.parse("March 31, 2009");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_MONTH_REPORT,
                projects, d1, d2, true, true, true, false);

        if (log.isDebugEnabled()) {
            log.debug("Monthly report = " + content);
        }

        Assert.assertNotNull(content);

        // no annotations
        if (SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_9)) {
            Assert.assertTrue(content.indexOf("* 2/3/09, 2:10 PM : Test task and subtask time") > 0);
            Assert.assertTrue(content.indexOf("* 12/11/08, 12:00 AM : Not easy work") > 0);
        } else {
            Assert.assertTrue(content.indexOf("* 2/3/09 2:10 PM : Test task and subtask time") > 0);
            Assert.assertTrue(content.indexOf("* 12/11/08 12:00 AM : Not easy work") > 0);
        }
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test report generation.
     * @throws ParseException
     */
    @Test
    public void getReportTextMonthly() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("November 1, 2008");
        Date d2 = df.parse("March 31, 2009");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_MONTH_REPORT,
                projects, d1, d2, true, false, false, false);

        if (log.isDebugEnabled()) {
            log.debug("Monthly report = " + content);
        }

        Assert.assertNotNull(content);
        Assert.assertTrue(content.indexOf("Chorem") > 0);
        Assert.assertTrue(content.indexOf("Topia") > 0);

        // no present task
        Assert.assertFalse(content.indexOf("Add service layer") > 0);

        // present task
        Assert.assertTrue(content.indexOf("Add webservice") > 0);
        Assert.assertTrue(content.indexOf("Add workspace support") > 0);

        // no annotations
        Assert.assertFalse(content.indexOf("Test task and subtask time") > 0);
        Assert.assertFalse(content.indexOf("Not easy work") > 0);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test report generation.
     * @throws ParseException
     */
    @Test
    public void getReportTextYearly() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("January 1, 2008");
        Date d2 = df.parse("December 31, 2008");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_YEAR_REPORT,
                projects, d1, d2, false, false, false, false);

        if (log.isDebugEnabled()) {
            log.debug("Yearly report = " + content);
        }

        Assert.assertNotNull(content);
        Assert.assertFalse(content.indexOf("Chorem") > 0);
        Assert.assertTrue(content.indexOf("Topia") > 0);

        // no present task
        Assert.assertFalse(content.indexOf("Add webservice") > 0);
        Assert.assertFalse(content.indexOf("Add workspace support") > 0);

        // present task
        Assert.assertTrue(content.indexOf("Add service layer") > 0);
        Assert.assertTrue(content.indexOf("Calendar to date") > 0);

        // no annotations
        Assert.assertFalse(content.indexOf("Test task and subtask time") > 0);
        Assert.assertFalse(content.indexOf("Not easy work") > 0);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test report generation.
     * @throws ParseException
     */
    @Test
    public void getReportTextByProject() throws ParseException {

        /*try {
            FIXME saver.lock();*/

        Date d1 = df.parse("January 1, 2008");
        Date d2 = df.parse("December 31, 2008");
        List<TimerProject> projects = new ArrayList<>();
        projects.addAll(testSaver.load());
        ReportGenerator generator = new ReportGenerator();
        String content = generator.getReportText(ReportGenerator.Type.BY_PROJECT_REPORT,
                projects, d1, d2, true, true, false, false);

        if (log.isDebugEnabled()) {
            log.debug("Project report = " + content);
        }

        Assert.assertNotNull(content);
        Assert.assertFalse(content.indexOf("Chorem") > 0);
        Assert.assertTrue(content.indexOf("Topia") > 0);

        // no present task
        Assert.assertFalse(content.indexOf("Add webservice") > 0);
        Assert.assertFalse(content.indexOf("Add workspace support") > 0);

        // present task
        Assert.assertTrue(content.indexOf("Add service layer") > 0);
        Assert.assertTrue(content.indexOf("Calendar to date") > 0);

        // test annotations
        Assert.assertFalse(content.indexOf("Test task and subtask time") > 0);
        Assert.assertTrue(content.indexOf("* Not easy work") > 0);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }
}
