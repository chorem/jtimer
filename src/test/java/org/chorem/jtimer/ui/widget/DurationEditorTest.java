/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.ui.widget;

import javax.swing.JFrame;

/**
 * Duration editor test class.
 *
 * @author chatellier
 * @version $Revision: 1.0 $
 *
 * Last update : $Date: 2 juil. 2009 $
 * By : $Author: chatellier $
 */
public class DurationEditorTest {

    /**
     * Show duration editor test.
     *
     * TODO add ui test framework
     */
    public void showDurationEditorTest() {
        JFrame f = new JFrame();
        f.add(new DurationEditor());
        f.pack();
        f.setVisible(true);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
