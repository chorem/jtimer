/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.system;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.ui.report.ReportGeneratorTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;

/**
 * Test for SystemInfoFactory.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class SystemInfoFactoryTest extends AbstractJTimerTest {

    /** log. */
    private static Log log = LogFactory.getLog(ReportGeneratorTest.class);

    /**
     * Test to get system info.
     *
     * Could return a valid system info, on linux
     * windows, or fail if none is available.
     */
    @Test
    public void getSystemInfoTest() {
        try {
            Optional<SystemInfo> systemInfo = SystemInfoFactory.getSystemInfo();
            if (systemInfo.isPresent()) {
                Assert.assertTrue(systemInfo.get().getIdleTime() >= 0, "Idle time must be positive");
            }
        } catch (UnsatisfiedLinkError e) {
            // can happen on system where libX11 is available
            // libXss is not (on xvfb x server for example)
            if (log.isWarnEnabled()) {
                log.warn("Can't initialize native system libraries", e);
            }
        }
    }
}
