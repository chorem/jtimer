/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.io;

import org.apache.commons.io.FileUtils;
import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.entities.TimerAlert;
import org.chorem.jtimer.entities.TimerProject;
import org.chorem.jtimer.entities.TimerTask;
import org.chorem.jtimer.entities.TimerTaskHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Test for JTimerFactory.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class GTimerIncrementalSaverTest extends AbstractJTimerTest {

    /**
     * Test que saveDirectory a une valeur attendue.
     */
    @Test
    public void saveDirectoryTest() {

        Assert.assertTrue(testSaver instanceof GTimerIncrementalSaver);

    }

    /**
     * Test que saveDirectory a une valeur attendue.
     */
    @Test
    public void autoSaveDelayTest() {

        Assert.assertTrue(testSaver instanceof GTimerIncrementalSaver);

        Assert.assertEquals(((GTimerIncrementalSaver) testSaver).autoSaveDelay, 199000);

    }

    /**
     * Test data loading.
     *
     * Test that 5 projects are loaded (39 tasks, etc...)
     */
    @Test
    public void loadTest() {

        /*try {
            //FIXME saver.lock();*/

        Collection<TimerProject> projects = testSaver.load();

        Assert.assertNotNull(projects);
        Assert.assertEquals(getProjectsCount(projects), 6);
        Assert.assertEquals(getTasksCount(projects), 41);
        Assert.assertEquals(getAnnotationsCount(projects), 9);
        Assert.assertEquals(getAlertsCount(projects), 6);
        
        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("loadTest error", e); 
        }
        Assert.fail();
        }*/
    }

    /**
     * Test project loading.
     *
     * @throws IOException
     */
    @Test
    public void getProjectFromFileTest() throws IOException {

        /*try {
            FIXME saver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        String projectFilePath = gsaver.dataSaveDirectory + "/1.project";

        TimerProject project = gsaver.getProjectFromFile(new File(
                projectFilePath));

        Assert.assertNotNull(project);
        Assert.assertEquals("Topia", project.getName());
        // not done by this method
        //Assert.assertEquals(1, project.getNumber());
        Assert.assertTrue(project.isClosed());

        Date expectedDate = new Date();
        expectedDate.setTime(2147483647 * 1000L);
        Assert.assertEquals(expectedDate, project.getCreationDate());

        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test task loading.
     *
     * Test:
     *  name
     *  times
     *  parent
     *
     * @throws IOException
     */
    @Test
    public void parseTaskFromFileTest() throws IOException {

        /*try {
            //FIXME saver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        String taskFilePath = gsaver.dataSaveDirectory + "/1.task";

        // parent projects map
        TimerProject parentProject = new TimerProject();
        parentProject.setName("Test");
        parentProject.setNumber(1);
        Map<String, TimerProject> mapNumberProject = new HashMap<>();
        mapNumberProject.put(String.valueOf(parentProject.getNumber()),
                parentProject);

        // out map for post parsing
        SortedMap<TimerTask, TimerProject> taskToManage = new TreeMap<>();

        TimerTask task = gsaver.parseTaskFromFile(mapNumberProject,
                new File(taskFilePath), taskToManage);

        Assert.assertNotNull(task);
        Assert.assertEquals("Add service layer", task.getName());
        // not done by this method
        // Assert.assertEquals(1, task.getNumber());
        Assert.assertEquals(false, task.isClosed());
        // not done by this method
        //Assert.assertEquals(parentProject, task.getParent());

        Date expectedDate = new Date();
        expectedDate.setTime(2147483647 * 1000L);
        Assert.assertEquals(expectedDate, task.getCreationDate());

        long totalTime = 0;
        for (Long time : task.getAllDaysAndTimes().values()) {
            totalTime += time;
        }
        Assert.assertEquals(totalTime, 11391000);

        // test 20080909 9000

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2008);
        calendar.set(Calendar.MONTH, 8);
        calendar.set(Calendar.DAY_OF_MONTH, 9);
        Assert.assertEquals(task.getTime(calendar.getTime()), 9000000);

        // 20080922 13
        calendar.set(Calendar.YEAR, 2008);
        calendar.set(Calendar.MONTH, 8);
        calendar.set(Calendar.DAY_OF_MONTH, 22);
        Assert.assertEquals(task.getTime(calendar.getTime()), 13000);

        // 20080930 2332
        calendar.set(Calendar.YEAR, 2008);
        calendar.set(Calendar.MONTH, 8);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        Assert.assertEquals(task.getTime(calendar.getTime()), 2332000);

        // 20081011 46
        calendar.set(Calendar.YEAR, 2008);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.DAY_OF_MONTH, 11);
        Assert.assertEquals(task.getTime(calendar.getTime()), 46000);

        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("getProjectFromFileTest error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test annotation loading.
     *
     * @throws IOException
     */
    @Test
    public void parseAnnotationsTest() throws IOException {

        /*try {
            //FIXME saver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        // task to get annotation
        TimerTask task = new TimerTask();
        task.setNumber(13);

        gsaver.parseAnnotations(task);

        Assert.assertNotNull(task.getAllDaysAnnotations());
        Assert.assertEquals(2, task.getAllDaysAnnotations().size());

        // first ann
        Date date = new Date();
        date.setTime(1228950001 * 1000L);
        Assert.assertEquals(date, task.getAllDaysAnnotations()
                .firstKey());
        Assert.assertEquals("Very hard work", task.getAllDaysAnnotations().get(
                task.getAllDaysAnnotations().firstKey()));

        // second ann
        date = new Date();
        date.setTime(1228950002 * 1000L);
        Assert.assertEquals(date, task.getAllDaysAnnotations()
                .lastKey());
        Assert.assertEquals("Not easy work", task.getAllDaysAnnotations().get(
                task.getAllDaysAnnotations().lastKey()));

        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("parseAnnotations error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test alert loading.
     *
     * @throws IOException
     */
    @Test
    public void parseAlertsTest() throws IOException {

        /*try {
            //FIXME saver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        // task to get annotation
        TimerTask task = new TimerTask();
        task.setNumber(18);

        gsaver.parseAlerts(task);

        Assert.assertNotNull(task.getAlerts());
        Assert.assertEquals(task.getAlerts().size(), 2);

        // first alert
        Assert.assertEquals(task.getAlerts().get(0).getType(), TimerAlert.Type.REACH_DAILY_TIME);
        Assert.assertEquals(task.getAlerts().get(0).getDuration(), 3600000);

        // second alert
        Assert.assertEquals(task.getAlerts().get(1).getType(), TimerAlert.Type.REACH_TOTAL_TIME);
        Assert.assertEquals(task.getAlerts().get(1).getDuration(), 25000000);

        /*FIXME saver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("parseAnnotations error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test que les taches sans projets parent (autorisé par gtimer) sont bien
     * chargé par jtimer dans un projet nommé "No project".
     *
     * @throws IOException
     */
    @Test
    public void defaultProjectTest() throws IOException {
        /*try {
            //FIXME testSaver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        // task to get annotation
        Collection<TimerProject> projects = gsaver.load();

        TimerProject project1 = findProject(projects, "No project");
        Assert.assertNotNull(project1);

        TimerTask task1 = findTask(projects, "No project/Test no parent project");
        Assert.assertNotNull(task1);
        Assert.assertEquals(TimerTaskHelper.getAllTotalTime(task1), 9644000);

        /*FIXME testSaver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("parseAnnotations error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test que les lignes avec 0 ne sont pas chargées dans les fichiers de temps.
     *
     * @throws IOException
     */
    @Test
    public void dontLoadZeroTimeTest() throws IOException {
        /*try {
            //FIXME testSaver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        // task to get annotation
        Collection<TimerProject> projects = gsaver.load();

        TimerTask task1 = findTask(projects, "Chorem/Add webservice");
        Assert.assertNotNull(task1);
        Assert.assertEquals(task1.getAllDaysAndTimes().size(), 3);

        /*FIXME testSaver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("parseAnnotations error", e);
            Assert.fail();
        }
        }*/
    }

    /**
     * Test que les lignes avec 0 ne sont pas enregistrées dans les fichiers de temps.
     *
     * @throws IOException
     */
    @Test
    public void dontSaveZeroTimeTest() throws IOException {
        /*try {
            //FIXME testSaver.lock();*/

        GTimerIncrementalSaver gsaver = (GTimerIncrementalSaver) testSaver;

        // task to get annotation
        Collection<TimerProject> projects = gsaver.load();

        TimerProject project = findProject(projects, "Chorem");
        TimerTask task = new TimerTask("test");
        task.setCreationDate(new Date());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_MONTH, 26);

        task.setTime(new Date(), 0L);
        task.setNumber(99);
        project.addTask(task);
        gsaver.saveTask(task);

        String taskFileName = gsaver.dataSaveDirectory + File.separator + task.getNumber() + "." + GTimerIncrementalSaver.GTIMER_TASK_EXTENSION;
        File taskFile = new File(taskFileName);
        List<String> lines = FileUtils.readLines(taskFile, StandardCharsets.UTF_8);
        Assert.assertTrue(lines.stream().noneMatch(line -> line.startsWith("2016")));

        /*FIXME testSaver.unlock();
        } catch (DataLockingException e) {
        if(log.isErrorEnabled()) {
            log.error("parseAnnotations error", e);
            Assert.fail();
        }
        }*/
    }
}
