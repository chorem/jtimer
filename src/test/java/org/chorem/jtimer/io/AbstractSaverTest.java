/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.io;

import org.apache.commons.io.FileUtils;
import org.chorem.jtimer.AbstractJTimerTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Test for AbstractSaver.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class AbstractSaverTest extends AbstractJTimerTest {

    /**
     * Test that backup create a backup file.
     *
     * @throws IOException
     */
    @Test
    public void makeBackupFileTest() throws IOException {
        File file = File.createTempFile("test", ".test");

        File backupFile = BackupUtils.makeBackupFile(file);

        Assert.assertNotNull(backupFile);
        Assert.assertTrue(backupFile.isFile());
    }

    /**
     * Test that restore restore backup file.
     *
     * @throws IOException
     */
    @Test
    public void restoreBackupFileTest() throws IOException {
        File file = File.createTempFile("test", ".test");
        FileUtils.writeStringToFile(file, "oldcontent", StandardCharsets.UTF_8);

        File backupFile = BackupUtils.makeBackupFile(file);

        Assert.assertNotNull(backupFile);
        Assert.assertTrue(backupFile.isFile());

        FileUtils.writeStringToFile(file, "newcontent", StandardCharsets.UTF_8);
        Assert.assertEquals(FileUtils.readFileToString(file, StandardCharsets.UTF_8), "newcontent");
        Assert.assertEquals(FileUtils.readFileToString(backupFile, StandardCharsets.UTF_8), "oldcontent");

        boolean result = BackupUtils.restoreBackupFile(backupFile);
        Assert.assertTrue(result);

        Assert.assertFalse(backupFile.isFile());
        Assert.assertEquals(FileUtils.readFileToString(file, StandardCharsets.UTF_8), "oldcontent");
    }

    /**
     * Test that delete really delete file.
     *
     * @throws IOException
     */
    @Test
    public void deleteBackupFile() throws IOException {
        File file = File.createTempFile("test", ".test");

        File backupFile = BackupUtils.makeBackupFile(file);

        BackupUtils.deleteBackupFile(backupFile);

        Assert.assertFalse(backupFile.isFile());
    }
}
