/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package org.chorem.jtimer.entities;

import org.chorem.jtimer.AbstractJTimerTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Test related to TimerTaskHelper.
 */
public class TimerTaskHelperTest extends AbstractJTimerTest {

    /**
     * Test get path from parent.
     */
    @Test
    public void getPathFromParentTest() {
        TimerProject project = new TimerProject();
        TimerTask task1 = new TimerTask();
        TimerTask task2 = new TimerTask();

        project.addTask(task1);
        task1.addTask(task2);

        List<TimerTask> components = TimerTaskHelper.getPathFromParent(task2);
        Assert.assertEquals(components.get(0), project);
        Assert.assertEquals(components.get(1), task1);
        Assert.assertEquals(components.get(2), task2);
    }
}
