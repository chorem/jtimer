/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.entities;

import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.utils.DailySortedMap;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Date;

/**
 * Test class for TimerTask.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TimerTaskTest extends AbstractJTimerTest {

    /**
     * Test project name.
     */
    @Test
    public void testTaskName() {

        TimerTask task = new TimerTask();
        task.setName("test");

        Assert.assertEquals("test", task.getName());
    }

    /**
     * Test task clone();
     */
    @Test
    public void testCloneDailyMap() {

        TimerTask task = new TimerTask();
        task.setName("test");

        TimerTask clonedTask = task.clone();

        Assert.assertTrue(clonedTask.allDaysTimes instanceof DailySortedMap<?>, "Clone put invalid type for map");
        Assert.assertFalse(clonedTask.allDaysAnnotations instanceof DailySortedMap<?>, "Clone put invalid type for map");
    }

    /**
     * Test task subtasks.
     */
    @Test
    public void testSubtasks() {

        TimerTask task = new TimerTask();
        task.setName("test");

        TimerTask subtask = new TimerTask();
        subtask.setName("subtest");
        subtask.setCreationDate(new Date());
        task.addTask(subtask);

        TimerTask clonedTask = task.clone();

        // subtask copied
        Assert.assertEquals(1, clonedTask.getSubTasks().size());

        // subtask modification
        Assert.assertEquals("subtest", clonedTask.getSubTasks().get(0).getName());

        // clone removal, not modify first
        clonedTask.getSubTasks().clear();
        Assert.assertEquals(0, clonedTask.getSubTasks().size());
        Assert.assertEquals(1, task.getSubTasks().size());
    }

    /**
     * Test that clone is a soft clone (excepted for lists).
     */
    @Test
    public void testCloneLists() {
        TimerTask task = new TimerTask();
        task.setName("test");

        TimerTask clonedTask = task.clone();

        task.setClosed(true);
        task.addTask(new TimerTask());
        task.setName("new name");

        Assert.assertEquals(task.getName(), "new name");
        Assert.assertEquals(clonedTask.getName(), "test");
        Assert.assertTrue(task.isClosed());
        Assert.assertFalse(clonedTask.isClosed());
        Assert.assertEquals(task.getSubTasks().size(), 1);
        Assert.assertEquals(clonedTask.getSubTasks().size(), 0);
    }
}
