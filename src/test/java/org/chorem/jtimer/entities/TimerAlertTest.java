/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.entities;

import org.chorem.jtimer.AbstractJTimerTest;
import org.chorem.jtimer.entities.TimerAlert.Type;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Test class for TimerAlert.
 *
 * @author chatellier
 * @version $Revision: 2607 $
 *
 * Last update : $Date: 2009-06-18 17:19:19 +0200 (jeu. 18 juin 2009) $
 * By : $Author: echatellier $
 */
public class TimerAlertTest extends AbstractJTimerTest {

    /**
     * Test alert duration.
     */
    @Test
    public void testAlertData() {

        TimerAlert alert = new TimerAlert(Type.REACH_DAILY_TIME, 8);

        Assert.assertEquals(alert.getDuration(), 8);
        Assert.assertEquals(alert.getType(), Type.REACH_DAILY_TIME);
    }

    /**
     * Test task clone();
     */
    @Test
    public void testClone() {

        TimerAlert alert = new TimerAlert(Type.REACH_DAILY_TIME, 8);
        TimerAlert alertClone = alert.clone();

        Assert.assertEquals(alertClone.getDuration(), 8);
        Assert.assertEquals(alertClone.getType(), Type.REACH_DAILY_TIME);

        // not origin modification
        alertClone.setDuration(16);
        Assert.assertEquals(alert.getDuration(), 8);
    }

    /**
     * Test equals.
     */
    @Test
    public void testEquals() {

        TimerAlert alert = new TimerAlert(Type.REACH_DAILY_TIME, 8);
        TimerAlert alert2 = new TimerAlert(Type.REACH_TOTAL_TIME, 8);

        Assert.assertFalse(alert.equals(alert2));
        alert.setType(Type.REACH_TOTAL_TIME);
        Assert.assertEquals(alert, alert2);
    }

    /**
     * Test equals with null values.
     */
    @Test
    public void testEqualsNull() {

        Assert.assertEquals(new TimerAlert(), new TimerAlert());

        TimerAlert alert1 = new TimerAlert();
        alert1.setDuration(1);
        Assert.assertFalse(alert1.equals(new TimerAlert()));
    }

    /**
     * Test list content (hash code implementation).
     */
    @Test
    public void testAlertList() {
        List<TimerAlert> alertList = new ArrayList<>();
        alertList.add(new TimerAlert(Type.REACH_DAILY_TIME, 8));
        alertList.add(new TimerAlert(Type.REACH_DAILY_TIME, 8));
        alertList.add(new TimerAlert(Type.REACH_DAILY_TIME, 7));

        Assert.assertEquals(alertList.size(), 3);
        Assert.assertTrue(alertList.contains(new TimerAlert(Type.REACH_DAILY_TIME, 7)));
    }

    /**
     * Some tests on alert type.
     */
    @Test
    public void testAlertSet() {
        Set<TimerAlert> alertSet = new HashSet<>();
        alertSet.add(new TimerAlert(Type.REACH_DAILY_TIME, 8));
        alertSet.add(new TimerAlert(Type.REACH_DAILY_TIME, 8));
        alertSet.add(new TimerAlert(Type.REACH_DAILY_TIME, 7));

        // 2 => due to hashcode
        Assert.assertEquals(alertSet.size(), 2);
        Assert.assertTrue(alertSet.contains(new TimerAlert(Type.REACH_DAILY_TIME, 7)));
    }
}
