/*
 * #%L
 * jTimer
 * %%
 * Copyright (C) 2008 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package org.chorem.jtimer.entities;

import org.chorem.jtimer.AbstractJTimerTest;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test class for TimerProject.
 *
 * @author chatellier
 * @version $Revision$
 *
 * Last update : $Date$
 * By : $Author$
 */
public class TimerProjectTest extends AbstractJTimerTest {

    /**
     * Test project name.
     */
    @Test
    public void testProjectName() {

        TimerProject project = new TimerProject();
        project.setName("test");

        Assert.assertEquals("test", project.getName());
    }

    /**
     * Test project clone()
     */
    @Test
    public void testClone() {

        TimerProject project = new TimerProject();
        project.setName("name1");

        TimerProject project2 = project.clone();

        Assert.assertEquals("name1", project2.getName());
        Assert.assertEquals(project, project2);
    }
}
