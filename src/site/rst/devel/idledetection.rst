.. -
.. * #%L
.. * jTimer
.. * %%
.. * Copyright (C) 2007 - 2016 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Idle detection
==============

System info
-----------

jTimer use JNA to call system native code to get user idle time since it's
not possible in pure Java.


Time tracking on task
---------------------

The time tracking algorithm is mainly based on "start task timestamp" and
"current timestamp" comparison. This comparison is adjusted with user idle time.

So the algorithm work as it:
  * init start timestamp
  * loop iteration each 1000 ms until user stop task

    * try to detect hibernation (previous idle tracking > 5min)
    * try to detect idle (user idle time = 5min)

      * publish time to listener (UI)
      * display idle resume dialog (block time tracking)
      * remember idle time ()
      * resume task

    * publish current time to listener (UI)
  * mark task as stopped
