.. -
.. * #%L
.. * jTimer
.. * %%
.. * Copyright (C) 2007 - 2016 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Roadmap
=======

Version 1.4
-----------
* Time tracking algorithm refactoring
* Switch to Better swing application framework (bsaf)
* Add MAC OS JNA system info

Version 1.3
-----------
* Add alert on tasks

Version 1.2
-----------
* Refactoring
* Report frame complete (tree)

Version 1.1
-----------
* Add report generation

Version 1.0
-----------
* Initial release (gtimer compatible)

TODO
====

- Add languages (I18N), for now, supported languages are:

  - English (EN)
  - French (FR)


