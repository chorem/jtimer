.. -
.. * #%L
.. * jTimer
.. * %%
.. * Copyright (C) 2007 - 2016 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

File format
===========

File format used in jTimer 1.2.0 is gTimer 1.2 file format.
Since 1.3.0, jTimer add new features that gTimer can't handle.


Project
-------

**Extension :** .project

**Format :**

::

  Format: <file format>
  Name: <project name>
  Created: <creation timestamp>
  Options: <project option>

**Example :** 2.project

::

  Format: 1.2
  Name: jTImer
  Created: 2147483647
  Options: 0

Task
----

**Extension :** .task

**Format :**

::

  Format: <file format>
  Name: <task name>
  Created: <creation timestamp>
  Options: <task option>
  Project: <parent project>
  Data:
  <dateyyyyMMdd timeinseconds>

**Example :** 6.task

::

  Format: 1.2
  Name: Interact with chorem services
  Created: 2147483647
  Options: 0
  Project: 2
  Data:
  20070625 23848
  20070626 22127
  20070627 23032

Notes
-----

**Extension :** .ann

**Format :**

::

  <timestamp texte>

**Note:** x.ann note is linked to x.task task.

**Example :** 7.ann

::

  1228950001 First note
  1228950002 Second note

Alerts
------

**Extension :** .alert

**Format :**

::

  <alert type> <duration>

**Note:** x.alert alert is linked to x.task task.

Alerts are not defined in 1.2 gTimer file format.

**Example :** 7.alert

::

  reachdailytime 3600
  reachtotaltime 7200
