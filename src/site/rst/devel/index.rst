.. -
.. * #%L
.. * jTimer
.. * %%
.. * Copyright (C) 2007 - 2016 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Developer
=========

Technologies
------------

Swing Application Framework
~~~~~~~~~~~~~~~~~~~~~~~~~~~

jTimer is build according to JSR-296_ which define
swing application lifecycle. He is build on reference
implementation : `Swing Application Framework`_.


Swingx
~~~~~~

jTimer use Swingx_ additional components set.


Java Native Access
~~~~~~~~~~~~~~~~~~

Pur java, idle detection seems to be impossible.
So, this application use JNA_ to access those
informations.


.. _JSR-296 : http://jcp.org/en/jsr/detail?id=296
.. _Swing Application Framework : https://appframework.dev.java.net/
.. _Swingx : https://swingx.dev.java.net/
.. _JNA: https://jna.dev.java.net/
