.. -
.. * #%L
.. * jTimer
.. * %%
.. * Copyright (C) 2007 - 2016 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Swing application framework
===========================

Here are examples of Swing codes based on `Swing Application Framework`_.

Actions
-------

File org.chorem.jtimer.test.SafTest1.java:

::

  public void SafTest1 extends SingleFrameApplication {
      
      public JButton helloWorldButton;
      
      public static void main(String[] args) {
          launch(SafTest1.class, args);
      }
      
      @Action
      public void helloWorld() {
          System.out.prinln("Hello world");
      }
      
      @Override
      public void initialize() {
          helloWorldButton = new JButton();
          helloWorldButton.setAction(getAction("helloWorld");
      }
      
      @Override
      public void ready() {
          show(helloWorldButton);
      }
  }

File org/chorem/jtimer/test/resources/SafTest1.properties:

::

  Application.title = Test 1
  Application.id = saftest1
  
  helloWorld.Action.text=Say hello
  helloWorld.Action.shortDescription=Click me to say hello


I18N
----

To translate previous example, in french for example, just create a properties
file suffixed by the locale containing translated strings.

File org/chorem/jtimer/test/resources/SafTest1_fr.properties:

::

  helloWorld.Action.text=Dit bonjour
  helloWorld.Action.shortDescription=Cliquer pour dire bonjour !


Bindings
--------

We can how bind some properties to swing elements. This bindings have to be set
in action annotation, this properties concern all element bind on this action.

File org.chorem.jtimer.test.SafTest2.java:

::

  public void SafTest2 extends SingleFrameApplication {
      
      public JButton helloWorldButton;
      
      public static void main(String[] args) {
          launch(SafTest1.class, args);
      }
      
      public boolean isActive() {
          return false;
      }
      
      @Action(enabledProperty="active")
      public void helloWorld() {
          System.out.prinln("Hello world");
      }
      
      @Override
      public void initialize() {
          helloWorldButton = new JButton();
          helloWorldButton.setAction(getAction("helloWorld");
      }
      
      @Override
      public void ready() {
          show(helloWorldButton);
      }
  }

.. _Swing Application Framework: https://appframework.dev.java.net/
