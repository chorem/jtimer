.. -
.. * #%L
.. * jTimer
.. * %%
.. * Copyright (C) 2007 - 2016 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Configuration
=============

As of jTimer 1.5, application does not contains (yet) a configuration UI. But, by editing jtimer
configuration file, it is possible to enable some features.

File
----
The location of jTimer configuration file depend on your system.

  * window: ``%APPDATA%\jtimer.properties`` or ``%USER_HOME%\Application Data\jtimer.properties``
  * mac os: ``${userHome}/Library/Application Support/jtimer.properties``
  * linux: ``${userHome}/.config/jtimer.properties``

Idle time
---------
Default idle time is ``300`` seconds (5 minutes). It is possible to modify this time using::

  jtimer.io.saver.autosavedelay=300

Task template
-------------
jTimer can create a task using subtask template. You can specify template, as yaml format, by creating
a file into ``${userHome}/.jtimer/templates`` directory (one file per template).

For example, in file ``templatename1.yml``::

  dev:
    E1:
    E2:
  ano: 
    A1: 
  admin:
    database:
        sub database task1:
        sub database task2:
  meetings:
  releases:
